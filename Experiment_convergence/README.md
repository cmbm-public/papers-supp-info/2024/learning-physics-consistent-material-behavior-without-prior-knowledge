## Learn constitutive relations with different data resolutions
This folder contains the code for experiments in Sec.4.1.


### Instructions

1. Suppose the Neo-Hookean FEM simulation (at h_gen = 0.001) has been generated in in  `../Experiment_various_materials/generate_data/`  
   Otherwise, following the instructions in the README in `../Experiment_various_materials/generate_data/` to generate the simulation
   


2. Process the simulation and generate input for training the neural network, at different coarse resolutions
   
   ```bash
   # in data_process/
   bash generate_uLED_input.bash
   ```

3. Train uLED
   
   ```bash
   #in uLED/
   bash auto_train.bash # train uLED, with different data resolutions
   ```



4. Reproduce Fig.8(a)
      ```bash
      # in learned_field_visualization/
      python visualization_difference_new.py
    
      # after this, NH_W_field_difference.pdf should be generated in learned_field_visualization/
      ```

5. Reproduce Fig.8(b)
      ```bash
      # in uLED/
      python NH_convergence_error_Fpath.py
    
      # after this, NH_convergence_error.pdf should be in uLED/figs/
      ```

