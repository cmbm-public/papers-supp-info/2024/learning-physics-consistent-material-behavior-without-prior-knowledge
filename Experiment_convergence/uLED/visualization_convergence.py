import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc,rcParams
from matplotlib.ticker import FormatStrFormatter

from figs.plot_helper_for_paper import set_size
plt.style.use(['figs/prl_paper.mplstyle'])
import colorcet


# plt.rcParams["font.weight"] = "bold"
# plt.rcParams['xtick.major.pad']='1'
# plt.rcParams['ytick.major.pad']='0.5'
# plt.rc('xtick', labelsize=6.5)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=6.5)
# plt.rc('legend', fontsize=7)


def read_data_info(log_file):
    """
    Read the inertia term (kinetc energy) and the internal strain energy term
    """
    average_momentum_scale = None
    average_predict_momentum_scale = None
    with open(log_file, "r") as f:
        lines = [line.strip() for line in f]
    
    for line in lines:
        line = line.split(":")
        line = [item.strip() for item in line]
        if line[0] == "average_momentum_scale":
            assert len(line) == 2
            average_momentum_scale = float(line[-1])
        elif line[0] == "average_predict_momentum_scale":
            assert len(line) == 2
            average_predict_momentum_scale = float(line[-1])
        else:
            continue
    assert average_momentum_scale != None
    assert average_predict_momentum_scale != None

    return average_momentum_scale, average_predict_momentum_scale



def read_testing_logs(log_file):
    """
    Read the MAE of predicted energy density and the MAE of predicted stress
    """
    momentum_consistence_MAE = None
    predict_momentum_MAE = None
    average_momentum_scale = None


    predict_P_MAE = None
    predict_W_MAE = None

    average_P_scale = None
    average_W_scale = None

    with open(log_file, "r") as f:
        lines = [line.strip() for line in f]
    
    for line in lines:
        line = line.split(":")
        line = [item.strip() for item in line]
        if line[0] == "average_momentum_MAE":
            assert len(line) == 2
            momentum_consistence_MAE = float(line[-1])
        elif line[0] == "predict_momentum_MAE":
            assert len(line) == 2
            predict_momentum_MAE = float(line[-1])
        elif line[0] == "average_momentum_scale":
            assert len(line) == 2
            average_momentum_scale = float(line[-1])
        elif line[0] == "predict_W_MAE":
            assert len(line) == 2
            predict_W_MAE = float(line[-1])
        elif line[0] == "predict_P_MAE":
            assert len(line) == 2
            predict_P_MAE = float(line[-1])            
        elif line[0] == "average_P_scale":
            assert len(line) == 2
            average_P_scale = float(line[-1])
        elif line[0] == "average_W_scale":
            assert len(line) == 2
            average_W_scale = float(line[-1])
        else:
            continue
    
    assert momentum_consistence_MAE != None
    assert predict_momentum_MAE != None
    assert average_momentum_scale != None

    assert predict_P_MAE != None
    assert predict_W_MAE != None

    assert average_P_scale != None
    assert average_W_scale != None
    
    # return momentum_consistence_MAE, predict_momentum_MAE, average_momentum_scale, predict_W_MAE, predict_P_MAE, average_P_scale, average_W_scale
    return  predict_W_MAE, predict_P_MAE, average_P_scale, average_W_scale


def construct_mean_std(curves_dict):
    mean_list = []
    std_list = []
    for ele_size in interpolate_size_list:
        this_ele_size_results = curves_dict["eleSize_{}".format(ele_size)]
        if ele_size == 0:
            assert len(this_ele_size_results) == 2
        else:
            assert len(this_ele_size_results) == 3

        this_mean = np.mean(this_ele_size_results)
        this_std = np.std(this_ele_size_results)
        mean_list.append(this_mean)
        std_list.append(this_std)
    mean_list = np.array(mean_list)
    std_list = np.array(std_list)
    return mean_list, std_list

def set_yaxis_format(ax):    
    formatter = matplotlib.ticker.ScalarFormatter(useMathText= True, useOffset=False)
    # formatter.set_powerlimits((-1, 1))
    formatter.set_powerlimits((0, 0))
    ax.yaxis.set_major_formatter(formatter)

    plt.gcf().canvas.draw()
    offset_text = ax.yaxis.get_offset_text().get_text()
    ax.get_yaxis().get_offset_text().set_visible(False)
    ax.text(0, 1.01, offset_text, transform=ax.transAxes, ha='left', va='baseline')

    ax.tick_params(axis='y', which='both', pad=1)

    ax.tick_params(axis='x', which='both', pad=1.5)





def plot_convergence_curves_fig():
    def cm2inch(*tupl):
        inch = 2.54
        if isinstance(tupl[0], tuple):
            return tuple(i/inch for i in tupl[0])
        else:
            return tuple(i/inch for i in tupl)
    # plt.rcParams.update({"axes.titleweight": "bold", "axes.titley": 1.05})
    
    fig, axs = plt.subplots(1, 2, figsize=set_size(width='two-column', fraction=.7, height_ratio=0.34) ) 
    # assert len(material_list) == 2
    # left top: axs[0, 0]
    # right top: axs[0, 1]

    x_pos = [i for i in range(len(interpolate_size_list))]
    x_tick_label = ["1/{}".format(10 - i) for i in range(len(interpolate_size_list) ) ]

    for material_id in range(len(material_list) ):
        material = material_list[material_id]
        

        
        ## plot discretization error of input        
        average_momentum_scale = [verify_momentum_consistence_curves[material]["eleSize_{}".format(ele_size)]["average_momentum_scale"] for ele_size in interpolate_size_list]
        average_predict_momentum_scale= [verify_momentum_consistence_curves[material]["eleSize_{}".format(ele_size)]["average_predict_momentum_scale"] for ele_size in interpolate_size_list]

        axs[0].plot(x_pos, average_momentum_scale, color = material_color[material], marker= "s", markersize=3, linewidth = 0.8, markerfacecolor="white",  markeredgewidth=0.7, markeredgecolor= "white", linestyle="-")
        axs[0].plot(x_pos, average_predict_momentum_scale, color = material_color[material], marker= "o", markersize=2.8, linewidth = 0.8, markerfacecolor="white",  markeredgewidth=0.7, markeredgecolor= "white", linestyle="-")


        # inertia term (kinetc energy)
        axs[0].plot(x_pos, average_momentum_scale, color = material_color[material], marker= "s", markersize=3, linewidth = 0.8, markerfacecolor="none",  markeredgewidth=0.7, label= "kinetic energy", markeredgecolor= material_color[material], linestyle="")

        # internal strain energy term
        axs[0].plot(x_pos, average_predict_momentum_scale, color = material_color[material], marker= "o", markersize=2.8, linewidth = 0.8, markerfacecolor="none",  markeredgewidth=0.7, label=  "internal strain energy", markeredgecolor= material_color[material], linestyle="")
        
            
            

        # predicted W MAE
        predict_W_MAE_mean, predict_W_MAE_std = construct_mean_std(W_MAE_curves[material])
        print("predict_W_MAE_mean, predict_W_MAE_std")
        print(predict_W_MAE_mean, predict_W_MAE_std)
        axs[1].plot(x_pos, predict_W_MAE_mean, color = material_color[material], marker= material_mean_marker[material], markersize=3, linewidth = .8, markerfacecolor="white",  markeredgewidth=0.7, label= MAE_mean_legend_name[material], markeredgecolor= material_color[material])
        axs[1].fill_between(x_pos, predict_W_MAE_mean - predict_W_MAE_std, predict_W_MAE_mean + predict_W_MAE_std, facecolor= material_color[material], alpha=0.25)
        
        # axs[1].plot(x_pos, predict_W_MAE_std, color = material_color[material], marker=material_std_marker[material], markersize=3, linewidth = .8, markerfacecolor="white",  markeredgewidth= .7, label = MAE_std_legend_name[material], markeredgecolor= material_color[material], linestyle=':')


    ## axs[0] format
    # axs[0].set_ylim([-0.00025, 0.004])
    # axs[0].set_ylim([0, 0.25])
    # axs[0].set_yscale('log')
    set_yaxis_format(axs[0])

    # x ticks            
    
    axs[0].set_xticks(x_pos )
    axs[0].set_xticklabels([x_tick_label[i] for i in x_pos], rotation=0, ha= "center")
        
               
    # axs[0].set_xlabel(r'data resolution ' + r'($\mathregular{h / h_{gen}}$)', labelpad= 1, fontsize=8)
    # axs[1].set_xlabel(r'data resolution ' + r'($\mathregular{h / h_{gen}}$)', labelpad= 1, fontsize=8)
    # axs[0].set_xlabel('data resolution ' + '$(h / h_{\\text{gen}} )$', labelpad= 1, fontsize=8)
    axs[0].set_xlabel(r'$ \text{data resolution}~ (h_{\text{gen}} / h )$', labelpad= 1, fontsize=8)
    axs[1].set_xlabel(r'$ \text{data resolution}~ (h_{\text{gen}} / h )$', labelpad= 1, fontsize=8)

    # discretization_ylabel = "Error of discretizing input" # due to the sampling
    discretization_ylabel = "Energy"
    # 'Discretization error of input'    
    axs[0].set_ylabel(discretization_ylabel, labelpad= 2, fontsize=8)
    # axs[0].set_ylabel('Normalized MAE of nodal force', labelpad= 1, fontsize=8)
    font_properties = axs[0].yaxis.get_label().get_fontproperties()
    print(f"Font name: {font_properties.get_name()}")
    print(f"Font style: {font_properties.get_style()}")
    print(f"Font size: {font_properties.get_size()}")
    print(f"Font weight: {font_properties.get_weight()}")

    # font_properties = axs[0].xaxis.get_label().get_fontproperties()
    # print(f"Font name: {font_properties.get_name()}")
    # print(f"Font style: {font_properties.get_style()}")
    # print(f"Font size: {font_properties.get_size()}")
    # print(f"Font weight: {font_properties.get_weight()}")

    # exit()

    predictW_ylabel = r'${\langle |\widehat{W} - W| \rangle } ~/~ {\langle |W| \rangle }$'
    # 'Normalized MAE of $\widehat{W}$'
    # axs[1].set_ylabel(r'$\frac{\langle |\widehat{W} - W| \rangle } {\langle |W| \rangle }$', labelpad= 4, fontsize=8)
    axs[1].set_ylabel(predictW_ylabel, labelpad= 2, fontsize=8)


    

    handles, labels = axs[0].get_legend_handles_labels()
    leg = axs[0].legend(handles, labels, loc='upper left', numpoints=1, ncol=1, handlelength=2, borderaxespad=0., bbox_to_anchor=(0.3, 0.97), fontsize = 6, borderpad=0.3)
    for legobj in leg.legend_handles:
        # legobj.set_linewidth(1.5)
        legobj.set_markersize(3)




    ## axs[1] format
    axs[1].set_ylim([-0.0025, 0.04])
    set_yaxis_format(axs[1])


    axs[1].set_xticks(x_pos )
    axs[1].set_xticklabels([x_tick_label[i] for i in x_pos], rotation=0, ha= "center")
    # axs[1].text(-.15, 1.05, '(C)', ha='left', va='top', transform=axs[1].transAxes, fontsize= 8)


    # handles, labels = axs[1].get_legend_handles_labels()
    # leg = axs[1].legend(handles, labels, loc='upper left', numpoints=1, ncol=1, handlelength=2, borderaxespad=0., bbox_to_anchor=(0.06, 0.97), fontsize = 6, borderpad=0.2)
    # for legobj in leg.legend_handles:
    #     # legobj.set_linewidth(1.5)
    #     legobj.set_markersize(3)

    plt.subplots_adjust(wspace=0.25)
    plt.savefig("./figs/" + 'exp2_convergence_NH_StVK.pdf', bbox_inches='tight', pad_inches = 0.03, dpi=300)
    plt.close()




if __name__ == "__main__":
    # material = "NH"
    # material = "VK"
    # material_list = ["NH", "VK"]
    material_list = ["NH"]
    material_legend_name = {"NH": "NH", "VK": "St.VK "} # Venant–\n Kirchhoff

    MAE_mean_legend_name = {"NH": "NH mean", "VK": "St.VK mean"}
    MAE_std_legend_name = {"NH": "NH std", "VK": "St.VK std"}
    
    material_color = {"NH": "#003f5c", "VK": "#ffa600"}
    material_mean_marker = {"NH": "s", "VK": "^"}
    material_std_marker = {"NH": "D", "VK": "o"}


    verify_momentum_consistence_curves = {}
    # momentum_MAE_curves = {}
    W_MAE_curves = {}
    P_MAE_curves = {}
    

    interpolate_size_list = [0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01]
    interpolate_size_list = sorted(interpolate_size_list, reverse= True)
    print("interpolate_size_list: ", interpolate_size_list)
    


    # exp_list = [1, 2]

    for material in material_list:
        if material == "NH":
            prefix = ""
        elif material == "VK":
            prefix = "VK_"
        else:
            raise RuntimeError("material invalid")
        
        verify_momentum_consistence_curves[material] = {}
        W_MAE_curves[material] = {}
        P_MAE_curves[material] = {}
   

        for ele_size in interpolate_size_list:
            
            # momentum_MAE_curves["eleSize_{}".format(ele_size)] = []
            W_MAE_curves[material]["eleSize_{}".format(ele_size)] = []
            P_MAE_curves[material]["eleSize_{}".format(ele_size)] = []

            # read the dataset info
            dataset_info_file = "../verify_dynamics/{}_ele_0.001_float64/interpolate_{}/exp1/logging.txt".format(material, ele_size)
            average_momentum_scale, average_predict_momentum_scale = read_data_info(dataset_info_file)

            verify_momentum_consistence_curves[material]["eleSize_{}".format(ele_size)] = {"average_momentum_scale": average_momentum_scale, "average_predict_momentum_scale": average_predict_momentum_scale}
            


            if ele_size == 0:
                # exp_list = [1]
                exp_list = [1, 2]
            else:
                exp_list = [1, 2, 3]


            for exp_id in exp_list:

                saved_model_folder = "./trained_{}ele_0.001_float64/interpolate_{}/exp{}/".format(prefix, ele_size, exp_id)            
            
                # print("saved_model_folder")
                # print(saved_model_folder)

                # read the training loggingdef read_data_info(log_file):

                log_file = saved_model_folder + "test_output.txt"
                W_MAE, P_MAE, P_scale, W_scale = read_testing_logs(log_file)
                

                # verify_momentum_consistence_curves[material]["eleSize_{}".format(ele_size)].append(momentum_consistence / average_momentum_scale)
                # momentum_MAE_curves["eleSize_{}".format(ele_size)].append(predict_momentum_MAE)
                W_MAE_curves[material]["eleSize_{}".format(ele_size)].append(W_MAE / W_scale)
                P_MAE_curves[material]["eleSize_{}".format(ele_size)].append(P_MAE / P_scale)

    plot_convergence_curves_fig()
    
    