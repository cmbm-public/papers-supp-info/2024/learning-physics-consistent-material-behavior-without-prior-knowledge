import os
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc,rcParams
plt.rcParams["font.weight"] = "bold"
plt.rcParams['xtick.major.pad']='1'
plt.rcParams['ytick.major.pad']='0.5'
plt.rc('xtick', labelsize=6)    # fontsize of the tick labels
plt.rc('ytick', labelsize=6)
plt.rc('legend', fontsize=7)

import pickle as pkl

from model import energy_density_NN

import argparse


parser = argparse.ArgumentParser()


parser.add_argument('--num_nodes_per_element', type=int, default=3, help='Number of vertex on each element.') 
parser.add_argument('--space_dim', type=int, default=2, help='space dimension') 

parser.add_argument('--batch_size', type=int, default=1, help='Number of samples per batch.')
parser.add_argument('--input_dim', type=int, default=2, help='Two invariants.')
parser.add_argument('--hidden_dim', type=int, default=128, help='Hidden dimension.')
parser.add_argument('--activation', type=str, default='elu', help='[]')

# parser.add_argument('--material', type= str, default= "-1", help= "[AB, Fung, Gent, MR, NH, VK]")
# parser.add_argument('--F_path', type= str, default= "-1", help= "uniaxial tension (UT), uniaxial compression (UC), biaxial tension (BT), biaxial compression (BC), simple shear (SS) and pure shear (PS).")

parser.add_argument('--gamma_steps', type=int, default=200, help='For plotting')
parser.add_argument('--exterpolation_ratio', type= float, default=0.2, help= "for the exterpolation test")
args = parser.parse_args()


def cmpt_cauchy_green_deformation(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2

    C = torch.cat((C11,C12,C21,C22), dim=-1)
    return C



def cmpt_green_strain_tensor(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2
    
    E11 = C11 - 1.0
    E12 = C12
    E21 = C21
    E22 = C22 - 1.0
    return 0.5 * torch.cat((E11, E12, E21, E22), dim= -1)



def load_F():
    # check the max / min values in F
    with open(input_data_dir + "deformation_gradient.pkl", "rb") as f:
        F_numExample_4 = pkl.load(f) 
    F_numExample_4 = F_numExample_4.numpy()
    num_time_steps = int(F_numExample_4.shape[0] * 0.8)
    F_numExample_4 = F_numExample_4[0:num_time_steps, :, :]
    print("F_numExample_4 shape: ", F_numExample_4.shape)
    F_max = np.max(F_numExample_4, axis=(0, 1) )
    # print("F_max")
    # print(F_max)
    F_min = np.min(F_numExample_4, axis=(0, 1) )
    # print("F_min")
    # print(F_min)
    return F_max, F_min


def construct_testing_F(F_max, F_min, F_path):
    F11_max, F12_max, F21_max, F22_max = F_max
    print(F11_max, F12_max, F21_max, F22_max)
    F11_min, F12_min, F21_min, F22_min = F_min
    print(F11_min, F12_min, F21_min, F22_min)

    F_numExample_4 = torch.zeros((args.gamma_steps, 4) ) # to return
    
    if F_path == "UT":
        F11_list = np.linspace(F11_min, F11_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_list[i]
            F_numExample_4[i, 3] = 1.0
        return F11_list, F_numExample_4
    
    elif F_path == "BT":
        F11_22_min = np.max((F11_min, F22_min) )
        F11_22_max = np.min((F11_max, F22_max) )
        F11_22_list = np.linspace(F11_22_min, F11_22_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_22_list[i]
            F_numExample_4[i, 3] = F11_22_list[i]
        return F11_22_list, F_numExample_4
    
    elif F_path == "SS":
        F12_list = np.linspace(F12_min, F12_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = 1.0
            F_numExample_4[i, 1] = F12_list[i]
            F_numExample_4[i, 3] = 1.0
        return F12_list, F_numExample_4
        
    else:
        raise RuntimeError("F_path invalid")
    
def construct_exterpolation_F(F_max, F_min, F_path):
    F11_max, F12_max, F21_max, F22_max = F_max
    print(F11_max, F12_max, F21_max, F22_max)
    F11_min, F12_min, F21_min, F22_min = F_min
    print(F11_min, F12_min, F21_min, F22_min)

    F_numExample_4 = torch.zeros((args.gamma_steps, 4) ) # to return
    
    if F_path == "UT":
        F11_min_extended = 1.0 -  (1+ args.exterpolation_ratio) * abs(F11_min - 1.0)
        F11_max_extended = 1.0 +  (1+ args.exterpolation_ratio) * abs(F11_max - 1.0)
        assert args.gamma_steps % 2 == 0
        left_F11_list = np.linspace(F11_min_extended, F11_min, args.gamma_steps // 2)
        right_F11_list = np.linspace(F11_max, F11_max_extended, args.gamma_steps // 2)

        F11_list = np.concatenate((left_F11_list, right_F11_list))

        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_list[i]
            F_numExample_4[i, 3] = 1.0
        return F11_list, F_numExample_4, F11_min, F11_max
    

    elif F_path == "BT":
        F11_22_min = np.max((F11_min, F22_min) )
        F11_22_max = np.min((F11_max, F22_max) )
        F11_22_min_extended = 1.0 - (1 + args.exterpolation_ratio) * abs(F11_22_min - 1.0)
        F11_22_max_extended = 1.0 + (1 + args.exterpolation_ratio) * abs(F11_22_max - 1.0)
        assert args.gamma_steps % 2 == 0
        left_F11_22_list = np.linspace(F11_22_min_extended, F11_22_min, args.gamma_steps // 2)
        right_F11_22_list = np.linspace(F11_22_max, F11_22_max_extended, args.gamma_steps // 2)

        F11_22_list = np.concatenate((left_F11_22_list, right_F11_22_list ) )

        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_22_list[i]
            F_numExample_4[i, 3] = F11_22_list[i]
        return F11_22_list, F_numExample_4, F11_22_min, F11_22_max
    

    elif F_path == "SS":
        F12_min_extended = (1 + args.exterpolation_ratio) * F12_min
        F12_max_extended = (1 + args.exterpolation_ratio) * F12_max
        assert args.gamma_steps % 2 == 0
        F12_list_left = np.linspace(F12_min_extended, F12_min, args.gamma_steps // 2)
        F12_list_right = np.linspace(F12_max, F12_max_extended, args.gamma_steps // 2)

        F12_list = np.concatenate((F12_list_left, F12_list_right ) )
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = 1.0
            F_numExample_4[i, 1] = F12_list[i]
            F_numExample_4[i, 3] = 1.0
        return F12_list, F_numExample_4, F12_min, F12_max
        
    else:
        raise RuntimeError("F_path invalid")



def cmpt_groundTruth_W_and_P(F_numExample_4)  -> np.array:
    Youngs_modulus = 1e4
    nu= 0.3 # Poisson rate
    mu  = Youngs_modulus / (2.0*(1.0 + nu))
    lmbda = Youngs_modulus * nu / ((1.0 + nu)*(1.0 - 2.0*nu))
    bulk_K = Youngs_modulus / (3.0 * (1.0 - 2 * nu) ) #bulk modulus

    if material == "NH":
        # New-Hookean
        F_numExample_4.requires_grad = True
        C_numExample_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_numExample_1 = C_numExample_4[:, 0:1] + C_numExample_4[:, 3:4]
        J_numExample_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        W = (mu / 2) * (I1_numExample_1 - 2) - mu * torch.log(J_numExample_1) + (lmbda / 2) * (torch.log(J_numExample_1))**2 # 2D
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "VK":            
        F_numExample_4.requires_grad = True
        E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4)
        I1 = E_numExample_4[:, 0:1] + E_numExample_4[:, 3:4]
        I2 = E_numExample_4[:, 0:1] * E_numExample_4[:, 3:4] - E_numExample_4[:, 1:2] * E_numExample_4[:, 2:3]
        W = (lmbda /2 ) * I1 **2 + mu * (I1 **2 - 2 * I2)
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    
    elif material == "MR":
        C10 = 7. * mu / 16
        C01 = 1. * mu / 16
        
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I2_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        I1_bar = J_T_numE_1 ** (- 2. / 2.) * I1_T_numE_1
        I2_bar = J_T_numE_1 ** (- 4. / 2.) * I2_T_numE_1
        term1 = C10 * (I1_bar - 2)
        term2 = C01 * (I2_bar - 1)
        term3 = 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        W = term1 + term2 + term3
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Fung":
        b = 1.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        exponential = b * (I1_T_numE_1 * ( I3_T_numE_1 ** (-0.5) ) - 2)
        W = mu / (2 * b) * ( exponential + torch.exp(exponential) - 1.0 ) + 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Gent":
        Jm = 10
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        W_inc = - (mu / 2) * (Jm * torch.log( 1 - ( I3_T_numE_1 ** (-0.5) * I1_T_numE_1 - 2.0) / Jm )  )
        W_vol = 0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W_inc + W_vol
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "AB":
        C1 = mu
        N = 10.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_bar = I3_T_numE_1 ** (-0.5) * I1_T_numE_1
        W1 = C1 * ( 0.5 * (I1_bar - 2) + (1 / (20 * N) ) * (I1_bar**2  - 4) + (11 / (N ** 2 * 1050 ) ) * (I1_bar**3  - 8) + (19 / (N ** 3 * 7000 ) ) * (I1_bar**4  - 16) + (519 / (N ** 4 * 673750 ) ) * (I1_bar**5  - 32) )
        W2 =  0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W1 + W2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    else:
        raise RuntimeError("material error")
    
    return W.detach().numpy(), P_numExample_4.detach().numpy()


def predict_W_and_P(F_numExample_4) -> np.array:
    zero_strain_1_4 = torch.zeros((1, 4) )
    if cuda_flag:
        zero_strain_1_4 = zero_strain_1_4.cuda()
    zero_strain_1_4.requires_grad = True
    


    ## input data
    E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4) # green strain tensor
    E_numExample_4 = torch.autograd.Variable(E_numExample_4)
    E_numExample_4.requires_grad = True

    # normalize INPUT Green strain
    normalizedE_numExample_4 = E_numExample_4 / greenStrain_max

    energy_numExample_1 = energy_model(normalizedE_numExample_4)
    S_numExample_4 = torch.autograd.grad(energy_numExample_1.sum(), E_numExample_4, create_graph=True)[0]

    ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
    zeroEnergy_1_1 = energy_model(zero_strain_1_4)
    zeroS_1_4  = torch.autograd.grad(zeroEnergy_1_1.sum(), zero_strain_1_4, create_graph = True)[0]
    zeroS_1_2_2 = zeroS_1_4.reshape(1, 2, 2)
    zeroP_1_2_2 = zeroS_1_2_2 # because of the unit_F_1_1_2_2
    
    # energy
    energy_numExample_1 = energy_numExample_1 - zeroEnergy_1_1
    

    ## correct the predicted stress
    S_numE_2_2 = S_numExample_4.reshape(S_numExample_4.shape[0],  2, 2) # last two dimensions are i and j. sum over j
    F_numE_2_2 = F_numExample_4.reshape(F_numExample_4.shape[0], 2, 2)
    P_numE_2_2 = torch.matmul(F_numE_2_2, S_numE_2_2)
    P_numE_2_2 = P_numE_2_2 - zeroP_1_2_2
    P_numExample_4 = P_numE_2_2.reshape(P_numE_2_2.shape[0], 4)
    
    # unnormalize predictions
    P_numExample_4 = P_numExample_4 * momentum_max
    energy_numExample_1 = energy_numExample_1 * momentum_max
    
    # return energy_numExample_1.detach().numpy() * greenStrain_max, P_numExample_4.detach().numpy()
    return energy_numExample_1.cpu().detach().numpy(), P_numExample_4.cpu().detach().numpy()


def plot_W_fig():
    num_materials = len(material_list)
    num_F_path = len(F_path_list)
    fig, axs = plt.subplots(num_materials, num_F_path, figsize=(5.5, 3), layout="constrained") # gridspec_kw={'height_ratios': [1, 2]}

    for material_id in range(num_materials):
        for F_path_id in range(num_F_path):
            material = material_list[material_id]
            F_path = F_path_list[F_path_id]

            # ax
            this_ax = axs[material_id, F_path_id]

            # true_W
            true_W_numExample_1 = true_W_curves[material][F_path]

            gamma_list = F_path_gamma_list[material][F_path]
            
            this_ax.plot(gamma_list, true_W_numExample_1, linewidth = 1, label = 'true W')


            # predict W, ele_size = 0
            predict_mean_W_ele0 = predict_W_curves[material][F_path][0]["mean"]
            predict_std_W_ele0 = predict_W_curves[material][F_path][0]["std"]
            this_ax.plot(gamma_list, predict_mean_W_ele0, linewidth = 1, label = "predict W, ele0")

            # predict W, ele_size = 0.005
            predict_mean_W_ele005 = predict_W_curves[material][F_path][0.005]["mean"]
            predict_std_W_ele0005 = predict_W_curves[material][F_path][0.005]["std"]
            this_ax.plot(gamma_list, predict_mean_W_ele005, linewidth = 1, label = "predict W, ele005")

            # predict W, ele_size = 0.001
            predict_mean_W_ele01 = predict_W_curves[material][F_path][0.01]["mean"]
            predict_std_W_ele001 = predict_W_curves[material][F_path][0.01]["std"]
            this_ax.plot(gamma_list, predict_mean_W_ele01, linewidth = 1, label = "predict W, ele01")

            # l1, = this_ax.plot(gamma_list, predict_mean_W_numExample_1, color = "xkcd:peacock blue", linewidth = 2, label = "predict W")
            # l2, = this_ax.plot(gamma_list, true_W_numExample_1, color = "xkcd:purple pink", linewidth = 1, label = 'true W')
            # this_ax.fill_between(gamma_list, (predict_mean_W_numExample_1 - predict_std_W_numExample_1).squeeze(), (predict_mean_W_numExample_1 + predict_std_W_numExample_1).squeeze(), facecolor="lightskyblue", alpha=1)
            
            
            # # exterpolation            
            # gamma_list = exterpolation_gamma_curves[material][F_path]
            # true_W_numExample_1 = exterpolation_true_W_curves[material][F_path]
            # predict_mean_W_numExample_1 = exterpolation_predict_W_curves[material][F_path]["mean"]
            
            # # left
            # this_ax.plot(gamma_list[:args.gamma_steps // 2], predict_mean_W_numExample_1[:args.gamma_steps // 2], color = "lightskyblue",  linewidth = 2) # linestyle= (0, (3, 1, 1, 1)),
            # this_ax.plot(gamma_list[:args.gamma_steps // 2], true_W_numExample_1[:args.gamma_steps // 2], color = "xkcd:light lilac",  linewidth = 1)
            # # right
            # l4, = this_ax.plot(gamma_list[args.gamma_steps // 2:], predict_mean_W_numExample_1[args.gamma_steps // 2:], color = "lightskyblue",  linewidth = 2, label = "predict W ext")
            # l3, = this_ax.plot(gamma_list[args.gamma_steps // 2:], true_W_numExample_1[args.gamma_steps // 2:], color = "xkcd:light lilac",  linewidth = 1, label = "true W ext")
            
            # this_ax.legend()

        if material == "VK":
            y_material = "StVK"
        else:
            y_material = material
        axs[material_id, 0].set_ylabel("{}".format(y_material)+ "\n"+ r'$W(\gamma)$', fontweight="bold", fontsize=8, labelpad= 4)

    plt.rcParams.update({"axes.titleweight": "bold", "axes.titley": 1.05})
    axs[0, 0].set_title("UT", fontsize=8, pad= 3, loc= "center")
    axs[0, 1].set_title("BT", fontsize=8, pad= 3, loc= "center")
    axs[0, 2].set_title("SS", fontsize=8, pad= 3, loc= "center")

    axs[-1, 0].set_xlabel(r'$\gamma$', fontweight="bold", fontsize=8, labelpad= 4)
    axs[-1, 1].set_xlabel(r'$\gamma$', fontweight="bold", fontsize=8, labelpad= 4)
    axs[-1, 2].set_xlabel(r'$\gamma$', fontweight="bold", fontsize=8, labelpad= 4)
    """
    handles, labels = axs[-1, -1].get_legend_handles_labels()
    handles = [(l1,l4), (l2,l3) ]
    fig.legend(handles, labels, loc='upper left', handler_map = {tuple: matplotlib.legend_handler.HandlerTuple(None)}, handlelength=4, borderaxespad=0., bbox_to_anchor=(0.02, 0.93))    
    # https://stackoverflow.com/questions/9834452/how-do-i-make-a-single-legend-for-many-subplots
    # https://stackoverflow.com/questions/51598004/custom-legend-of-several-lines-with-two-markers-for-the-same-text/51598918#51598918
    # https://stackoverflow.com/questions/31478077/how-to-make-two-markers-share-the-same-label-in-the-legend
    """
    handles, labels = axs[-1, -1].get_legend_handles_labels()
    leg = fig.legend(handles, labels, loc='upper left', ncol=4, handlelength=2.4, borderaxespad=0., bbox_to_anchor=(0.13, 0.93))
    for legobj in leg.legend_handles:
        legobj.set_linewidth(1.5)



    plt.savefig("./figs/" + 'exp2_W.pdf', bbox_inches='tight', pad_inches = 0.03)
    plt.close()




def plot_P_fig():
    num_materials = len(material_list)
    num_F_path = len(F_path_list)
    fig, axs = plt.subplots(num_materials, num_F_path, figsize=(5.5, 3), layout="constrained") # gridspec_kw={'height_ratios': [1, 2]}

    for material_id in range(num_materials):
        for F_path_id in range(num_F_path):
            material = material_list[material_id]
            F_path = F_path_list[F_path_id]
            
            # ax
            this_ax = axs[material_id, F_path_id]

            # true P
            true_P_numExample_1 = true_P_curves[material][F_path]
            
            gamma_list = F_path_gamma_list[material][F_path]

            this_ax.plot(gamma_list, true_P_numExample_1,  linewidth = 1, label = "true P")


            # predict P, ele_size = 0
            predict_mean_P_ele0 = predict_P_curves[material][F_path][0]["mean"]
            predict_std_P_ele0 = predict_P_curves[material][F_path][0]["std"]
            this_ax.plot(gamma_list, predict_mean_P_ele0, linewidth = 1, label = "predict P, ele0")

            # predict P, ele_size = 0.005
            predict_mean_P_ele005 = predict_P_curves[material][F_path][0.005]["mean"]
            predict_std_P_ele005 = predict_P_curves[material][F_path][0.005]["std"]
            this_ax.plot(gamma_list, predict_mean_P_ele005, linewidth = 1, label = "predict P, ele005")

            # predict P, ele_size = 0.01
            predict_mean_P_ele01 = predict_P_curves[material][F_path][0.01]["mean"]
            predict_std_P_ele01 = predict_P_curves[material][F_path][0.01]["std"]
            this_ax.plot(gamma_list, predict_mean_P_ele01, linewidth = 1, label = "predict P, ele01")


            # l1, = this_ax.plot(gamma_list, predict_mean_P_numExample_1, color = "lightskyblue", linewidth = 2, label = "predict P")
            # l2, = this_ax.plot(gamma_list, true_P_numExample_1, color = "xkcd:light lilac", linewidth = 1, label = 'true P')
            
            
            # # exterpolation            
            # gamma_list = exterpolation_gamma_curves[material][F_path]
            # true_P_numExample_1 = exterpolation_true_P_curves[material][F_path]
            # predict_mean_P_numExample_1 = exterpolation_predict_P_curves[material][F_path]["mean"]
            
            # # left
            # this_ax.plot(gamma_list[:args.gamma_steps // 2], predict_mean_P_numExample_1[:args.gamma_steps // 2], color = "lightskyblue",  linewidth = 2)
            # this_ax.plot(gamma_list[:args.gamma_steps // 2], true_P_numExample_1[:args.gamma_steps // 2], color = "xkcd:light lilac",  linewidth = 1)
            # # right
            # l4, = this_ax.plot(gamma_list[args.gamma_steps // 2:], predict_mean_P_numExample_1[args.gamma_steps // 2:], color = "lightskyblue",  linewidth = 2, label = "predict P ext")
            # l3, = this_ax.plot(gamma_list[args.gamma_steps // 2:], true_P_numExample_1[args.gamma_steps // 2:], color = "xkcd:light lilac",  linewidth = 1, label = "true P ext")
            
            # this_ax.legend()

        if material == "VK":
            y_material = "StVK"
        else:
            y_material = material
        axs[material_id, 0].set_ylabel("{}".format(y_material)+ "\n" + r'$P(\gamma)$' + " magnitude", fontweight="bold", fontsize=8, labelpad= 4)

    plt.rcParams.update({"axes.titleweight": "bold", "axes.titley": 1.05})
    axs[0, 0].set_title("UT", fontsize=8, pad= 3, loc= "center")
    axs[0, 1].set_title("BT", fontsize=8, pad= 3, loc= "center")
    axs[0, 2].set_title("SS", fontsize=8, pad= 3, loc= "center")

    axs[-1, 0].set_xlabel(r'$\gamma$', fontweight="bold", fontsize=8, labelpad= 4)
    axs[-1, 1].set_xlabel(r'$\gamma$', fontweight="bold", fontsize=8, labelpad= 4)
    axs[-1, 2].set_xlabel(r'$\gamma$', fontweight="bold", fontsize=8, labelpad= 4)
    """
    handles, labels = axs[-1, -1].get_legend_handles_labels()
    handles = [(l1,l4), (l2,l3) ]
    fig.legend(handles, labels, loc='upper left', handler_map = {tuple: matplotlib.legend_handler.HandlerTuple(None)}, handlelength=4, borderaxespad=0., bbox_to_anchor=(0.02, 0.93))    
    # https://stackoverflow.com/questions/9834452/how-do-i-make-a-single-legend-for-many-subplots
    # https://stackoverflow.com/questions/51598004/custom-legend-of-several-lines-with-two-markers-for-the-same-text/51598918#51598918
    # https://stackoverflow.com/questions/31478077/how-to-make-two-markers-share-the-same-label-in-the-legend
    """
    handles, labels = axs[-1, -1].get_legend_handles_labels()
    leg = fig.legend(handles, labels, loc='upper left', ncol=4, handlelength=2.4, borderaxespad=0., bbox_to_anchor=(0.13, 0.93))
    for legobj in leg.legend_handles:
        legobj.set_linewidth(1.5)



    plt.savefig("./figs/" + 'exp2_P.pdf', bbox_inches='tight', pad_inches = 0.03)
    plt.close()


if __name__ == "__main__":
    cuda_flag = False
    material_list = ["NH", "VK"]

    # interpolate_size_list = [0, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01]
    interpolate_size_list = [0, 0.005, 0.01]
    interpolate_size_list = sorted(interpolate_size_list, reverse= True)
    print("interpolate_size_list: ", interpolate_size_list)

    F_path_list = ["UT", "BT", "SS"]

    # define model
    energy_model = energy_density_NN(args.input_dim, args.hidden_dim, args.activation)    

    predict_W_curves = {}# predict_W_curves[material][F_path][ele_size]
    true_W_curves = {} # true_W_curves[material][F_path]
    predict_P_curves = {}
    true_P_curves = {}

    F_path_gamma_list = {} # F_path_gamma_list[material][F_path]
    
    # exterpolation_predict_W_curves = {} # for exterpolation
    # exterpolation_true_W_curves = {} # for exterpolation
    # exterpolation_predict_P_curves = {} # for exterpolation
    # exterpolation_true_P_curves = {} # for exterpolation

    # exterpolation_gamma_curves = {} # for exterpolation


    for material in material_list:
        print("material: ", material)
        if material == "NH":
            prefix = ""
        elif material == "VK":
            prefix = "VK_"
        else:
            raise RuntimeError("material invalid")
        
        # input is the original data, without coarse-graining
        input_data_dir = "../data_process/{}GNN_input_0.001/".format(prefix)
        

        predict_W_curves[material] = {} 
        true_W_curves[material] = {} 
        predict_P_curves[material] = {} # the magnitude of P
        true_P_curves[material] = {} # the magnitude of P
        F_path_gamma_list[material] = {}

        # exterpolation_predict_W_curves[material] = {} # for exterpolation
        # exterpolation_true_W_curves[material] = {} # for exterpolation
        # exterpolation_predict_P_curves[material] = {} # exterpolation, the magnitude of P
        # exterpolation_true_P_curves[material] = {} # exterpolation, the magnitude of P

        # exterpolation_gamma_curves[material] = {} # for exterpolation
              
        
        F_max, F_min = load_F()        

        

        for F_path in F_path_list:
            print("F_path: ", F_path)
            predict_W_curves[material][F_path] = {}
            # exterpolation_predict_W_curves[material][F_path] = {}
            predict_P_curves[material][F_path] = {}
            # exterpolation_predict_P_curves[material][F_path] = {}
            

            ## construct input
            gamma_list, F_numExample_4 = construct_testing_F(F_max, F_min, F_path)
            F_path_gamma_list[material][F_path] = gamma_list

            # # exterpolation input
            # exterpolation_gamma, exterpolation_F_numExample_4, left_tick, right_tick = construct_exterpolation_F(F_max, F_min, F_path)
            # exterpolation_gamma_curves[material][F_path] = exterpolation_gamma

            ## true W and P
            true_W_numExample_1, true_P_numExample_4 = cmpt_groundTruth_W_and_P(F_numExample_4)
            print("true_W_numExample_1 shape: ", true_W_numExample_1.shape)
            true_W_curves[material][F_path] = true_W_numExample_1
            true_P_curves[material][F_path] = np.linalg.norm(true_P_numExample_4, axis= -1, keepdims= True)

            # # true exterpolation W and P
            # exterpolation_true_W_numExample_1, exterpolation_true_P_numExample_4 = cmpt_groundTruth_W_and_P(exterpolation_F_numExample_4)
            # exterpolation_true_W_curves[material][F_path] = exterpolation_true_W_numExample_1
            # exterpolation_true_P_curves[material][F_path] = np.linalg.norm(exterpolation_true_P_numExample_4, axis= -1, keepdims= True)


            

            for ele_size in interpolate_size_list:
                predict_W_curves[material][F_path][ele_size] = {}
                predict_P_curves[material][F_path][ele_size] = {}

                if ele_size == 0:
                    exp_list = [1, 2]
                else:
                    exp_list = [1, 2, 3]

                ## predicted W and P in multiple experiments
                predict_W = []
                predict_P = []
                exterpolation_predict_W = []
                exterpolation_predict_P = []

                for exp in exp_list:                
                    # load trained model                    

                    saved_model_folder = "./trained_{}ele_0.001_float64/interpolate_{}/exp{}/".format(prefix, ele_size, exp)            
                    best_trained_model_file = saved_model_folder + "best_trained_model.pt"
                    energy_model.load_state_dict(torch.load(best_trained_model_file))
                    
                    with open(saved_model_folder + "greenStrain_max.pkl", "rb") as f:
                        greenStrain_max = pkl.load(f)
                    with open(saved_model_folder + "momentum_max.pkl", "rb") as f:
                        momentum_max_dict = pkl.load(f)
                        momentum_max = momentum_max_dict["momentum_max"]

                    
                    
                    # predicted W and P
                    predict_W_numExample_1, predict_P_numExample_4 = predict_W_and_P(F_numExample_4)
                    predict_W.append(predict_W_numExample_1)
                    predict_P.append(predict_P_numExample_4)

                    # # exterpolation predicted W and P
                    # exterpolation_predict_W_numExample_1, exterpolation_predict_P_numExample_4 = predict_W_and_P(exterpolation_F_numExample_4)
                    # exterpolation_predict_W.append(exterpolation_predict_W_numExample_1)
                    # exterpolation_predict_P.append(exterpolation_predict_P_numExample_4)


                # W
                predictW_exp_numExamples_1 = np.stack(predict_W, axis= 0)
                print("predictW_exp_numExamples_1 shape: ", predictW_exp_numExamples_1.shape)
                # exterpolation_predictW_exp_numExamples_1 = np.stack(exterpolation_predict_W, axis= 0) # exterpolation


                predict_W_curves[material][F_path][ele_size]["mean"] = np.mean(predictW_exp_numExamples_1, axis= 0)
                predict_W_curves[material][F_path][ele_size]["std"] = np.std(predictW_exp_numExamples_1, axis= 0)
                print("predict_W_curves[material][F_path][mean] shape: ", predict_W_curves[material][F_path][ele_size]["mean"].shape, material, F_path)
                print("predict_W_curves[material][F_path][std] shape: ", predict_W_curves[material][F_path][ele_size]["std"].shape, material, F_path)

                # exterpolation_predict_W_curves[material][F_path]["mean"] = np.mean(exterpolation_predictW_exp_numExamples_1, axis= 0) # exterpolation
                # exterpolation_predict_W_curves[material][F_path]["std"] = np.std(exterpolation_predictW_exp_numExamples_1, axis= 0) # exterpolation


                # P
                predictP_exp_numExamples_4 = np.stack(predict_P, axis= 0)
                # exterpolation_predictP_exp_numExamples_4 = np.stack(exterpolation_predict_P, axis= 0) # exterpolation
                assert predictP_exp_numExamples_4.shape[-1] == 4
                # assert exterpolation_predictP_exp_numExamples_4.shape[-1] == 4
                predict_Pmagnitude_exp_numExamples_1 = np.linalg.norm(predictP_exp_numExamples_4, axis= -1, keepdims= True)
                # exterpolation_predict_Pmagnitude_exp_numExamples_1 = np.linalg.norm(exterpolation_predictP_exp_numExamples_4, axis= -1, keepdims= True)

                predict_P_curves[material][F_path][ele_size]["mean"] = np.mean(predict_Pmagnitude_exp_numExamples_1, axis= 0)
                predict_P_curves[material][F_path][ele_size]["std"] = np.std(predict_Pmagnitude_exp_numExamples_1, axis= 0)

                # exterpolation_predict_P_curves[material][F_path]["mean"] = np.mean(exterpolation_predict_Pmagnitude_exp_numExamples_1, axis= 0) # exterpolation
                # exterpolation_predict_P_curves[material][F_path]["std"] = np.std(exterpolation_predict_Pmagnitude_exp_numExamples_1, axis= 0) # exterpolation

    # plot_W_fig()
    
    
    
    plot_P_fig()

