import os
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.colors as mcolors
from matplotlib.collections import LineCollection
from plot_helper_for_paper import set_size
plt.style.use(['prl_paper.mplstyle'])


# from matplotlib import rc,rcParams
# plt.rcParams["font.weight"] = "bold"
# plt.rcParams['xtick.major.pad']='1'
# plt.rcParams['ytick.major.pad']='0.5'
# plt.rc('xtick', labelsize=5)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=5)
# plt.rc('legend', fontsize=7)
# plt.rcParams['axes.linewidth'] = 0.1


import pickle as pkl
import sys

sys.path.append('..')

from GNN.model import energy_density_NN

import argparse


parser = argparse.ArgumentParser()


parser.add_argument('--num_nodes_per_element', type=int, default=3, help='Number of vertex on each element.') 
parser.add_argument('--space_dim', type=int, default=2, help='space dimension') 

parser.add_argument('--batch_size', type=int, default=1, help='Number of samples per batch.')
parser.add_argument('--input_dim', type=int, default=2, help='Two invariants.')
parser.add_argument('--hidden_dim', type=int, default=128, help='Hidden dimension.')
parser.add_argument('--activation', type=str, default='elu', help='[]')

# parser.add_argument('--material', type= str, default= "-1", help= "[AB, Fung, Gent, MR, NH, VK]")
# parser.add_argument('--F_path', type= str, default= "-1", help= "uniaxial tension (UT), uniaxial compression (UC), biaxial tension (BT), biaxial compression (BC), simple shear (SS) and pure shear (PS).")

parser.add_argument('--gamma_steps', type=int, default=200, help='For plotting')
parser.add_argument('--exterpolation_ratio', type= float, default=0.2, help= "for the exterpolation test")
args = parser.parse_args()


def cmpt_cauchy_green_deformation(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2

    C = torch.cat((C11,C12,C21,C22), dim=-1)
    return C



def cmpt_green_strain_tensor(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2
    
    E11 = C11 - 1.0
    E12 = C12
    E21 = C21
    E22 = C22 - 1.0
    return 0.5 * torch.cat((E11, E12, E21, E22), dim= -1)



def return_time_steps(dir):
    file_list = os.listdir(dir)
    used_time_steps = []
    for f_name in file_list:
        if f_name[0] != "a":
            continue
        f_name = f_name.split(".")[0]
        used_time_steps.append(int(f_name[2:]))
    return np.sort(used_time_steps)



def cmpt_groundTruth_W_and_P(F_numExample_4)  -> np.array:
    Youngs_modulus = 1e4
    nu= 0.3 # Poisson rate
    mu  = Youngs_modulus / (2.0*(1.0 + nu))
    lmbda = Youngs_modulus * nu / ((1.0 + nu)*(1.0 - 2.0*nu))
    bulk_K = Youngs_modulus / (3.0 * (1.0 - 2 * nu) ) #bulk modulus

    if material == "NH":
        # New-Hookean
        F_numExample_4.requires_grad = True
        C_numExample_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_numExample_1 = C_numExample_4[:, 0:1] + C_numExample_4[:, 3:4]
        J_numExample_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        W = (mu / 2) * (I1_numExample_1 - 2) - mu * torch.log(J_numExample_1) + (lmbda / 2) * (torch.log(J_numExample_1))**2 # 2D
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "VK":            
        F_numExample_4.requires_grad = True
        E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4)
        I1 = E_numExample_4[:, 0:1] + E_numExample_4[:, 3:4]
        I2 = E_numExample_4[:, 0:1] * E_numExample_4[:, 3:4] - E_numExample_4[:, 1:2] * E_numExample_4[:, 2:3]
        W = (lmbda /2 ) * I1 **2 + mu * (I1 **2 - 2 * I2)
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    
    elif material == "MR":
        C10 = 7. * mu / 16
        C01 = 1. * mu / 16
        
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I2_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        I1_bar = J_T_numE_1 ** (- 2. / 2.) * I1_T_numE_1
        I2_bar = J_T_numE_1 ** (- 4. / 2.) * I2_T_numE_1
        term1 = C10 * (I1_bar - 2)
        term2 = C01 * (I2_bar - 1)
        term3 = 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        W = term1 + term2 + term3
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Fung":
        b = 1.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        exponential = b * (I1_T_numE_1 * ( I3_T_numE_1 ** (-0.5) ) - 2)
        W = mu / (2 * b) * ( exponential + torch.exp(exponential) - 1.0 ) + 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Gent":
        Jm = 10
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        W_inc = - (mu / 2) * (Jm * torch.log( 1 - ( I3_T_numE_1 ** (-0.5) * I1_T_numE_1 - 2.0) / Jm )  )
        W_vol = 0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W_inc + W_vol
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "AB":
        C1 = mu
        N = 10.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_bar = I3_T_numE_1 ** (-0.5) * I1_T_numE_1
        W1 = C1 * ( 0.5 * (I1_bar - 2) + (1 / (20 * N) ) * (I1_bar**2  - 4) + (11 / (N ** 2 * 1050 ) ) * (I1_bar**3  - 8) + (19 / (N ** 3 * 7000 ) ) * (I1_bar**4  - 16) + (519 / (N ** 4 * 673750 ) ) * (I1_bar**5  - 32) )
        W2 =  0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W1 + W2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    else:
        raise RuntimeError("material error")
    
    return W.detach().numpy(), P_numExample_4.detach().numpy()


def predict_W_and_P(F_numExample_4) -> np.array:
    zero_strain_1_4 = torch.zeros((1, 4) )
    if cuda_flag:
        zero_strain_1_4 = zero_strain_1_4.cuda()
    zero_strain_1_4.requires_grad = True
    


    ## input data
    E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4) # green strain tensor
    E_numExample_4 = torch.autograd.Variable(E_numExample_4)
    E_numExample_4.requires_grad = True

    # normalize INPUT Green strain
    normalizedE_numExample_4 = E_numExample_4 / greenStrain_max

    energy_numExample_1 = energy_model(normalizedE_numExample_4)
    S_numExample_4 = torch.autograd.grad(energy_numExample_1.sum(), E_numExample_4, create_graph=True)[0]

    ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
    zeroEnergy_1_1 = energy_model(zero_strain_1_4)
    zeroS_1_4  = torch.autograd.grad(zeroEnergy_1_1.sum(), zero_strain_1_4, create_graph = True)[0]
    zeroS_1_2_2 = zeroS_1_4.reshape(1, 2, 2)
    zeroP_1_2_2 = zeroS_1_2_2 # because of the unit_F_1_1_2_2
    
    # energy
    energy_numExample_1 = energy_numExample_1 - zeroEnergy_1_1
    

    ## correct the predicted stress
    S_numE_2_2 = S_numExample_4.reshape(S_numExample_4.shape[0],  2, 2) # last two dimensions are i and j. sum over j
    F_numE_2_2 = F_numExample_4.reshape(F_numExample_4.shape[0], 2, 2)
    P_numE_2_2 = torch.matmul(F_numE_2_2, S_numE_2_2)
    P_numE_2_2 = P_numE_2_2 - zeroP_1_2_2
    P_numExample_4 = P_numE_2_2.reshape(P_numE_2_2.shape[0], 4)
    
    # unnormalize predictions
    P_numExample_4 = P_numExample_4 * momentum_max
    energy_numExample_1 = energy_numExample_1 * momentum_max
    
    # return energy_numExample_1.detach().numpy() * greenStrain_max, P_numExample_4.detach().numpy()
    return energy_numExample_1.cpu().detach().numpy(), P_numExample_4.cpu().detach().numpy()



### color map
def make_colormap(seq):
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return mcolors.LinearSegmentedColormap('CustomMap', cdict)


def get_border_edges_rectangle(triang):
    border_edges = []
    for i in range(len(triang.edges) ):
        this_v1 = triang.edges[i, 0]
        this_v2 = triang.edges[i, 1]
        if vertex_coordinates_N_2[this_v1, 1] == 0 and vertex_coordinates_N_2[this_v2, 1] == 0:            
            border_edges.append([this_v1, this_v2])
        elif vertex_coordinates_N_2[this_v1, 1] == 0.4 and vertex_coordinates_N_2[this_v2, 1] == 0.4:
            border_edges.append([this_v1, this_v2])
        
        elif vertex_coordinates_N_2[this_v1, 0] == 0 and vertex_coordinates_N_2[this_v2, 0] == 0:
            border_edges.append([this_v1, this_v2])
        
        elif vertex_coordinates_N_2[this_v1, 0] == 1.0 and vertex_coordinates_N_2[this_v2, 0] == 1.0:
            border_edges.append([this_v1, this_v2])        
        
    return border_edges


def plot_colorbar(fig, ax, cs, pos):

    # axins1 = ax.inset_axes([0.1, 1.085, 0.8, 0.05])
    # axins1 = ax.inset_axes(pos)
    axins1 = fig.add_axes(pos)

    cbar = fig.colorbar(cs, cax=axins1, pad=0.05, orientation="vertical")
    cbar.ax.yaxis.set_tick_params(pad=1)

    tick_locator = matplotlib.ticker.MaxNLocator(nbins=4)
    cbar.locator = tick_locator
    cbar.ax.yaxis.set_major_locator(tick_locator)
    cbar.update_ticks()
    # cbar.ax.yaxis.set_offset_position('left')  

    formatter = matplotlib.ticker.ScalarFormatter(useMathText= True, useOffset=False)
    # formatter.set_powerlimits((-1, 1))
    formatter.set_powerlimits((0, 0))
    cbar.ax.yaxis.set_major_formatter(formatter)

    plt.gcf().canvas.draw()
    offset_text = cbar.ax.yaxis.get_offset_text().get_text()
    cbar.ax.get_yaxis().get_offset_text().set_visible(False)
    cbar.ax.text(0.7, 1.01, offset_text, transform=cbar.ax.transAxes, ha='left', va='baseline')

    return cbar


# def plot_colorbar2(fig, ax, tpc_predict):
#     ax2 = ax.twinx()
#     ax2.tick_params(which="both", right=False, labelright=False, rotation=90)
#     cbar = fig.colorbar(tpc_predict,  ax= ax2)
#     plt.setp(cbar.ax.get_yticklabels(), rotation=90, ha="left", rotation_mode="anchor", fontsize = 5.5)
#     cbar.ax.tick_params(pad=5)
#     extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
#     ax2.axis('off')


def plot_W_field():
    num_time_steps = len(used_time_steps)
    num_ele_size = len(interpolate_size_list)
    print("In plot_W_field(), num_time_steps: ", num_time_steps, " num_ele_size: ", num_ele_size)
    num_rows = num_time_steps * num_ele_size
    fig_width = 1.5
    height_unit = 0.6
    fig_hight = height_unit * num_rows #+ 0.2

    fig, axs = plt.subplots(1, num_rows, figsize=set_size(width='two-column', fraction=0.4, height_ratio= 0.67), ) # layout="constrained"

    c = mcolors.ColorConverter().to_rgb
    min_rgb_value = np.array(c('cornflowerblue')) # yellow, gold,  goldenrod, 
    max_rgb_value = np.array(c('gold'))# maroon, brown, tomato, darkorange, orangered, sienna, chocolate, saddlebrown, khaki
    energy_cmap = make_colormap([tuple(min_rgb_value), tuple(min_rgb_value/2.0 + max_rgb_value/2.0), 0.5, tuple(min_rgb_value/2.0 + max_rgb_value/2.0), tuple(max_rgb_value)])


    # difference colormap
    min_rgb_value = np.array(c('white')) # yellow, gold,  goldenrod, 
    max_rgb_value = np.array(c('xkcd:dark grey'))# maroon, brown, tomato, darkorange, orangered, sienna, chocolate, saddlebrown, khaki, fuchsia, #ffa600
    # max_rgb_value = np.array([173., 46., 182.]) / 255.
    difference_cmap = make_colormap([tuple(min_rgb_value), tuple(min_rgb_value/2.0 + max_rgb_value/2.0), 0.5, tuple(min_rgb_value/2.0 + max_rgb_value/2.0), tuple(max_rgb_value)])



    count_row = 0
    lineCollection_linewidth = 0.1

    for time_id in range(num_time_steps):
        this_time = used_time_steps[time_id]

        vertexPos_N_2 = vertex_coordinates_N_2 + displacement_fields[this_time]

        ## construct the tri.Triangulation
        # triang = tri.Triangulation(vertexPos_N_2[:, 0], vertexPos_N_2[:, 1], ele_node_array_numE_3 )
        # x_pos_min = np.min(vertexPos_N_2[:, 0])
        # x_pos_max = np.max(vertexPos_N_2[:, 0])
        rotate_vertexPos_N_2 = np.zeros(shape= vertexPos_N_2.shape)
        rotate_vertexPos_N_2[:, 0] = -  vertexPos_N_2[:, 1]
        rotate_vertexPos_N_2[:, 1] =  vertexPos_N_2[:, 0]
        triang = tri.Triangulation(rotate_vertexPos_N_2[:, 0], rotate_vertexPos_N_2[:, 1], ele_node_array_numE_3 )

        border_edges  = get_border_edges_rectangle(triang)

        
        # the maximum and minimum of the difference        
        difference_min = 0
        difference_max = -np.inf
        for ele_size in interpolate_size_list:            
            true_W_numEle_1 = true_W_fields[ele_size][this_time][:, 0]
            predict_W_numEle_1 = predict_W_fields[ele_size][this_time][:, 0]
            if normalize_flag:
                # W_scale = np.mean(np.abs(true_W_numEle_1))
                W_scale = np.abs(true_W_numEle_1)
                print("W_scale: ", W_scale)
                this_max = np.max(np.abs(predict_W_numEle_1 - true_W_numEle_1) / W_scale )
            else:
                this_max = np.max(np.abs(predict_W_numEle_1 - true_W_numEle_1) )
            if this_max > difference_max:
                difference_max = this_max

        # plot the difference
        for ele_size in interpolate_size_list:

            ## W
            true_W_numEle_1 = true_W_fields[ele_size][this_time][:, 0]
            predict_W_numEle_1 = predict_W_fields[ele_size][this_time][:, 0]

            print("true_W_numEle_1 shape", true_W_numEle_1.shape)
            print("predict_W_numEle_1 shape", predict_W_numEle_1.shape)


            # difference
            axs[count_row].set_aspect('auto')
            # axs[count_row].set(xlim=(x_pos_min - 0.02 , x_pos_max + 0.02))        
            axs[count_row].axes.get_xaxis().set_ticks([])
            axs[count_row].axes.get_yaxis().set_ticks([])

            if normalize_flag == True:
                # W_diff = np.abs(predict_W_numEle_1 - true_W_numEle_1) / np.mean(np.abs(true_W_numEle_1))
                W_diff = np.abs(predict_W_numEle_1 - true_W_numEle_1) / np.abs(true_W_numEle_1)
            else:
                W_diff = np.abs(predict_W_numEle_1 - true_W_numEle_1)

            tpc_predict = axs[count_row].tripcolor(triang, W_diff, vmin= difference_min, vmax= difference_max, shading='flat', antialiased=True, linewidth=0.5, edgecolors='face', cmap= difference_cmap, rasterized=True)

            lc = LineCollection(rotate_vertexPos_N_2[border_edges], linewidths= lineCollection_linewidth, colors= "black")
            axs[count_row].add_collection(lc)

                        

            # axs[count_row].axis('off')
            axs[count_row].spines['top'].set_visible(False)
            axs[count_row].spines['right'].set_visible(False)
            axs[count_row].spines['bottom'].set_visible(False)
            axs[count_row].spines['left'].set_visible(False)


            

            

            count_row += 1
        
        fig.canvas.draw()
        ax_pos = axs[2].get_position()            
        right, bottom, width, height  = ax_pos.x1, ax_pos.y0, ax_pos.width, ax_pos.height
        cbar = plot_colorbar(fig, axs[2], tpc_predict, [right * 1.02, bottom*1.3 , height * 0.02, 0.7])  
        # plot_colorbar2(fig, axs[2], tpc_predict)

    if normalize_flag == True:
        # axs[0].set_ylabel(r'${|\widehat{W} - W|}~ /~ {|W|}$', rotation=90, fontsize =8, labelpad= 1)
        cbar.set_label(r'${|\widehat{W} - W|}~ /~ {|W|}$', rotation=90, fontsize =8, labelpad= 1)
    else:
        # axs[0].set_ylabel(r'$|\widehat{W} - W|$', rotation=90, fontsize = 8, labelpad= 0.2)
        cbar.set_label(r'$|\widehat{W} - W|$', rotation=90, fontsize = 8, labelpad= 0.2)



    # axs[0].set_xlabel( r'$h_{\text{gen}} / h=0.1$', labelpad= 0.5, fontsize=8)
    # axs[1].set_xlabel( r'$h_{\text{gen}} / h=0.2$', labelpad= 0.5, fontsize=8)
    # axs[2].set_xlabel( r'$h_{\text{gen}} / h=0.5$', labelpad= 0.5, fontsize=8)
    axs[0].set_ylabel( r'$y$', labelpad= 0.5, fontsize=8)
    axs[0].set_xlabel( r'$x$', labelpad= -3, fontsize=8)
    axs[1].set_xlabel( r'$x$', labelpad= -3, fontsize=8)
    axs[2].set_xlabel( r'$x$', labelpad= -3, fontsize=8)

    axs[0].set_title( r'$h_{\text{gen}} / h=0.1$', y =0.96, fontsize=8)
    axs[1].set_title( r'$h_{\text{gen}} / h=0.2$', y =0.96, fontsize=8)
    axs[2].set_title( r'$h_{\text{gen}} / h=0.5$', y =0.96, fontsize=8)

    plt.subplots_adjust(wspace=0.01) 

    plt.savefig(save_fig_folder + '{}_W_field_difference.pdf'.format(material), bbox_inches='tight', pad_inches = 0.03)
    # plt.savefig(save_fig_folder + '{}_W_field_difference.png'.format(material), bbox_inches='tight', pad_inches = 0.03, dpi=300, format='png')
    plt.close()






if __name__ == "__main__":

    cuda_flag = False
    # material_list = ["NH", "VK"]
    material = "NH"
    if material == "NH":
        prefix = "NH_"
    elif material == "VK":
        prefix = "VK_"
    else:
        raise RuntimeError("material invalid")

    interpolate_size_list = [0.002, 0.005, 0.01]
    interpolate_size_list = sorted(interpolate_size_list, reverse= True)
    print("interpolate_size_list: ", interpolate_size_list)

    normalize_flag = True

    sim_case = 1

    # time_step_id_list = [0, 1, 15]
    # time_step_id_list = [0, 15, 200]
    time_step_id_list = [0]
    print("time_step_id_list: ", time_step_id_list)

    exp_id = 1 # or compute the mean from multiple experiments

    true_W_fields = {} # true_W_fields[time_step] is np.array() with shape (num_ele, 1)
    predict_W_fields = {}# predict_W_fields[time_step] is np.array() with shape (num_ele, 1)
    true_P_fields = {} # true_P_fields[time_step] is np.array() with shape (num_ele, 4)
    predict_P_fields = {}# true_P_fields[time_step] is np.array() with shape (num_ele, 4)

    displacement_fields = {}# displacement_fields[time_step] is np.array() with shape (num_node, 2), for constructing triangle position
    used_time_steps = []

    # node coordinate in the reference frame
    simulation_folder = "../../Experiment1_different_material_backup/stress_field_visualization/case{}/NH_data_0.01/".format(sim_case)
    GNN_input_folder = "../../Experiment1_different_material_backup/stress_field_visualization/case{}/NH_data_0.01_F/".format(sim_case)

    save_fig_folder = "./case{}/".format(sim_case)

    vertex_coordinates_N_2 = np.load(simulation_folder + "coordinates.npy")


    # connectivity, for constructing tri.Triangulation
    with open(simulation_folder + "mesh_info.pkl", "rb") as f:
        mesh_info = pkl.load(f)
        assert (vertex_coordinates_N_2 == mesh_info["coordinates"] ).all()
        ele_node_array_numE_3 = mesh_info["ele_node_array_numE_3"].astype(np.int64)
    
    # deformation gradient 
    with open(GNN_input_folder + "deformation_gradient.pkl", "rb") as f:
        F_T_numEle_4 = pkl.load(f) 
    assert len(F_T_numEle_4.shape) == 3
    

    
    # all available time steps
    available_time_steps = return_time_steps(simulation_folder)
    assert F_T_numEle_4.shape[0] == len(available_time_steps)


    energy_model = energy_density_NN(args.input_dim, args.hidden_dim, args.activation)    



    for ele_size in interpolate_size_list:
        true_W_fields[ele_size] = {}
        predict_W_fields[ele_size] = {}
        true_P_fields[ele_size] = {}
        predict_P_fields[ele_size] = {}

        ## the trained model
        saved_model_folder = "../GNN/trained_{}ele_0.001_float64/interpolate_{}/exp{}/".format(prefix, ele_size, exp_id)            
        best_trained_model_file = saved_model_folder + "best_trained_model.pt"
        energy_model.load_state_dict(torch.load(best_trained_model_file))
        
        with open(saved_model_folder + "greenStrain_max.pkl", "rb") as f:
            greenStrain_max = pkl.load(f)
        with open(saved_model_folder + "momentum_max.pkl", "rb") as f:
            momentum_max_dict = pkl.load(f)
            momentum_max = momentum_max_dict["momentum_max"]


        ### check every time step
        for time_id in time_step_id_list:
            this_time = available_time_steps[time_id]
            if ele_size == interpolate_size_list[0]:
                used_time_steps.append(this_time)
                print("ele_size: ", ele_size, " time_id: ", time_id, " this_time: ", this_time)

            u_N_2 = np.load(simulation_folder + "u_{}.npy".format(this_time )  )
            # currentPos_N_2 = vertex_coordinates_N_2 + u_N_2
            if ele_size == interpolate_size_list[0]:
                displacement_fields[this_time] = u_N_2

            F_numEle_4 = F_T_numEle_4[time_id] # note the time_id


            # W and P, can also be mean here
            true_W_numEle_1, true_P_numEle_4 = cmpt_groundTruth_W_and_P(F_numEle_4)

            predict_W_numEle_1, predict_P_numEle_4 = predict_W_and_P(F_numEle_4)

            # predict_W_mean = np.stack(predict_W_mean)
            # predict_W_mean = np.mean(predict_W_mean, axis= 0)
            assert len(predict_W_numEle_1.shape) == 2
            assert predict_W_numEle_1.shape[-1] == 1
            # predict_P_mean = np.stack(predict_P_mean)
            # predict_P_mean = np.mean(predict_P_mean, axis= 0)
            assert len(predict_P_numEle_4.shape) == 2
            assert predict_P_numEle_4.shape[-1] == 4


            true_W_fields[ele_size][this_time] = true_W_numEle_1
            predict_W_fields[ele_size][this_time] = predict_W_numEle_1
            true_P_fields[ele_size][this_time] =  np.linalg.norm(true_P_numEle_4, axis= -1, keepdims= True) ## NOTE: norm of the stress
            predict_P_fields[ele_size][this_time] =  np.linalg.norm(predict_P_numEle_4, axis= -1, keepdims= True) ## NOTE: norm of the stress

    
        
            
    plot_W_field()
    
    
    
    # plot_P_field()

