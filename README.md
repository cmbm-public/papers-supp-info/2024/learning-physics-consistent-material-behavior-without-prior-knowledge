# Learning Physics-consistent Material Behavior Without Prior Knowledge

This repository contains the source code for the paper *Learning Physics-consistent Material Behavior Without Prior Knowledge*.
In the paper, we introduce the `uLED` method, which stands for <ins>u</ins>nsupervised <ins>l</ins>earning of hyper<ins>e</ins>lastic constitutive relations in <ins>d</ins>ynamic environments.

## Authors
Zhichao Han, Mohit Pundir, Olga Fink, David S. Kammer


## Getting started

### Requirements

The simulation part of data generation relies on `FEniCS`. The machine learning part relies on `PyTorch`.  
Specifically, we use the following versions:    

- Python==3.9.15  
- Pytorch==1.13.1  
- Fenics==2019.1.0   

We recommend using conda to install the environment. For example:

```bash
# create new environment named uLED
conda create --name uLED python=3.9.15
conda activate uLED
# install pytorch
conda install pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.7 -c pytorch -c nvidia
# install fenics
conda install -c conda-forge fenics
# Theoretically, fenics should be able to imported in python by now. 
# However, in case you meet the error with libsuperlu_dist.so when importing fenics in python, install the superlu_dist additionally by:
conda install -c conda-forge superlu_dist=6.2.0 
```
Please note there could be a warning about the version of `mpi4py` used by `fenics`. In our experiments, the stated incompatibility did not cause any error in the simulation.  


### Hardware of our experiments

The FEM simulation is conducted on a CPU server with multiple cores.  

To train `uLED` to learn the constitutive relation, we assume GPU is available. We conduct the experiments on a Linux (Ubuntu 20.04) server with RTX 3090.  


### Example: learn the Neo-Hookean constitutive relation

The `Example` folder shows the example to train `uLED` to learn the Neo-Hookean constitutive relation from scratch.  


Structure of `Example`:  

* Example/
  * generate_data/
    * generate simulation data at the fine resolution, using fenics
  * data_process/
    * process the generated simulation data to the input of uLED, at coarse resolution
  * uLED/
    * machine learning code, including training and testing

You can following the following three steps to generate data and train the neural network:  
(Make sure the uLED environment is installed and activated)  

1. Generate simulation and mesh info:  
   
   ```bash
   cd Example/generate_data/
   python fenics_simulation.py --material "NH" --ele_size 0.01 # this mesh size is only for demonstration. We use 0.001 for the simulation in the paper.
   python fenics_simulation_mesh.py --ele_size 0.01
   ```
   
   After this, you should see two new folders named `NH_data_0.01` and `mesh_0.01` in `generate_data/`  

2. Process the simulation and generate input for training the neural network
   
   ```bash
   cd Example/data_process/
   python interpolation_generate_GNN_input.py --material "NH" --fine_ele_size 0.01 --coarse_ele_size 0.02
   ```
   
   Here, we sample the dynamics at the coarse data resolution 1/h, i.e., 1/h = 1/(2 h_gen).  
   After this, you should see a new folder named `NH_GNN_input_interpolation_0.01/0.02/` in `data_process/` folder  

3. Train and test the neural network
   
   ```bash
   cd Example/GNN/
   # training, independent experiment 1
   CUDA_VISIBLE_DEVICES=0 python training_sparse.py --input_data_dir "../data_process/NH_GNN_input_interpolation_0.01/0.02/" --save_folder "./trained_NH_ele_0.01_float64/interpolate_0.02/" # will save the trained model in "./trained_NH_ele_0.01_float64/interpolate_0.02/exp1/"
   # training, independent experiment 2
   CUDA_VISIBLE_DEVICES=0 python training_sparse.py --input_data_dir "../data_process/NH_GNN_input_interpolation_0.01/0.02/" --save_folder "./trained_NH_ele_0.01_float64/interpolate_0.02/" # will save the trained model in "./trained_NH_ele_0.01_float64/interpolate_0.02/exp2/"
   ...
   # test
   CUDA_VISIBLE_DEVICES=0 python test_sparse.py --material "NH"
   ```
   
   You can see the training log and test results, for example, in `./trained_NH_ele_0.01_float64/interpolate_0.02/exp1/`  


## Reproduce experiments in the paper

The folders `Experiment_various_materials`, `Experiment_convergence`, `Experiment_partial_observation`, `Experiment_noise` and `Experiment_damping` contains the experiements in the paper.  

You can follow the instructions in the README in each folder, to reproduce the results in the paper.

<!-- 
### Reference

If our work was useful to your research, please cite:

```
Copy the bibtex item here
``` -->

## License

MIT License