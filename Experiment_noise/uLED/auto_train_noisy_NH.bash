CUDA_VISIBLE_DEVICES=2 python training_sparse_noisy.py --input_data_dir "../data_process/NH_GNN_input_interpolation_0.001/0.002/" --save_folder "./NH_trained_float64/" --noisy_level 1E-5
CUDA_VISIBLE_DEVICES=2 python training_sparse_noisy.py --input_data_dir "../data_process/NH_GNN_input_interpolation_0.001/0.002/" --save_folder "./NH_trained_float64/" --noisy_level 1E-6
CUDA_VISIBLE_DEVICES=2 python training_sparse_noisy.py --input_data_dir "../data_process/NH_GNN_input_interpolation_0.001/0.002/" --save_folder "./NH_trained_float64/" --noisy_level 1E-7
CUDA_VISIBLE_DEVICES=2 python training_sparse_noisy.py --input_data_dir "../data_process/NH_GNN_input_interpolation_0.001/0.002/" --save_folder "./NH_trained_float64/" --noisy_level 1E-8
