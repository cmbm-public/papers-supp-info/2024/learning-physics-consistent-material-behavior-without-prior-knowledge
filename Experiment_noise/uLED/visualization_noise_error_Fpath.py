## adapted from Experiment1_different_material_new/GNN/W_P_error_Fpath.py
## plot the W error with different h, for NH

import os
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc,rcParams
from matplotlib.ticker import FormatStrFormatter
# plt.rcParams["font.weight"] = "bold"
# plt.rcParams['xtick.major.pad']='1'
# plt.rcParams['ytick.major.pad']='0.5'
# plt.rc('xtick', labelsize=6)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=6)
# plt.rc('legend', fontsize=8)
# plt.rcParams["axes.titlepad"] = 8

plt.style.use(['./figs/prl_paper.mplstyle'])
from figs.plot_helper_for_paper import set_size

import pickle as pkl

from model import energy_density_NN

import argparse


parser = argparse.ArgumentParser()


parser.add_argument('--num_nodes_per_element', type=int, default=3, help='Number of vertex on each element.') 
parser.add_argument('--space_dim', type=int, default=2, help='space dimension') 

parser.add_argument('--batch_size', type=int, default=1, help='Number of samples per batch.')
parser.add_argument('--input_dim', type=int, default=2, help='Two invariants.')
parser.add_argument('--hidden_dim', type=int, default=128, help='Hidden dimension.')
parser.add_argument('--activation', type=str, default='elu', help='[]')

# parser.add_argument('--material', type= str, default= "-1", help= "[AB, Fung, Gent, MR, NH, VK]")
# parser.add_argument('--F_path', type= str, default= "-1", help= "uniaxial tension (UT), uniaxial compression (UC), biaxial tension (BT), biaxial compression (BC), simple shear (SS) and pure shear (PS).")

parser.add_argument('--gamma_steps', type=int, default=200, help='For plotting')
parser.add_argument('--exterpolation_ratio', type= float, default=0.2, help= "for the exterpolation test")
args = parser.parse_args()


def cmpt_cauchy_green_deformation(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2

    C = torch.cat((C11,C12,C21,C22), dim=-1)
    return C



def cmpt_green_strain_tensor(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2
    
    E11 = C11 - 1.0
    E12 = C12
    E21 = C21
    E22 = C22 - 1.0
    return 0.5 * torch.cat((E11, E12, E21, E22), dim= -1)



def load_F():
    def get_max_min(x):
        x = np.sort(x)
        min_id = int(len(x) * 0.005)
        max_id = int(len(x) * 0.005)
        return x[min_id], x[-max_id]
    # check the max / min values in F
    with open(input_data_dir + "deformation_gradient.pkl", "rb") as f:
        F_numExample_4 = pkl.load(f) 
    F_numExample_4 = F_numExample_4.numpy()
    num_time_steps = int(F_numExample_4.shape[0] * 0.8)
    F_numExample_4 = F_numExample_4[0:num_time_steps, :, :]
    print("F_numExample_4 shape: ", F_numExample_4.shape)

    F11_min, F11_max = get_max_min(F_numExample_4[:, :, 0].flatten())
    F12_min, F12_max = get_max_min(F_numExample_4[:, :, 1].flatten())
    F21_min, F21_max = get_max_min(F_numExample_4[:, :, 2].flatten())
    F22_min, F22_max = get_max_min(F_numExample_4[:, :, 3].flatten())
    
    # F_max = np.max(F_numExample_4, axis=(0, 1) )
    F_max = (F11_max, F12_max, F21_max, F22_max)
    # print("F_max")
    # print(F_max)
    # F_min = np.min(F_numExample_4, axis=(0, 1) )
    F_min = (F11_min, F12_min, F21_min, F22_min)
    # print("F_min")
    # print(F_min)
    return F_max, F_min


def construct_testing_F(F_max, F_min, F_path):
    F11_max, F12_max, F21_max, F22_max = F_max
    print(F11_max, F12_max, F21_max, F22_max)
    F11_min, F12_min, F21_min, F22_min = F_min
    print(F11_min, F12_min, F21_min, F22_min)

    F_numExample_4 = torch.zeros((args.gamma_steps, 4) ) # to return
    
    if F_path == "UT":
        F11_list = np.linspace(F11_min, F11_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_list[i]
            F_numExample_4[i, 3] = 1.0
        return F11_list - 1.0, F_numExample_4
    
    elif F_path == "BT":
        F11_22_min = np.max((F11_min, F22_min) )
        F11_22_max = np.min((F11_max, F22_max) )
        F11_22_list = np.linspace(F11_22_min, F11_22_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_22_list[i]
            F_numExample_4[i, 3] = F11_22_list[i]
        return F11_22_list - 1.0, F_numExample_4
    
    elif F_path == "SS":
        F12_list = np.linspace(F12_min, F12_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = 1.0
            F_numExample_4[i, 1] = F12_list[i]
            F_numExample_4[i, 3] = 1.0
        return F12_list, F_numExample_4
        
    else:
        raise RuntimeError("F_path invalid")
    
def construct_exterpolation_F(F_max, F_min, F_path):
    F11_max, F12_max, F21_max, F22_max = F_max
    print(F11_max, F12_max, F21_max, F22_max)
    F11_min, F12_min, F21_min, F22_min = F_min
    print(F11_min, F12_min, F21_min, F22_min)

    F_numExample_4 = torch.zeros((args.gamma_steps, 4) ) # to return
    
    if F_path == "UT":
        F11_min_extended = 1.0 -  (1+ args.exterpolation_ratio) * abs(F11_min - 1.0)
        F11_max_extended = 1.0 +  (1+ args.exterpolation_ratio) * abs(F11_max - 1.0)
        assert args.gamma_steps % 2 == 0
        left_F11_list = np.linspace(F11_min_extended, F11_min, args.gamma_steps // 2)
        right_F11_list = np.linspace(F11_max, F11_max_extended, args.gamma_steps // 2)

        F11_list = np.concatenate((left_F11_list, right_F11_list))

        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_list[i]
            F_numExample_4[i, 3] = 1.0
        return F11_list - 1.0, F_numExample_4, F11_min - 1.0, F11_max - 1.0
    

    elif F_path == "BT":
        F11_22_min = np.max((F11_min, F22_min) )
        F11_22_max = np.min((F11_max, F22_max) )
        F11_22_min_extended = 1.0 - (1 + args.exterpolation_ratio) * abs(F11_22_min - 1.0)
        F11_22_max_extended = 1.0 + (1 + args.exterpolation_ratio) * abs(F11_22_max - 1.0)
        assert args.gamma_steps % 2 == 0
        left_F11_22_list = np.linspace(F11_22_min_extended, F11_22_min, args.gamma_steps // 2)
        right_F11_22_list = np.linspace(F11_22_max, F11_22_max_extended, args.gamma_steps // 2)

        F11_22_list = np.concatenate((left_F11_22_list, right_F11_22_list ) )

        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_22_list[i]
            F_numExample_4[i, 3] = F11_22_list[i]
        return F11_22_list - 1.0, F_numExample_4, F11_22_min - 1.0, F11_22_max - 1.0
    

    elif F_path == "SS":
        F12_min_extended = (1 + args.exterpolation_ratio) * F12_min
        F12_max_extended = (1 + args.exterpolation_ratio) * F12_max
        assert args.gamma_steps % 2 == 0
        F12_list_left = np.linspace(F12_min_extended, F12_min, args.gamma_steps // 2)
        F12_list_right = np.linspace(F12_max, F12_max_extended, args.gamma_steps // 2)

        F12_list = np.concatenate((F12_list_left, F12_list_right ) )
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = 1.0
            F_numExample_4[i, 1] = F12_list[i]
            F_numExample_4[i, 3] = 1.0
        return F12_list, F_numExample_4, F12_min, F12_max
        
    else:
        raise RuntimeError("F_path invalid")



def cmpt_groundTruth_W_and_P(F_numExample_4)  -> np.array:
    Youngs_modulus = 1e4
    nu= 0.3 # Poisson rate
    mu  = Youngs_modulus / (2.0*(1.0 + nu))
    lmbda = Youngs_modulus * nu / ((1.0 + nu)*(1.0 - 2.0*nu))
    bulk_K = Youngs_modulus / (3.0 * (1.0 - 2 * nu) ) #bulk modulus

    if material == "NH":
        # New-Hookean
        F_numExample_4.requires_grad = True
        C_numExample_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_numExample_1 = C_numExample_4[:, 0:1] + C_numExample_4[:, 3:4]
        J_numExample_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        W = (mu / 2) * (I1_numExample_1 - 2) - mu * torch.log(J_numExample_1) + (lmbda / 2) * (torch.log(J_numExample_1))**2 # 2D
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "StVK":            
        F_numExample_4.requires_grad = True
        E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4)
        I1 = E_numExample_4[:, 0:1] + E_numExample_4[:, 3:4]
        I2 = E_numExample_4[:, 0:1] * E_numExample_4[:, 3:4] - E_numExample_4[:, 1:2] * E_numExample_4[:, 2:3]
        W = (lmbda /2 ) * I1 **2 + mu * (I1 **2 - 2 * I2)
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    
    elif material == "MR":
        C10 = 7. * mu / 16
        C01 = 1. * mu / 16
        
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I2_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        I1_bar = J_T_numE_1 ** (- 2. / 2.) * I1_T_numE_1
        I2_bar = J_T_numE_1 ** (- 4. / 2.) * I2_T_numE_1
        term1 = C10 * (I1_bar - 2)
        term2 = C01 * (I2_bar - 1)
        term3 = 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        W = term1 + term2 + term3
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Fung":
        b = 1.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        exponential = b * (I1_T_numE_1 * ( I3_T_numE_1 ** (-0.5) ) - 2)
        W = mu / (2 * b) * ( exponential + torch.exp(exponential) - 1.0 ) + 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Gent":
        Jm = 10
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        W_inc = - (mu / 2) * (Jm * torch.log( 1 - ( I3_T_numE_1 ** (-0.5) * I1_T_numE_1 - 2.0) / Jm )  )
        W_vol = 0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W_inc + W_vol
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "AB":
        C1 = mu
        N = 10.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_bar = I3_T_numE_1 ** (-0.5) * I1_T_numE_1
        W1 = C1 * ( 0.5 * (I1_bar - 2) + (1 / (20 * N) ) * (I1_bar**2  - 4) + (11 / (N ** 2 * 1050 ) ) * (I1_bar**3  - 8) + (19 / (N ** 3 * 7000 ) ) * (I1_bar**4  - 16) + (519 / (N ** 4 * 673750 ) ) * (I1_bar**5  - 32) )
        W2 =  0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W1 + W2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    else:
        raise RuntimeError("material error")
    
    return W.detach().numpy(), P_numExample_4.detach().numpy()


def predict_W_and_P(F_numExample_4) -> np.array:
    zero_strain_1_4 = torch.zeros((1, 4) )
    if cuda_flag:
        zero_strain_1_4 = zero_strain_1_4.cuda()
    zero_strain_1_4.requires_grad = True
    


    ## input data
    E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4) # green strain tensor
    E_numExample_4 = torch.autograd.Variable(E_numExample_4)
    E_numExample_4.requires_grad = True

    # normalize INPUT Green strain
    normalizedE_numExample_4 = E_numExample_4 / greenStrain_max

    energy_numExample_1 = energy_model(normalizedE_numExample_4)
    S_numExample_4 = torch.autograd.grad(energy_numExample_1.sum(), E_numExample_4, create_graph=True)[0]

    ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
    zeroEnergy_1_1 = energy_model(zero_strain_1_4)
    zeroS_1_4  = torch.autograd.grad(zeroEnergy_1_1.sum(), zero_strain_1_4, create_graph = True)[0]
    zeroS_1_2_2 = zeroS_1_4.reshape(1, 2, 2)
    zeroP_1_2_2 = zeroS_1_2_2 # because of the unit_F_1_1_2_2
    
    # energy
    energy_numExample_1 = energy_numExample_1 - zeroEnergy_1_1
    

    ## correct the predicted stress
    S_numE_2_2 = S_numExample_4.reshape(S_numExample_4.shape[0],  2, 2) # last two dimensions are i and j. sum over j
    F_numE_2_2 = F_numExample_4.reshape(F_numExample_4.shape[0], 2, 2)
    P_numE_2_2 = torch.matmul(F_numE_2_2, S_numE_2_2)
    P_numE_2_2 = P_numE_2_2 - zeroP_1_2_2
    P_numExample_4 = P_numE_2_2.reshape(P_numE_2_2.shape[0], 4)
    
    # unnormalize predictions
    P_numExample_4 = P_numExample_4 * momentum_max
    energy_numExample_1 = energy_numExample_1 * momentum_max
    
    # return energy_numExample_1.detach().numpy() * greenStrain_max, P_numExample_4.detach().numpy()
    return energy_numExample_1.cpu().detach().numpy(), P_numExample_4.cpu().detach().numpy()



def set_yaxis_format(ax):    
    formatter = matplotlib.ticker.ScalarFormatter(useMathText= True, useOffset=False)
    # formatter.set_powerlimits((-1, 1))
    formatter.set_powerlimits((0, 0))
    ax.yaxis.set_major_formatter(formatter)

    plt.gcf().canvas.draw()
    offset_text = ax.yaxis.get_offset_text().get_text()
    ax.get_yaxis().get_offset_text().set_visible(False)
    ax.text(0, 1.01, offset_text, transform=ax.transAxes, ha='left', va='baseline')

    ax.tick_params(axis='y', which='both', pad=1)

    ax.tick_params(axis='x', which='both', pad=1.5)




def plot_W_Fpath_separate():
    # suppose only NH material
    num_materials = len(material_list)
    num_F_path = len(F_path_list)
    assert num_materials == 1, "should only NH material"
    assert num_F_path == 3, "should only 3 F_path"

    fig, axs = plt.subplots(1, 3, figsize=set_size(width='two-column', fraction=.65, height_ratio=0.366) )
    


    x_pos = [i for i in range(len(noisy_level_list))]
    x_tick_label = [r"$10^{-8}$", r"$10^{-7}$", r"$10^{-6}$", r"$10^{-5}$"]
    

    count = 0


    for material_id in range(num_materials):
        for F_path_id in range(num_F_path):
            material = material_list[material_id]
            F_path = F_path_list[F_path_id]

            true_W_numExample_1 = true_W_curves[material][F_path]
            true_W_scale = np.mean(np.abs(true_W_numExample_1))

            predict_W_MAE_mean, predict_W_MAE_std = [], []
            for noisy_level in noisy_level_list:
                diff_W_mean = predict_W_curves[material][F_path]["noisy_{}".format(noisy_level)]["mean"]
                diff_W_std = predict_W_curves[material][F_path]["noisy_{}".format(noisy_level)]["std"]
                diff_W_mean = diff_W_mean / true_W_scale
                diff_W_std = diff_W_std / true_W_scale
                
                predict_W_MAE_mean.append(diff_W_mean)
                predict_W_MAE_std.append(diff_W_std)
            predict_W_MAE_mean = np.array(predict_W_MAE_mean)
            predict_W_MAE_std = np.array(predict_W_MAE_std)
            

            # axs[F_path_id].plot(x_pos, predict_W_MAE_mean, color = F_path_colors[F_path], marker= "s", markersize=3, linewidth = .8, markerfacecolor="white",  markeredgewidth=0.7, label= legend_labels[F_path], markeredgecolor= F_path_colors[F_path])
            # axs[F_path_id].fill_between(x_pos, predict_W_MAE_mean - predict_W_MAE_std, predict_W_MAE_mean + predict_W_MAE_std, facecolor= F_path_colors[F_path], alpha=0.25)

            # lines
            axs[F_path_id].plot(x_pos, predict_W_MAE_mean, F_path_colors[F_path], marker="s", markersize=3, linewidth = 0.8, markerfacecolor="white",  markeredgewidth=0.7, markeredgecolor= "white", linestyle = "-")
            axs[F_path_id].plot(x_pos, predict_W_MAE_std, F_path_colors[F_path], marker="o", markersize=3, linewidth = 0.8, markerfacecolor="white",  markeredgewidth=0.7, markeredgecolor= "white", linestyle = "-")
            
            # markers
            axs[F_path_id].plot(x_pos, predict_W_MAE_mean, F_path_colors[F_path], marker="s", markersize=3, linewidth = 0.8, markerfacecolor="none",  markeredgewidth=0.7, markeredgecolor= F_path_colors[F_path], label = "mean", linestyle="")
            axs[F_path_id].plot(x_pos, predict_W_MAE_std, F_path_colors[F_path], marker="o", markersize=3, linewidth = 0.8, markerfacecolor="none",  markeredgewidth=0.7, markeredgecolor= F_path_colors[F_path], label = "std", linestyle="")


            axs[F_path_id].set_title(legend_labels[F_path],  fontsize = 8, pad = 1) # lightblue
            axs[F_path_id].set_ylim([2E-4, 1.2])
            axs[F_path_id].set_yscale('log')

            # legend
            leg = axs[F_path_id].legend(loc="upper right", bbox_to_anchor=(0.6, 1.0), ncol=1, frameon=True, fontsize=6, numpoints=1, borderpad=0.3)
            for legobj in leg.legend_handles:
                legobj.set_markersize(3)


        #     if material_id == 0:
        #         axs.errorbar(x_pos[count], diff_W_mean, yerr=diff_W_std, xerr = None, color = F_path_colors[F_path], capsize=5, marker='o',  markersize=5, linestyle='-', elinewidth=2, capthick=1, alpha=1.) 
        #         axs.plot(x_pos[count], diff_W_mean, color = F_path_colors[F_path], label = legend_labels[F_path], marker='o',  markersize=5, alpha=1., linestyle='None') # for the legend
        #     else:
        #         axs.errorbar(x_pos[count], diff_W_mean, yerr=diff_W_std, color = F_path_colors[F_path], capsize=5, marker='o',  markersize=5, linestyle='-', elinewidth=2, capthick=1, alpha=1.)
        #     if F_path == "BT":
        #         x_label_pos.append(x_pos[count])
            
            
        #     count += 1
        # count += 2
            
            
             

            
        #     # this_ax.legend()
        #     this_ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        #     this_ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))


    
    # axs.xaxis.set_ticks_position('none') 

    axs[1].set_yticklabels([])
    axs[2].set_yticklabels([])

    axs[0].set_ylabel(r'${\langle |\widehat{W} - W| \rangle }~/~ {\langle |W| \rangle }$', labelpad= 3, fontsize=8)

    axs[0].set_xticks(x_pos )
    axs[0].set_xticklabels(x_tick_label )
    axs[0].tick_params(axis='x', pad=1)
    axs[0].tick_params(axis='y', pad=2)
    axs[0].set_xlabel(r'$\text{noise level}~ (\sigma / h )$', labelpad= 1, fontsize=8)

    axs[1].set_xticks(x_pos )
    axs[1].set_xticklabels(x_tick_label)
    axs[1].tick_params(axis='x', pad=1)
    axs[1].set_xlabel(r'$\text{noise level}~ (\sigma / h )$', labelpad= 1, fontsize=8)

    axs[2].set_xticks(x_pos )
    axs[2].set_xticklabels(x_tick_label)
    axs[2].tick_params(axis='x', pad=1)
    axs[2].set_xlabel(r'$\text{noise level}~ (\sigma / h )$', labelpad= 1, fontsize=8)
    # axs[0, 0].set_title("UD", loc= "center")
    # axs[0, 1].set_title("BD", loc= "center")
    # axs[0, 2].set_title("SD", loc= "center")

    # axs[-1, 0].set_xlabel('$\gamma$',  labelpad= 4)
    # axs[-1, 1].set_xlabel('$\gamma$',  labelpad= 4)
    # axs[-1, 2].set_xlabel('$\gamma$',  labelpad= 4)

    
    # handles, labels = axs.get_legend_handles_labels()
    # # handles = [h[0] for h in handles]
    # leg = fig.legend(handles, labels, loc='upper left', numpoints=1, ncol=1, handlelength=2.5, borderaxespad=0., bbox_to_anchor=(0.15, 0.9))
    # for legobj in leg.legend_handles:
    #     # legobj.set_linewidth(1.5)
    #     legobj.set_markersize(5)
        


    # axs.tick_params(axis='x', which='both', bottom=False, top=False)
    # axs.set_xticks(x_label_pos)
    # axs.set_xticklabels(x_labes)  # Rotating labels and aligning them to the right

    plt.subplots_adjust(wspace=0.1)
    plt.savefig("./figs/NH_noise_error_Fpath.pdf", bbox_inches='tight', pad_inches = 0.03)
    plt.close()




def plot_W_Fpath_together():
    # suppose only NH material
    num_materials = len(material_list)
    num_F_path = len(F_path_list)
    assert num_materials == 1, "should only NH material"
    assert num_F_path == 3, "should only 3 F_path"

    fig, axs = plt.subplots(1, 1, figsize=set_size(width='two-column', fraction=0.4, height_ratio=0.6) )
    


    # x_pos = [i for i in range(len(noisy_level_list))]
    # x_tick_label = [r"$10^{-8}$", r"$10^{-7}$", r"$10^{-6}$", r"$10^{-5}$"]
    x_pos = noisy_level_list
    

    count = 0


    for material_id in range(num_materials):
        for F_path_id in range(num_F_path):
            material = material_list[material_id]
            F_path = F_path_list[F_path_id]

            true_W_numExample_1 = true_W_curves[material][F_path]
            true_W_scale = np.mean(np.abs(true_W_numExample_1))

            predict_W_MAE_mean, predict_W_MAE_std = [], []
            for noisy_level in noisy_level_list:
                diff_W_mean = predict_W_curves[material][F_path]["noisy_{}".format(noisy_level)]["mean"]
                diff_W_std = predict_W_curves[material][F_path]["noisy_{}".format(noisy_level)]["std"]
                diff_W_mean = diff_W_mean / true_W_scale
                diff_W_std = diff_W_std / true_W_scale
                
                predict_W_MAE_mean.append(diff_W_mean)
                predict_W_MAE_std.append(diff_W_std)
            predict_W_MAE_mean = np.array(predict_W_MAE_mean)
            predict_W_MAE_std = np.array(predict_W_MAE_std)
            

            
            # # lines
            # axs[F_path_id].plot(x_pos, predict_W_MAE_mean, F_path_colors[F_path], marker="s", markersize=3, linewidth = 0.8, markerfacecolor="white",  markeredgewidth=0.7, markeredgecolor= "white", linestyle = "-")
            # axs[F_path_id].plot(x_pos, predict_W_MAE_std, F_path_colors[F_path], marker="o", markersize=3, linewidth = 0.8, markerfacecolor="white",  markeredgewidth=0.7, markeredgecolor= "white", linestyle = "-")
            
            # # markers
            # axs[F_path_id].plot(x_pos, predict_W_MAE_mean, F_path_colors[F_path], marker="s", markersize=3, linewidth = 0.8, markerfacecolor="none",  markeredgewidth=0.7, markeredgecolor= F_path_colors[F_path], label = "mean", linestyle="")
            # axs[F_path_id].plot(x_pos, predict_W_MAE_std, F_path_colors[F_path], marker="o", markersize=3, linewidth = 0.8, markerfacecolor="none",  markeredgewidth=0.7, markeredgecolor= F_path_colors[F_path], label = "std", linestyle="")

            print("F_path: ", F_path)
            # mean
            axs.plot(x_pos, predict_W_MAE_mean, F_path_colors[F_path], linewidth = 0.8, linestyle = "-", label= legend_labels[F_path])
            axs.plot(x_pos, predict_W_MAE_mean, F_path_colors[F_path], marker="s", markersize=3.5, linewidth = 0.8, markerfacecolor="none",  markeredgewidth=0.7, markeredgecolor= F_path_colors[F_path], linestyle="")
            # std
            axs.plot(x_pos, predict_W_MAE_std, F_path_colors[F_path], linewidth = 0.8, linestyle = "dotted")
            axs.plot(x_pos, predict_W_MAE_std, F_path_colors[F_path], marker="o", markersize=3.5, linewidth = 0.8, markerfacecolor="none",  markeredgewidth=0.7, markeredgecolor= F_path_colors[F_path], linestyle="")



            # axs[F_path_id].set_ylim([2E-4, 1.2])
            # axs[F_path_id].set_yscale('log')

            # # legend
            # leg = axs[F_path_id].legend(loc="upper right", bbox_to_anchor=(0.6, 1.0), ncol=1, frameon=True, fontsize=6, numpoints=1, borderpad=0.3)
            # for legobj in leg.legend_handles:
            #     legobj.set_markersize(3)


        

            
        #     # this_ax.legend()
        #     this_ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        #     this_ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))


    
    # axs.xaxis.set_ticks_position('none') 

    # axs[1].set_yticklabels([])
    # axs[2].set_yticklabels([])

    axs.set_ylabel(r'${\langle |\widehat{W} - W| \rangle }~/~ {\langle |W| \rangle }$', labelpad= 3, fontsize=7.5)
    axs.set_xlabel(r'$\text{noise level}~ (\sigma / h )$', labelpad= 1, fontsize=8.)


    # axs.set_xticks(x_pos )
    # axs.set_xticklabels(x_tick_label )
    axs.set_xscale('log')
    axs.set_yscale('log')


    # axs[0].set_xticks(x_pos )
    # axs[0].set_xticklabels(x_tick_label )
    # axs[0].tick_params(axis='x', pad=1)
    # axs[0].tick_params(axis='y', pad=2)
    # axs[0].set_xlabel(r'$\text{noise level}~ (\sigma / h )$', labelpad= 1, fontsize=8)

    # axs[1].set_xticks(x_pos )
    # axs[1].set_xticklabels(x_tick_label)
    # axs[1].tick_params(axis='x', pad=1)
    # axs[1].set_xlabel(r'$\text{noise level}~ (\sigma / h )$', labelpad= 1, fontsize=8)

    # axs[2].set_xticks(x_pos )
    # axs[2].set_xticklabels(x_tick_label)
    # axs[2].tick_params(axis='x', pad=1)
    # axs[2].set_xlabel(r'$\text{noise level}~ (\sigma / h )$', labelpad= 1, fontsize=8)
    
    # Create custom legend entries
    custom_handles = [
        matplotlib.lines.Line2D([0], [0], color="black", marker="s", markersize=3, markerfacecolor="none",  markeredgewidth=0.7, markeredgecolor= "black", linestyle="-", label='Mean'),
        matplotlib.lines.Line2D([0], [0],  color="black", marker="o", markersize=3, markerfacecolor="none",  markeredgewidth=0.7, markeredgecolor= "black", linestyle="dotted", label='Std')
    ]

    
    handles, labels = axs.get_legend_handles_labels()
    all_handles = handles + custom_handles
    all_labels = labels + [handle.get_label() for handle in custom_handles]

    leg = fig.legend(all_handles[:3], all_labels[:3], loc='upper left', numpoints=1, ncol=1, handlelength=1, borderaxespad=0., borderpad=0.3, bbox_to_anchor=(0.15, 0.85), fontsize=7)
    for legobj in leg.legend_handles:
        legobj.set_linewidth(2)
        # legobj.set_markersize(4)
        
    leg = fig.legend(all_handles[3:], all_labels[3:], loc='upper left', numpoints=1, ncol=1, handlelength=2.2, borderaxespad=0., borderpad=0.3, bbox_to_anchor=(0.67, 0.61), fontsize=7)
    for legobj in leg.legend_handles:
        legobj.set_linewidth(0.9)
        legobj.set_markersize(4.)


    # axs.tick_params(axis='x', which='both', bottom=False, top=False)
    # axs.set_xticks(x_label_pos)
    # axs.set_xticklabels(x_labes)  # Rotating labels and aligning them to the right

    plt.subplots_adjust(wspace=0.1)
    plt.savefig("./figs/NH_noise_error_Fpath.pdf", bbox_inches='tight', pad_inches = 0.03)
    plt.close()






if __name__ == "__main__":
    cuda_flag = False

    error_type = "both"  # "interpolation", "extrapolation", "both"
    # material_list = ["NH", "StVK", "Gent", "MR", "Fung", "AB"]
    material_list = ["NH"]


    noisy_level_list = [1E-5, 1E-6, 1E-7, 1E-8]
    noisy_level_list = sorted(noisy_level_list, reverse= False)
    print("noisy_level_list: ", noisy_level_list)



    F_path_list = ["UT", "BT", "SS"]

    F_path_colors = {"UT": "#E49E21", "BT": "#A61B29", "SS": "#A48CE6"}
    legend_labels = {"UT": "Uniaxial Deformation", "BT": "Biaxial Deformation", "SS": "Shear Deformation"}
    x_label_names = {"NH": "Neo-Hookean", "StVK": "St. Venant–Kirchhoff", "MR": "Mooney-Rivlin", "Fung": "Fung", "Gent": "Gent", "AB": "Arruda-Boyce"}

    predict_W_curves = {}
    true_W_curves = {}
    predict_P_curves = {}
    true_P_curves = {}

    F_path_gamma_list_interpolation = {}

    F_path_gamma_list_extrapolation = {} # for exterpolation





    for material in material_list:
        print("material: ", material)
        if material == "StVK":
            material_name = "VK"
        else:
            material_name = material
        input_data_dir = "../../Experiment1_different_material_backup/data_process/{}_GNN_input_0.01/".format(material_name)
        

        predict_W_curves[material] = {}
        true_W_curves[material] = {}
        predict_P_curves[material] = {} # the magnitude of P
        true_P_curves[material] = {} # the magnitude of P

        F_path_gamma_list_interpolation[material] = {}
        F_path_gamma_list_extrapolation[material] = {} # for exterpolation
              
        
        F_max, F_min = load_F()        


        for F_path in F_path_list:
            print("F_path: ", F_path)
            predict_W_curves[material][F_path] = {}
            # exterpolation_predict_W_curves[material][F_path] = {}
            predict_P_curves[material][F_path] = {}
            # exterpolation_predict_P_curves[material][F_path] = {}
            

            ## construct input
            gamma_list, F_numExample_4 = construct_testing_F(F_max, F_min, F_path)
            F_path_gamma_list_interpolation[material][F_path] = gamma_list

            # exterpolation input
            exterpolation_gamma, exterpolation_F_numExample_4, left_tick, right_tick = construct_exterpolation_F(F_max, F_min, F_path)
            F_path_gamma_list_extrapolation[material][F_path] = exterpolation_gamma

            ## true W and P
            true_W_numExample_1, true_P_numExample_4 = cmpt_groundTruth_W_and_P(F_numExample_4)
            # true exterpolation W and P
            exterpolation_true_W_numExample_1, exterpolation_true_P_numExample_4 = cmpt_groundTruth_W_and_P(exterpolation_F_numExample_4)

            if error_type == "both":
                true_W_numExample_1 = np.concatenate([true_W_numExample_1, exterpolation_true_W_numExample_1], axis= 0)
                true_P_numExample_4 = np.concatenate([true_P_numExample_4, exterpolation_true_P_numExample_4], axis= 0)
            elif error_type == "interpolation":
                true_W_numExample_1 = true_W_numExample_1
                true_P_numExample_4 = true_P_numExample_4
            elif error_type == "extrapolation":
                true_W_numExample_1 = exterpolation_true_W_numExample_1
                true_P_numExample_4 = exterpolation_true_P_numExample_4
            else:
                raise RuntimeError("error type error")
            

            # The ground-truth W for this F_path
            # print("true_W_numExample_1 shape: ", true_W_numExample_1.shape)
            true_W_curves[material][F_path] = true_W_numExample_1
            true_P_curves[material][F_path] = np.linalg.norm(true_P_numExample_4, axis= -1, keepdims= True)

            
            # The predicted W at different h_size
            for noisy_level in noisy_level_list:
                trained_model_folder = "./{}_trained_float64/noisy_{}/".format(material, noisy_level)                      
                predict_W_curves[material][F_path]["noisy_{}".format(noisy_level)] = {}


                ## predicted W and P in multiple experiments
                predict_W = []
                predict_P = []
                exterpolation_predict_W = []
                exterpolation_predict_P = []

                num_experiments = len(os.listdir(trained_model_folder ) )
                exp_list = [i + 1 for i in range(num_experiments)]
                for exp in exp_list:                
                    # load trained model
                    energy_model = energy_density_NN(args.input_dim, args.hidden_dim, args.activation)    
                    best_trained_model_file = trained_model_folder + "exp{}/".format(exp) + "best_trained_model.pt"
                    
                    with open(trained_model_folder + "exp{}/".format(exp) + "greenStrain_max.pkl", "rb") as f:
                        greenStrain_max = pkl.load(f)
                    with open(trained_model_folder + "exp{}/".format(exp) + "momentum_max.pkl", "rb") as f:
                        momentum_max_dict = pkl.load(f)
                        momentum_max = momentum_max_dict["momentum_max"]

                    energy_model.load_state_dict(torch.load(best_trained_model_file))
                    
                    # predicted W and P
                    predict_W_numExample_1, predict_P_numExample_4 = predict_W_and_P(F_numExample_4)
                    predict_W.append(predict_W_numExample_1)
                    predict_P.append(predict_P_numExample_4)

                    # exterpolation predicted W and P
                    exterpolation_predict_W_numExample_1, exterpolation_predict_P_numExample_4 = predict_W_and_P(exterpolation_F_numExample_4)
                    exterpolation_predict_W.append(exterpolation_predict_W_numExample_1)
                    exterpolation_predict_P.append(exterpolation_predict_P_numExample_4)


                # W
                predictW_exp_numExamples_1 = np.stack(predict_W, axis= 0)
                # print("predictW_exp_numExamples_1 shape: ", predictW_exp_numExamples_1.shape)
                exterpolation_predictW_exp_numExamples_1 = np.stack(exterpolation_predict_W, axis= 0) # exterpolation

                if error_type == "both":
                    predictW_exp_numExamples_1 = np.concatenate([predictW_exp_numExamples_1, exterpolation_predictW_exp_numExamples_1], axis= 1)
                elif error_type == "interpolation":
                    predictW_exp_numExamples_1 = predictW_exp_numExamples_1
                elif error_type == "extrapolation":
                    predictW_exp_numExamples_1 = exterpolation_predictW_exp_numExamples_1
                else:
                    raise RuntimeError("error type error")
                
                diff_W_exp_numExample_1 = np.abs(predictW_exp_numExamples_1 - true_W_numExample_1[None, :, :])
                # print("diff_W_exp_numExample_1 shape: ", diff_W_exp_numExample_1.shape)            
                
                diff_W_exp = np.mean(diff_W_exp_numExample_1.squeeze(), axis= 1, keepdims= False)
                predict_W_curves[material][F_path]["noisy_{}".format(noisy_level)]["mean"] = np.mean(diff_W_exp, axis= 0)
                predict_W_curves[material][F_path]["noisy_{}".format(noisy_level)]["std"] = np.std(diff_W_exp, axis= 0)
            




            
    # plot_W_Fpath_separate()
    plot_W_Fpath_together()
    
    
    
