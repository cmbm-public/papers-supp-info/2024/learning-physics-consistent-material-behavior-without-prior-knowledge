import pickle as pkl
import numpy as np
import torch
from torch.utils.data.dataset import TensorDataset
from torch.utils.data import DataLoader


torch.set_default_dtype(torch.float64)



def load_noisy_data(input_data_dir, batch_size, noisy_level, save_min_max_dir = None):

    ## target (momentum)
    noisy_momentum_file = input_data_dir + "noise_{}/".format(noisy_level) + "node_momentum.pkl"
    with open(noisy_momentum_file, "rb") as f:
        node_momentum_T_numNode_2 = pkl.load(f)
    print("numT: ", node_momentum_T_numNode_2.shape[0])
    print("numN: ", node_momentum_T_numNode_2.shape[1])

    numTotalSteps = node_momentum_T_numNode_2.shape[0]
    numTrainSteps = int(numTotalSteps * 0.8)
    trainTids = [i for i in range(numTrainSteps) ]
    validTids = [i for i in range(numTrainSteps, numTotalSteps) ]

    train_node_momentum_T_numNode_2 = node_momentum_T_numNode_2[trainTids]
    valid_node_momentum_T_numNode_2 = node_momentum_T_numNode_2[validTids]
    
    if save_min_max_dir != None:
        # Normalize the target momentum
        momentum_max = train_node_momentum_T_numNode_2.abs().max().item()
        momentum_max_pkl = { "momentum_max": momentum_max}
        with open(save_min_max_dir + "momentum_max.pkl", "wb") as f:
            pkl.dump(momentum_max_pkl, f)
        print("momentum_max.pkl: ", momentum_max_pkl)
        train_node_momentum_T_numNode_2 = train_node_momentum_T_numNode_2 / momentum_max
        valid_node_momentum_T_numNode_2 = valid_node_momentum_T_numNode_2 / momentum_max
    else:
        print("testing")



    ## input (deformation gradient)
    noisy_F_file = input_data_dir + "noise_{}/".format(noisy_level) + "deformation_gradient.pkl"
    with open(noisy_F_file, "rb") as f:
        F_T_numE_4 = pkl.load(f)   
    trainF_T_numE_4 = F_T_numE_4[trainTids]
    validF_T_numE_4 = F_T_numE_4[validTids]
    
    # ## max and min of F_T_numE_4, do not consider I
    # FwithoutI_T_numE_4 = torch.zeros_like(F_T_numE_4)
    # FwithoutI_T_numE_4[:, :, 0] = F_T_numE_4[:, :, 0] - 1.0
    # FwithoutI_T_numE_4[:, :, 1] = F_T_numE_4[:, :, 1]
    # FwithoutI_T_numE_4[:, :, 2] = F_T_numE_4[:, :, 2]
    # FwithoutI_T_numE_4[:, :, 3] = F_T_numE_4[:, :, 3] - 1.0
    # FwithoutI_max = torch.abs(FwithoutI_T_numE_4).max().item()
    # with open(save_min_max_dir + "FwithoutI_max.pkl", "wb") as f:
    #     pkl.dump(FwithoutI_max, f)

        
        

    ## element weights
    with open(input_data_dir + "qpWeights.pkl", "rb") as f:
        qpWeights_numE = pkl.load(f)
        # qpWeights_numE = qpWeights_numE.type(torch.FloatTensor)
    print("numE: ", len(qpWeights_numE))
    print("qpWeights_numE dtype: ", qpWeights_numE.dtype)

    
    
    # node-element-adjacency
    with open(input_data_dir + "connectivity.pkl", "rb") as f:            
        connectivity = pkl.load(f)
    with open(input_data_dir + "gradNa.pkl", "rb") as f:
        gradNa = pkl.load(f)
        # for i in range(len(gradNa)):
        #     gradNa[i] = gradNa[i].type(torch.FloatTensor)
   
    # internal node ids
    with open(input_data_dir + "vertex_boundary_mask.pkl", "rb") as f:
        vertex_boundary_mask = pkl.load(f) # list

    train_data = TensorDataset(train_node_momentum_T_numNode_2, trainF_T_numE_4)
    train_data_loader = DataLoader(train_data, batch_size = batch_size, shuffle = True)
    valid_data = TensorDataset(valid_node_momentum_T_numNode_2, validF_T_numE_4)
    valid_data_loader = DataLoader(valid_data, batch_size = batch_size, shuffle = False)

    return train_data_loader, valid_data_loader, qpWeights_numE, connectivity, gradNa, vertex_boundary_mask



def cmpt_cauchy_green_deformation(F):
    """
    The Cauchy-Green deformation tensor

    Compute right Cauchy-Green strain tensor from deformation gradient.

    _Input Arguments_

    - `F` - deformation gradient in Voigt notation

    _Output Arguments_

    - `C` - Cauchy-Green strain tensor in Voigt notation

    ---

    """
    F11 = F[:, :, 0:1]
    F12 = F[:, :, 1:2]
    F21 = F[:, :, 2:3]
    F22 = F[:, :, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2

    C = torch.cat((C11,C12,C21,C22), dim=-1)
    return C



def cmpt_green_strain_tensor(F):
    """
    The Green-Lagrange strain tensor E

    Compute Green-Lagrange strain tensor E from deformation gradient.

    _Input Arguments_

    - `F` - deformation gradient in Voigt notation

    _Output Arguments_

    - `E` - Green-Lagrange strain tensor in Voigt notation

    ---

    """

    F11 = F[:, :, 0:1]
    F12 = F[:, :, 1:2]
    F21 = F[:, :, 2:3]
    F22 = F[:, :, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2
    
    E11 = C11 - 1.0
    E12 = C12
    E21 = C21
    E22 = C22 - 1.0
    return 0.5 * torch.cat((E11, E12, E21, E22), dim= -1)