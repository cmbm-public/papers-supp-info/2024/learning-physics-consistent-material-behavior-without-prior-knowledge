## Learn constitutive relations with noisy data
This folder contains the code for experiments in Sec.4.3.


### Instructions

1. Suppose the Neo-Hookean FEM simulation (at h_gen = 0.001) has been generated in in  `../Experiment_various_materials/generate_data/`  
   Otherwise, following the instructions in the README in `../Experiment_various_materials/generate_data/` to generate the simulation
   


2. Process the simulation and generate input for training the neural network, at different noise levels
   
   ```bash
   # in data_process/
   bash interpolation_noisy_GNN_input.bash
   ```

3. Train uLED
   
   ```bash
   #in uLED/
   bash auto_train.bash # train uLED, with different data resolutions
   ```



4. Reproduce Fig.10(a)
      ```bash
      # in learned_field_visualization/
      mkdir case1/
      python visualization_difference_new.py
    
      # after this, NH_W_field_difference.pdf should be generated in learned_field_visualization/case1/
      ```

5. Reproduce Fig.10(b)
      ```bash
      # in uLED/
      python visualization_noise_error_Fpath.py
    
      # after this, NH_noise_error_Fpath.pdf should be in uLED/figs/
      ```

