import pathlib
import os
import torch
import numpy  as np
import pickle as pkl
from tqdm import tqdm
from fractions import Fraction
import decimal
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import argparse

torch.set_default_dtype(torch.float64)

parser = argparse.ArgumentParser()
parser.add_argument('--fine_ele_size', type=float, default=0.01, help='fine mesh size, used for simulation')
parser.add_argument('--coarse_ele_size', type=float, default=0.02, help='coarse mesh size, used for generating GNN input')
parser.add_argument('--material', type=str, default= "NH", help='material, used in simulation_folder ')
args = parser.parse_args() 

#=====================================================================
# interpolate fine mesh on coarse mesh
#=====================================================================
def interpolate_mesh():
    d = decimal.Decimal(str(args.fine_ele_size) )
    num_decimal = abs(d.as_tuple().exponent)
    vertex_pos2id_dict = {} # map position to id in the fine mesh
    for i in range(len(vertex_coordinates_N_2) ):
        cor_x = vertex_coordinates_N_2[i, 0]
        cor_x = round(cor_x, num_decimal)
        cor_y = vertex_coordinates_N_2[i, 1]
        cor_y = round(cor_y, num_decimal)
        vertex_pos2id_dict[(cor_x, cor_y)] = i
    # print(vertex_pos2id_dict)

    L = 1.0
    H = 0.4

    num_points_x = int(L / args.fine_ele_size)
    num_points_y = int(H / args.fine_ele_size)
    print("num_points_x ", num_points_x)
    print("num_points_y ", num_points_y)

    ele_node_array_numE_3_new = []
    sampled_vertex = set()
    coarse_fine_ratio = int((args.coarse_ele_size * decimals) / (args.fine_ele_size * decimals) )
    
    for i in range(0, num_points_x, coarse_fine_ratio):
        this_x = args.fine_ele_size * i
        this_x = round(this_x, num_decimal)
        assert this_x < L
        next_x = args.fine_ele_size * (i + coarse_fine_ratio)
        next_x = round(next_x, num_decimal)
        if next_x >= L:
            next_x = L

        for j in range(0, num_points_y, coarse_fine_ratio):            
            this_y = args.fine_ele_size * j
            this_y = round(this_y, num_decimal)
            assert this_y < H
            next_y = args.fine_ele_size * (j + coarse_fine_ratio)
            next_y = round(next_y, num_decimal)
            if next_y >= H:
                next_y = H
            
            left_bottom_id = vertex_pos2id_dict[(this_x, this_y)]
            right_bottom_id = vertex_pos2id_dict[(next_x, this_y)]
            right_top_id = vertex_pos2id_dict[(next_x, next_y)]
            left_top_id = vertex_pos2id_dict[(this_x, next_y)]

            ele_node_array_numE_3_new.append([left_bottom_id, right_bottom_id, right_top_id])
            ele_node_array_numE_3_new.append([left_bottom_id, right_top_id, left_top_id] )
            
            sampled_vertex.update([left_bottom_id, right_bottom_id, right_top_id, left_top_id])
    
    fineNodeId2coarseNodeId_dict = dict()
    vertexId_numCoarseN = [] # the id on original fine mesh
    print("sampled vertex: ", len(sampled_vertex))

    sampled_vertex = sorted(list(sampled_vertex) )
    for i in range(len(sampled_vertex) ):
        this_vertex_id = sampled_vertex[i]
        fineNodeId2coarseNodeId_dict[this_vertex_id] = i
        vertexId_numCoarseN.append(this_vertex_id)
    
    ## change the node id from fine mesh to coarse mesh
    # print(fineNodeId2coarseNodeId_dict)

    for i in range(len(ele_node_array_numE_3_new) ):
        for j in range(num_vertex_per_element):
            this_vertex_id = ele_node_array_numE_3_new[i][j]
            ele_node_array_numE_3_new[i][j] = fineNodeId2coarseNodeId_dict[this_vertex_id]
    ele_node_array_numE_3_new = np.array(ele_node_array_numE_3_new)

    return vertexId_numCoarseN, ele_node_array_numE_3_new



#=====================================================================
# extract time steps
#=====================================================================
def return_time_steps(dir):
    file_list = os.listdir(dir)
    used_time_steps = []
    for f_name in file_list:
        if f_name[0] != "a":
            continue
        f_name = f_name.split(".")[0]
        used_time_steps.append(int(f_name[2:]))
    return np.sort(used_time_steps)



#=====================================================================
# grad_N: the gradient of the base function
#=====================================================================
## the grad_N
def cmpt_J(x_coordinates, y_coordinates):
    vertex_physical_coordinates = np.stack([x_coordinates, y_coordinates] )
    gradN_xi = np.array([[1., 0.], [0., 1.], [-1., -1.]]) # [[grad_N_1/grad_xi_1, grad_N_1/grad_xi_2], [grad_N_2/grad_xi_1, grad_N_2/grad_xi_2], [grad_N_3/grad_xi_1, grad_N_3/grad_xi_2]]
    return np.matmul(vertex_physical_coordinates, gradN_xi)

def cmpt_J_inverse(J):
    assert J.shape == (2, 2)
    A = J[0, 0]
    B = J[0, 1]
    C = J[1, 0]
    D = J[1, 1]
    det_J = A * D - B * C
    mat_new = np.array([[D, -B], [-C, A] ] )
    J_inverse = 1.0 / det_J * mat_new
    # print("J_inverse * J: ", np.matmul(J_inverse, J) )
    return J_inverse


def cmpt_grad_N1(J_inverse):
    grad_xi_1_grad_x_1 = J_inverse[0, 0]
    grad_xi_1_grad_x_2 = J_inverse[0, 1]
    grad_xi_2_grad_x_1 = J_inverse[1, 0]
    grad_xi_2_grad_x_2 = J_inverse[1, 1]

    grad_N1_grad_xi_1 = 1.0
    grad_N1_grad_xi_2 = 0.0

    grad_N1_grad_x_1 = grad_N1_grad_xi_1 * grad_xi_1_grad_x_1 + grad_N1_grad_xi_2 * grad_xi_2_grad_x_1 # x dimension
    grad_N1_grad_x_2 = grad_N1_grad_xi_1 * grad_xi_1_grad_x_2 + grad_N1_grad_xi_2 * grad_xi_2_grad_x_2 # y dimension
    return [grad_N1_grad_x_1, grad_N1_grad_x_2]


def cmpt_grad_N2(J_inverse):
    grad_xi_1_grad_x_1 = J_inverse[0, 0]
    grad_xi_1_grad_x_2 = J_inverse[0, 1]
    grad_xi_2_grad_x_1 = J_inverse[1, 0]
    grad_xi_2_grad_x_2 = J_inverse[1, 1]

    grad_N2_grad_xi_1 = 0.0
    grad_N2_grad_xi_2 = 1.0

    grad_N2_grad_x_1 = grad_N2_grad_xi_1 * grad_xi_1_grad_x_1 + grad_N2_grad_xi_2 * grad_xi_2_grad_x_1 # x dimension
    grad_N2_grad_x_2 = grad_N2_grad_xi_1 * grad_xi_1_grad_x_2 + grad_N2_grad_xi_2 * grad_xi_2_grad_x_2 # y dimension
    return [grad_N2_grad_x_1, grad_N2_grad_x_2]


def cmpt_grad_N3(J_inverse):
    grad_xi_1_grad_x_1 = J_inverse[0, 0]
    grad_xi_1_grad_x_2 = J_inverse[0, 1]
    grad_xi_2_grad_x_1 = J_inverse[1, 0]
    grad_xi_2_grad_x_2 = J_inverse[1, 1]

    grad_N3_grad_xi_1 = - 1.0
    grad_N3_grad_xi_2 = - 1.0

    grad_N3_grad_x_1 = grad_N3_grad_xi_1 * grad_xi_1_grad_x_1 + grad_N3_grad_xi_2 * grad_xi_2_grad_x_1 # x dimension
    grad_N3_grad_x_2 = grad_N3_grad_xi_1 * grad_xi_1_grad_x_2 + grad_N3_grad_xi_2 * grad_xi_2_grad_x_2 # y dimension
    return [grad_N3_grad_x_1, grad_N3_grad_x_2]


def get_area(x, y):
    area = 0.5 * (x[0] * (y[1] - y[2]) + x[1] * (y[2] - y[0]) + x[2] * (y[0] - y[1]) )
    # return area
    return np.abs(area)

def cmpt_grad_N(coordinates_3_2):
    assert coordinates_3_2.shape == (3, 2)
    x_coordinates = coordinates_3_2[:, 0]
    y_coordinates = coordinates_3_2[:, 1]

    J = cmpt_J(x_coordinates, y_coordinates)
    # print("J shape: ", J.shape)

    J_inverse = cmpt_J_inverse(J)
    # print("J_inverse shape: ", J_inverse.shape)

    grad_N1 = cmpt_grad_N1(J_inverse)
    # print("grad_N1: ", grad_N1)

    grad_N2 = cmpt_grad_N2(J_inverse)
    # print("grad_N2: ", grad_N2)

    grad_N3 = cmpt_grad_N3(J_inverse)
    # print("grad_N3: ", grad_N3)

    return (grad_N1, grad_N2, grad_N3)


#=====================================================================
# node momentum: (T, numNode, dim)
# remember to sample the nodes
#=====================================================================
def generate_node_momentum(simulation_dir, save_folder):
    print("number of available_time_steps", len(available_time_steps))

    num_nodes_per_element = 3
    numElements = ele_node_array_numE_3.shape[0]


    qpWeights_numE = qpWeight_list
    assert len(qpWeights_numE) == numElements


    dim = 2
    node_momentum_T_numNode_dim = []

    for t in available_time_steps:
        acc_nodes_numNode_dim = np.load(simulation_dir + "a_{}.npy".format(t) )
        # sample vertex
        acc_nodes_numNode_dim = acc_nodes_numNode_dim[vertexId_numCoarseN, :]
        acc_nodes_numNode_dim = torch.tensor(acc_nodes_numNode_dim)

        assert num_vertex == acc_nodes_numNode_dim.shape[0]

        node_momentum_numNode_dim = torch.zeros((num_vertex, dim), dtype= torch.float64) # save Ma
        for a in range(num_nodes_per_element):
            for b in range(num_nodes_per_element):
                accelerations = acc_nodes_numNode_dim[connectivity[b] ]
                mass = qpWeights_numE[:, None ] # rho is 1.0
                # print("accelerations shape: ", accelerations.shape) # accelerations shape:  torch.Size([200, 2])
                # print("mass shape: ", mass.shape) # mass shape:  torch.Size([200, 1])
                # exit()
                if b == a:
                    this_acc_mul_mass = 1. / 6. * mass * accelerations                    
                else:
                    this_acc_mul_mass = 1. / 12. * mass * accelerations                    
                node_momentum_numNode_dim.index_add_(0, connectivity[a], this_acc_mul_mass)
        
        node_momentum_T_numNode_dim.append(node_momentum_numNode_dim)
    
    node_momentum_T_numNode_dim = torch.stack(node_momentum_T_numNode_dim)
    print("node_momentum_T_numNode_dim shape: ", node_momentum_T_numNode_dim.shape)

    with open(save_folder + "node_momentum.pkl", "wb") as f:
        pkl.dump(node_momentum_T_numNode_dim.cpu().detach(), f)
        print("Saved node_momentum_T_numNode_dim to " + save_folder + "node_momentum.pkl")



#=====================================================================
# deformation gradient: (T, numElement, dim)
#=====================================================================        
def generate_deformation_gradient(simulation_dir, save_folder):
    num_nodes_per_element = 3
    dim = 2
    voigtMap = [[0,1],[2,3]]

    numElements = ele_node_array_numE_3.shape[0]


    F_T_numE_4 = []

    for t in available_time_steps:
        u_nodes_numNode_dim = np.load(simulation_dir + "u_{}.npy".format(t) )
        # sample vertex
        u_nodes_numNode_dim = u_nodes_numNode_dim[vertexId_numCoarseN, :]
        assert num_vertex == u_nodes_numNode_dim.shape[0]
        u_nodes = torch.tensor(u_nodes_numNode_dim)
        
        u = []
        for i in range(num_nodes_per_element):
            u.append(u_nodes[connectivity[i], :])
        
        F=torch.zeros(numElements, 4)
        for a in range(num_nodes_per_element):
            for i in range(dim):
                for j in range(dim):
                    F[:, voigtMap[i][j]] += u[a][:,i] * gradNa[a][:,j]
        F[:,0] += 1.0
        F[:,3] += 1.0
        
        F_T_numE_4.append(F)
    
    F_T_numE_4 = torch.stack(F_T_numE_4)
    print("F_T_numE_4 shape: ", F_T_numE_4.shape)

    with open(save_folder + "deformation_gradient.pkl", "wb") as f:
        pkl.dump(F_T_numE_4.cpu().detach(), f)
        print("Saved F_T_numE_4 to " + save_folder + "deformation_gradient.pkl")





if __name__ == "__main__":
    
    decimals = 1000000000
    assert (args.coarse_ele_size * decimals) % (args.fine_ele_size * decimals) == 0
    # simulation_folder = "../generate_data/data_{}/".format(args.fine_ele_size)
    simulation_folder = "../generate_data/{}_data_{}/".format(args.material, args.fine_ele_size)
    mesh_folder = "../generate_data/mesh_{}/".format(args.fine_ele_size)

    # save_folder = "./GNN_input_interpolation_{}/{}/".format(args.fine_ele_size, args.coarse_ele_size)
    save_folder = "./{}_GNN_input_interpolation_{}/{}/".format(args.material, args.fine_ele_size, args.coarse_ele_size)
    pathlib.Path(save_folder).mkdir(parents=True, exist_ok=True)

    ## load data
    num_vertex_per_element = 3

    vertex_coordinates_N_2 = np.load(simulation_folder + "coordinates.npy")
    print("num_vertex original: ", len(vertex_coordinates_N_2) )
    with open(mesh_folder + "mesh_info.pkl", "rb") as f:
        mesh_info = pkl.load(f)
        print("mesh_info keys: ", mesh_info.keys())
        mesh_coordinates  = mesh_info["coordinates"]
        vertex_boundary_indicator = mesh_info["boundary_indicator"]
        ele_node_array_numE_3 = mesh_info["ele_node_array_numE_3"].astype(np.int64)

    # make sure the order of nodes in Mesh and in Simulation are the same
    assert (vertex_coordinates_N_2 == mesh_coordinates ).all()
    del mesh_coordinates


    ## interpolate on coarse mesh
    vertexId_numCoarseN, ele_node_array_numE_3_new = interpolate_mesh()
    del ele_node_array_numE_3
    
    # vertex info on the coarse mesh
    ele_node_array_numE_3 = ele_node_array_numE_3_new
    vertex_coordinates_N_2 = vertex_coordinates_N_2[vertexId_numCoarseN]
    vertex_boundary_indicator = vertex_boundary_indicator[vertexId_numCoarseN]

    # ## plotting
    # triang = tri.Triangulation(vertex_coordinates_N_2[:, 0], vertex_coordinates_N_2[:, 1], ele_node_array_numE_3 )
    # fig1, ax1 = plt.subplots(figsize=(10, 4))
    # # ax1.set_aspect('equal')
    # ax1.triplot(triang, 'bo-', lw=1)
    # plt.savefig(save_folder + "mesh.pdf", bbox_inches='tight', pad_inches = 0.02)


    num_element = ele_node_array_numE_3.shape[0] # number of elements in the coarse mesh
    print("num_element: ", num_element)
    assert ele_node_array_numE_3.shape[1] == num_vertex_per_element
    num_vertex = len(vertex_coordinates_N_2) # number of vertex in the coarse mesh

    ## the grad_N
    gradNa = []
    grad_N1_list = []
    grad_N2_list = []
    grad_N3_list = []
    qpWeight_list = []
    for i in tqdm(range(num_element)):
        vertex_list = ele_node_array_numE_3[i]
        assert len(vertex_list) == num_vertex_per_element
        vertex_coordinates_array = vertex_coordinates_N_2[vertex_list]
        # print(vertex_coordinates_array)

        grad_N1, grad_N2, grad_N3 = cmpt_grad_N(vertex_coordinates_array)
        area = get_area(vertex_coordinates_array[:, 0], vertex_coordinates_array[:, 1] )
        # print("grad_N1: ", grad_N1)
        grad_N1_list.append(grad_N1)
        grad_N2_list.append(grad_N2)
        grad_N3_list.append(grad_N3)
        qpWeight_list.append(area)

    grad_N1_list = np.stack(grad_N1_list, axis= 0)
    gradNa.append(torch.from_numpy(grad_N1_list))
    grad_N2_list = np.stack(grad_N2_list, axis= 0)
    gradNa.append(torch.from_numpy(grad_N2_list))
    grad_N3_list = np.stack(grad_N3_list, axis= 0)
    gradNa.append(torch.from_numpy(grad_N3_list))

    ## the qpWeight
    qpWeight_list = np.array(qpWeight_list)
    qpWeight_list = torch.from_numpy(qpWeight_list)
    print("grad_N1_list shape: ", grad_N1_list.shape)

    ## the connectivity info used
    connectivity = []
    for i in range(num_vertex_per_element):
        connectivity.append(torch.tensor(ele_node_array_numE_3[:, i] ) )
    assert len(connectivity[0]) == num_element
    assert len(connectivity) == num_vertex_per_element
    assert np.max([connectivity[i].max().item() for i in range(num_vertex_per_element)] ) + 1 == len(vertex_coordinates_N_2)

    # saving
    with open(save_folder + "gradNa.pkl", "wb") as f:
        pkl.dump(gradNa, f)
        print("Saved gradNa to " + save_folder + "gradNa.pkl")
    with open(save_folder + "qpWeights.pkl", "wb") as f:
        pkl.dump(qpWeight_list, f)
        print("Saved qpWeight_list to " + save_folder + "qpWeight.pkl")
    with open(save_folder + "vertex_boundary_mask.pkl", "wb") as f:
        pkl.dump(vertex_boundary_indicator, f)
        print("\n#boundary vertex: ", vertex_boundary_indicator.sum() )
        print("boundary vertex percentage: ", Fraction(vertex_boundary_indicator.sum() /  np.size(vertex_boundary_indicator) ).limit_denominator())
        print("#non-boundary vertex: ", np.size(vertex_boundary_indicator) - vertex_boundary_indicator.sum() )
        print("Saved vertex_boundary_indicator to " + save_folder + "vertex_boundary_mask.pkl\n")
    with open(save_folder + "connectivity.pkl", "wb") as f:
        pkl.dump(connectivity, f)
        print("Saved connectivity to " + save_folder + "connectivity.pkl")
    # for the use of downsampling vertex
    with open(save_folder + "vertex_coordinates_N_2.pkl", "wb") as f:
        pkl.dump(vertex_coordinates_N_2, f)
        print("Saved vertex_coordinates_N_2 to " + save_folder + "vertex_coordinates_N_2.pkl")



    ## time steps:
    available_time_steps = return_time_steps(simulation_folder)


    ## momentum
    generate_node_momentum(simulation_folder, save_folder)


    ## deformation gradient
    generate_deformation_gradient(simulation_folder, save_folder)



