import os
import numpy as np
from tqdm import tqdm
import pickle as pkl
import torch
# torch.set_num_threads(4)
torch.set_default_dtype(torch.float64)



from utils import load_data, cmpt_green_strain_tensor, cmpt_cauchy_green_deformation
from model import energy_density_NN

import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--num_nodes_per_element', type=int, default=3, help='Number of vertex on each element.') 
parser.add_argument('--space_dim', type=int, default=2, help='space dimension') 

parser.add_argument('--batch_size', type=int, default=1, help='Number of samples per batch.')
parser.add_argument('--input_dim', type=int, default=2, help='Two invariants.')
parser.add_argument('--hidden_dim', type=int, default=128, help='Hidden dimension.')
parser.add_argument('--activation', type=str, default='elu', help='[]')

parser.add_argument('--training_epochs', type=int, default=1000, help='training epochs.')

# parser.add_argument('--input_data_dir', type=str, default='../data_process/GNN_input_0.0005/', help='Dir for input data')
# parser.add_argument('--saved_model_folder', type=str, default='./trained_ele_0.0005_float64/interpolate_0/', help='Dir of the saved trained model')

parser.add_argument('--material', type=str, default='-1', help='NH or VK')
parser.add_argument('--exp_id', type=int, default=-1, help='exp_id')



args = parser.parse_args()



cuda_flag = True




def cmpt_momentum_inconsistency(data_loader):
    # print("\n\n" + "######" * 5)
    # print("In verify_dynamics")
    
    Youngs_modulus = 1e4
    nu= 0.3 # Poisson rate
    mu  = Youngs_modulus / (2.0*(1.0 + nu))
    lmbda = Youngs_modulus * nu / ((1.0 + nu)*(1.0 - 2.0*nu))
    bulk_K = Youngs_modulus / (3.0 * (1.0 - 2 * nu) ) #bulk modulus


    
    total_momentum_MAE = 0.0
    total_momentum_scale = 0.0
    count_time_node = 0.0

    for batch_idx, (node_momentum_T_numNode_2, F_T_numE_4) in enumerate(data_loader):
        # print("\n\n## batch_idx {}".format(batch_idx))
        this_batch_size = len(F_T_numE_4)
        num_total_node = node_momentum_T_numNode_2.shape[1]
        num_elements = F_T_numE_4.shape[1]
        if cuda_flag:
            node_momentum_T_numNode_2 = node_momentum_T_numNode_2.cuda()
            F_T_numE_4 = F_T_numE_4.cuda()
            # print("F_T_numE_4")
            # print(F_T_numE_4)
                        
        E_T_numE_4  = cmpt_green_strain_tensor(F_T_numE_4) # green strain tensor
        # print("E_T_numE_4 shape: ", E_T_numE_4.shape)
        # if batch_idx == 1:
        #     print("E_T_numE_4")
        #     print(E_T_numE_4)

        trE_T_numE_1 = E_T_numE_4[:, :, 0:1] + E_T_numE_4[:, :, 3:4]


        if args.material == "NH":
            # ### New-Hookean
            F_T_numE_4.requires_grad = True
            C_T_numE_4 = cmpt_cauchy_green_deformation(F_T_numE_4)
            I1_T_numE_1 = C_T_numE_4[:, :, 0:1] + C_T_numE_4[:, :, 3:4]
            J_T_numE_1 = F_T_numE_4[:, :, 0:1] * F_T_numE_4[:, :, 3:4] - F_T_numE_4[:, :, 1:2] * F_T_numE_4[:, :, 2:3]
            W = (mu / 2) * (I1_T_numE_1 - 2) - mu * torch.log(J_T_numE_1) + (lmbda / 2) * (torch.log(J_T_numE_1))**2 # 2D
            P_T_numE_4 = torch.autograd.grad(W.sum(), F_T_numE_4, create_graph=True)[0]
        elif args.material == "VK":
            # St. Venant–Kirchhoff
            F_T_numE_4.requires_grad = True
            E_T_numE_4  = cmpt_green_strain_tensor(F_T_numE_4)
            I1 = E_T_numE_4[:, :, 0:1] + E_T_numE_4[:, :, 3:4]
            I2 = E_T_numE_4[:, :, 0:1] * E_T_numE_4[:, :, 3:4] - E_T_numE_4[:, :, 1:2] * E_T_numE_4[:, :, 2:3]
            W = (lmbda /2 ) * I1 **2 + mu * (I1 **2 - 2 * I2)
            P_T_numE_4 = torch.autograd.grad(W.sum(), F_T_numE_4, create_graph=True)[0]
        
        else:
            raise RuntimeError("args.material invalid")
        


        P_T_numE_2_2 = P_T_numE_4.reshape(this_batch_size, num_elements, 2, 2)

        nodeAggregatedP_T_numN_2 = torch.zeros((this_batch_size, num_total_node, 2), dtype = F_T_numE_4.dtype, device = F_T_numE_4.device)

        for a in range(args.num_nodes_per_element):
            assert gradNa[a].shape == (num_elements, 2)
            for i in range(args.space_dim):        
                P_T_numE_2 = P_T_numE_2_2[:, :, i, :] * gradNa[a][None, :, :] * EleWeights_numE[None, :, None] # P_T_numE_2, 2 is j
                P_T_numE = P_T_numE_2.sum(-1) # sum over j
                nodeAggregatedP_T_numN_2[:, :, i].index_add_(1, connectivity[a], P_T_numE) # pay attention dim is 1


        # print("nodeAggregatedP_T_numN_2 shape: ", nodeAggregatedP_T_numN_2.shape)

        node_momentum_T_numNode_2 = node_momentum_T_numNode_2[:, vertex_internal_mask, :]
        nodeAggregatedP_T_numN_2 = nodeAggregatedP_T_numN_2[:, vertex_internal_mask, :]
        # print("node_momentum_T_numNode_2 shape: ", node_momentum_T_numNode_2.shape)
        # print("nodeAggregatedP_T_numN_2 shape: ", nodeAggregatedP_T_numN_2.shape)

        MAE_momentum = torch.sum(torch.abs(node_momentum_T_numNode_2 + nodeAggregatedP_T_numN_2) )
        total_momentum_MAE += MAE_momentum.item()
        total_momentum_scale += torch.sum(torch.abs(node_momentum_T_numNode_2) ).item()

        count_time_node += node_momentum_T_numNode_2.shape[0] * node_momentum_T_numNode_2.shape[1]
    
    average_momentum_MAE = total_momentum_MAE / count_time_node
    average_momentum_scale = total_momentum_scale / count_time_node
    # print("######" * 5)
    # print("average_momentum_MAE: ", average_momentum_MAE)
    # print("average_momentum_scale: ", average_momentum_scale)
    with open(log_file, "a") as f:
        print("###" * 4, file = f)
        print("Verify dynamics", file = f)
        print("average_momentum_MAE : {}".format(average_momentum_MAE), file = f)
        print("average_momentum_scale : {}".format(average_momentum_scale), file = f)    





def cmpt_predicted_momentum_error(data_loader):
    # https://stackoverflow.com/questions/60018578/what-does-model-eval-do-in-pytorch
    # energy_model.eval()

    total_momentum_MAE = 0.0
    count_node = 0.0

    # for zero stress with zero strain
    # Green strain
    zero_strain_1_1_4 = torch.zeros((1, 1, 4) )
    if cuda_flag:
        zero_strain_1_1_4 = zero_strain_1_1_4.cuda()
    zero_strain_1_1_4.requires_grad = True
    
    ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
    zeroEnergy_1_1_1 = energy_model(zero_strain_1_1_4)
    zeroS_1_1_4  = torch.autograd.grad(zeroEnergy_1_1_1.sum(), zero_strain_1_1_4, create_graph = True)[0]
    zeroS_1_1_2_2 = zeroS_1_1_4.reshape(1, 1, 2, 2)
    # zeroP_1_1_2_2 = torch.matmul(unit_F_1_1_2_2, zeroS_1_1_2_2)
    zeroP_1_1_2_2 = zeroS_1_1_2_2
    # zeroP_1_1_2_2 = zeroP_1_1_2_2.cpu().detach()

    for batch_idx, (node_momentum_T_numNode_2, F_T_numE_4) in enumerate(data_loader):
        this_batch_size = len(F_T_numE_4)
        num_total_node = node_momentum_T_numNode_2.shape[1]
        num_elements = F_T_numE_4.shape[1]
        if cuda_flag:
            node_momentum_T_numNode_2 = node_momentum_T_numNode_2.cuda()
            F_T_numE_4 = F_T_numE_4.cuda()
             
        E_T_numE_4  = cmpt_green_strain_tensor(F_T_numE_4) # green strain tensor
        E_T_numE_4.requires_grad = True

        # normalize INPUT Green strain
        normalizedE_T_numE_4 = E_T_numE_4 / greenStrain_max


        ## learn the 2-nd Piola Kirchhoff stress S
        energy_T_numE_1 = energy_model(normalizedE_T_numE_4)
        S_T_numE_4 = torch.autograd.grad(energy_T_numE_1.sum(), E_T_numE_4, create_graph=True)[0]
        
        
        ## correct the predicted stress
        S_T_numE_2_2 = S_T_numE_4.reshape(S_T_numE_4.shape[0], S_T_numE_4.shape[1], 2, 2) # last two dimensions are i and j. sum over j
        F_T_numE_2_2 = F_T_numE_4.reshape(F_T_numE_4.shape[0], F_T_numE_4.shape[1], 2, 2)
        P_T_numE_2_2 = torch.matmul(F_T_numE_2_2, S_T_numE_2_2)
        P_T_numE_2_2 = P_T_numE_2_2 - torch.matmul(F_T_numE_2_2, zeroP_1_1_2_2) # change the stress regularizer!!!

        del S_T_numE_4, S_T_numE_2_2, F_T_numE_4, F_T_numE_2_2, energy_T_numE_1, normalizedE_T_numE_4, E_T_numE_4
        
        
        ## unnormalize the output
        P_T_numE_2_2 = P_T_numE_2_2 * momentum_max


        # do not need grad here
        nodeAggregatedP_T_numN_2 = torch.zeros((this_batch_size, num_total_node, 2) )
        if cuda_flag:
            nodeAggregatedP_T_numN_2 = nodeAggregatedP_T_numN_2.cuda()


        for a in range(args.num_nodes_per_element):
            assert gradNa[a].shape == (num_elements, 2)
            for i in range(args.space_dim):        
                P_T_numE_2 = P_T_numE_2_2[:, :, i, :] * gradNa[a][None, :, :] * EleWeights_numE[None, :, None] # P_T_numE_2, 2 is j
                P_T_numE = P_T_numE_2.sum(-1) # sum over j
                nodeAggregatedP_T_numN_2[:, :, i].index_add_(1, connectivity[a], P_T_numE) # pay attention dim is 1


        ## choose the internal vertex
        node_momentum_T_numNode_2 = node_momentum_T_numNode_2[:, vertex_internal_mask, :]
        nodeAggregatedP_T_numN_2 = nodeAggregatedP_T_numN_2[:, vertex_internal_mask, :]
        
        # print("node_momentum_T_numNode_2 shape: ", node_momentum_T_numNode_2.shape)
        # print("nodeAggregatedP_T_numN_2 shape: ", nodeAggregatedP_T_numN_2.shape)

        total_momentum_MAE += torch.sum(torch.abs(node_momentum_T_numNode_2 + nodeAggregatedP_T_numN_2) ).item()
        count_node += node_momentum_T_numNode_2.shape[0] * node_momentum_T_numNode_2.shape[1]

        del nodeAggregatedP_T_numN_2, P_T_numE_2_2, P_T_numE_2, P_T_numE
    
    with open(log_file, "a") as f:
        print("###" * 4, file = f)
        print("predict_momentum_MAE : {}".format(total_momentum_MAE / count_node), file = f)







def cmpt_predicted_W_and_P_error(data_loader):
    
    # configuration for grount-truth W and P
    Youngs_modulus = 1e4
    nu= 0.3 # Poisson rate
    mu  = Youngs_modulus / (2.0*(1.0 + nu))
    lmbda = Youngs_modulus * nu / ((1.0 + nu)*(1.0 - 2.0*nu))


    count_element = 0.0
    total_W_MAE = 0.0
    total_P_MAE = 0.0
    total_W_scale = 0.0
    total_P_scale = 0.0

    # for zero stress with zero strain
    # Green strain
    zero_strain_1_1_4 = torch.zeros((1, 1, 4) )
    if cuda_flag:
        zero_strain_1_1_4 = zero_strain_1_1_4.cuda()
    zero_strain_1_1_4.requires_grad = True
        
    ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
    zeroEnergy_1_1_1 = energy_model(zero_strain_1_1_4)
    zeroS_1_1_4  = torch.autograd.grad(zeroEnergy_1_1_1.sum(), zero_strain_1_1_4, create_graph = True)[0]
    zeroS_1_1_2_2 = zeroS_1_1_4.reshape(1, 1, 2, 2)
    # zeroP_1_1_2_2 = torch.matmul(unit_F_1_1_2_2, zeroS_1_1_2_2)
    zeroP_1_1_2_2 = zeroS_1_1_2_2

    for batch_idx, (node_momentum_T_numNode_2, F_T_numE_4) in enumerate(data_loader):
        this_batch_size = len(F_T_numE_4)
        num_total_node = node_momentum_T_numNode_2.shape[1]
        num_elements = F_T_numE_4.shape[1]
        if cuda_flag:
            F_T_numE_4 = F_T_numE_4.cuda()
             
        E_T_numE_4  = cmpt_green_strain_tensor(F_T_numE_4) # green strain tensor
        E_T_numE_4.requires_grad = True

        # normalize INPUT Green strain
        normalizedE_T_numE_4 = E_T_numE_4 / greenStrain_max

        # print("normalizedE_T_numE_4 shape: ", normalizedE_T_numE_4.shape)

        ## learn the 2-nd Piola Kirchhoff stress S
        energy_T_numE_1 = energy_model(normalizedE_T_numE_4)
        S_T_numE_4 = torch.autograd.grad(energy_T_numE_1.sum(), E_T_numE_4, create_graph=True)[0]
        
        
        
        
        ## correct the predicted stress
        S_T_numE_2_2 = S_T_numE_4.reshape(S_T_numE_4.shape[0], S_T_numE_4.shape[1], 2, 2) # last two dimensions are i and j. sum over j
        F_T_numE_2_2 = F_T_numE_4.reshape(F_T_numE_4.shape[0], F_T_numE_4.shape[1], 2, 2)
        P_T_numE_2_2 = torch.matmul(F_T_numE_2_2, S_T_numE_2_2)
        P_T_numE_2_2 = P_T_numE_2_2 - torch.matmul(F_T_numE_2_2, zeroP_1_1_2_2) # change the stress regularizer!!!
        P_T_numE_4 = P_T_numE_2_2.reshape(P_T_numE_2_2.shape[0], P_T_numE_2_2.shape[1], 4)
        
        ## correct the predicted energy
        energy_T_numE_1 = energy_T_numE_1 - zeroEnergy_1_1_1 - torch.mul(E_T_numE_4, zeroS_1_1_4).sum(dim = -1, keepdim= True) # change the energy regularizer!!!


        ## unnormalize the output
        P_T_numE_4 = P_T_numE_4 * momentum_max
        energy_T_numE_1 = energy_T_numE_1 * momentum_max

        del S_T_numE_4, S_T_numE_2_2, F_T_numE_2_2, normalizedE_T_numE_4, P_T_numE_2_2, E_T_numE_4


        ##############################################
        ## ground-truth energy and stress ##
        ##############################################

        if args.material == "NH":
            # ### New-Hookean
            F_T_numE_4.requires_grad = True
            C_T_numE_4 = cmpt_cauchy_green_deformation(F_T_numE_4)
            I1_T_numE_1 = C_T_numE_4[:, :, 0:1] + C_T_numE_4[:, :, 3:4]
            J_T_numE_1 = F_T_numE_4[:, :, 0:1] * F_T_numE_4[:, :, 3:4] - F_T_numE_4[:, :, 1:2] * F_T_numE_4[:, :, 2:3]
            true_W_T_numE_1 = (mu / 2) * (I1_T_numE_1 - 2) - mu * torch.log(J_T_numE_1) + (lmbda / 2) * (torch.log(J_T_numE_1))**2 # 2D
            true_P_T_numE_4 = torch.autograd.grad(true_W_T_numE_1.sum(), F_T_numE_4, create_graph=True)[0]

            del F_T_numE_4, C_T_numE_4, I1_T_numE_1, J_T_numE_1


        elif args.material == "VK":
            # St. Venant–Kirchhoff
            F_T_numE_4.requires_grad = True
            E_T_numE_4  = cmpt_green_strain_tensor(F_T_numE_4)
            I1 = E_T_numE_4[:, :, 0:1] + E_T_numE_4[:, :, 3:4]
            I2 = E_T_numE_4[:, :, 0:1] * E_T_numE_4[:, :, 3:4] - E_T_numE_4[:, :, 1:2] * E_T_numE_4[:, :, 2:3]
            true_W_T_numE_1 = (lmbda /2 ) * I1 **2 + mu * (I1 **2 - 2 * I2)
            true_P_T_numE_4 = torch.autograd.grad(true_W_T_numE_1.sum(), F_T_numE_4, create_graph=True)[0]

            del F_T_numE_4, E_T_numE_4, I1, I2

        else:
            raise RuntimeError("args.material invalid")

        total_W_MAE += torch.sum(torch.abs(energy_T_numE_1 - true_W_T_numE_1) ).item()
        total_P_MAE += torch.sum(torch.abs(P_T_numE_4 - true_P_T_numE_4) ).item()

        total_W_scale += torch.sum(torch.abs(true_W_T_numE_1) ).item()
        total_P_scale += torch.sum(torch.abs(true_P_T_numE_4) ).item()

        count_element += this_batch_size * num_elements

        del true_W_T_numE_1, true_P_T_numE_4, P_T_numE_4, energy_T_numE_1
        

    average_P_MAE = total_P_MAE / count_element
    average_W_MAE = total_W_MAE / count_element
    average_P_scale = total_P_scale / count_element
    average_W_scale = total_W_scale / count_element


    with open(log_file, "a") as f:
        print("###" * 4, file = f)
        print("predict_W_MAE : {}".format(average_W_MAE), file = f)
        print("predict_P_MAE : {}".format(average_P_MAE), file = f)        
        print("average_P_scale : {}".format(average_P_scale), file = f)
        print("average_W_scale : {}".format(average_W_scale), file = f)









### main ###
if __name__ == "__main__":
    """
    ------------- define NN model -------------
    """
    energy_model = energy_density_NN(args.input_dim, args.hidden_dim, args.activation)            
    if cuda_flag:
        energy_model.cuda()

    
    """
    ------------- load data -------------
    """
    # args.material = "NH" # material
    args.material = "VK"

    assert args.material in ["NH", "VK"]

    if args.material == "NH":
        prefix = ""
    else:
        prefix = "VK_"
    
    # interpolate_list = [0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01]
    interpolate_list = [0]


    # exp_list = [1, 2, 3]
    # exp_list = [2, 3]
    exp_list = [2]


    for interpolate_size in interpolate_list:
        if interpolate_size == 0:
            data_dir = "../data_process/{}GNN_input_0.001/".format(prefix)
        else:
            data_dir = "../data_process/{}GNN_input_interpolation_0.001/{}/".format(prefix, interpolate_size)
        print("data_dir: ", data_dir)

    
        train_loader, valid_loader, EleWeights_numE, connectivity, gradNa, vertex_boundary_mask = load_data(
            data_dir, args.batch_size, save_min_max_dir= None)
        vertex_internal_mask = ~vertex_boundary_mask
        train_loader = None

        if cuda_flag:
            EleWeights_numE = EleWeights_numE.cuda()
            for a in range(args.num_nodes_per_element):
                connectivity[a] = connectivity[a].cuda()
                gradNa[a] = gradNa[a].cuda()
        
        for exp in exp_list:

            saved_model_folder = "./trained_{}ele_0.001_float64/interpolate_{}/exp{}/".format(prefix, interpolate_size, exp)
            print("saved_model_folder: ", saved_model_folder)

            """
            ------------- Load the trained NN model -------------
            """    

            best_trained_model_file = saved_model_folder + 'best_trained_model.pt'
            energy_model.load_state_dict(torch.load(best_trained_model_file))


            # for normalize INPUT
            with open(saved_model_folder + "greenStrain_max.pkl", "rb") as f:
                greenStrain_max = pkl.load(f)
            print("greenStrain_max: ", greenStrain_max)
            # for unnormalize OUTPUT
            with open(saved_model_folder + "momentum_max.pkl", "rb") as f:
                momentum_max_dict = pkl.load(f)
            momentum_max = momentum_max_dict["momentum_max"]
            print("momentum_max: ", momentum_max)



            ## Testing output file
            log_file = saved_model_folder + "test_output.txt"
            with open(log_file, "w") as f:
                print("args: ", args, file = f)
                print("momentum_max: ", momentum_max, file = f)
                print("data_dir: ", data_dir, file = f)
                print("saved_model_folder: ", saved_model_folder, file = f)
    

    
            ## Verify the assembly 
            cmpt_momentum_inconsistency(valid_loader)


            ## momentum error
            cmpt_predicted_momentum_error(valid_loader)


            ## W and P error
            cmpt_predicted_W_and_P_error(valid_loader)
            


# Change the setting in __main__
# CUDA_VISIBLE_DEVICES=3 python test_sparse.py             