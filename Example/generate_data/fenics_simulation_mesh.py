from fenics import *
import numpy as np
from mpi4py import MPI
import pathlib
import pickle as pkl

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--ele_size', type=float, default= 0.01, help= 'mesh size for simulation')
args = parser.parse_args()


L = 1
H = 0.4
elem_size = args.ele_size



num_points_x = int(L/elem_size)
num_points_y = int(H/elem_size)


mesh = RectangleMesh(Point(0, 0), Point(L, H), num_points_x, num_points_y)
print(mesh.num_vertices())
print(mesh.num_cells())

save_folder = "mesh_{}/".format(elem_size)
pathlib.Path(save_folder).mkdir(parents=True, exist_ok=True)


## mesh coordinates
mesh_coordinates = mesh.coordinates()

# boundary indicator
num_vertex = len(mesh_coordinates)
tol = 1E-14
boundary_indicator = np.zeros((num_vertex), dtype=bool)
for i in range(num_vertex):
    x = mesh_coordinates[i]
    x0_boundary_mark = 0
    x1_boundary_mark = 0
    if near(x[0], 0, tol) or near(x[0], L, tol):
        boundary_indicator[i] = True
    else:
        boundary_indicator[i] = False


## connectivity
ele_node_array_numE_3 = mesh.cells()
print("ele_node_array_numE_3")
print(ele_node_array_numE_3)

res = {
    "coordinates": mesh_coordinates,
    "boundary_indicator": boundary_indicator,
    "ele_node_array_numE_3": ele_node_array_numE_3
}
with open(save_folder + "mesh_info.pkl", "wb") as f:
    pkl.dump(res, f)