import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.font_manager as font_manager
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import ticker
from matplotlib import gridspec
from matplotlib.collections import LineCollection
from matplotlib.ticker import FormatStrFormatter

class OOMFormatter(mpl.ticker.ScalarFormatter):
    def __init__(self, order=0, fformat="%1.1f", offset=True, mathText=True):
        self.oom = order
        self.fformat = fformat
        mpl.ticker.ScalarFormatter.__init__(self,useOffset=offset,useMathText=mathText)
    def _set_order_of_magnitude(self):
        self.orderOfMagnitude = self.oom
    def _set_format(self, vmin=None, vmax=None):
        self.format = self.fformat
        if self._useMathText:
             self.format = r'$\mathdefault{%s}$' % self.format

def put_label(ax, label, fontsize=10):
    ax.text(0.05, 0.95, label, transform=ax.transAxes,
      fontsize=fontsize, fontweight='bold', va='top')

def set_size(fraction=1, height_ratio='golden', width='two-column', subplots=(1, 1)):
    if width == 'two-column':
        width_pt = 180 # mm
    elif width == 'one-column':
        width_pt = 90 # mm
    else:
        width_pt = width

    if height_ratio == 'golden':
        ratio_pt = (np.sqrt(5) - 1.0) / 2.0
    else:
        ratio_pt = height_ratio
        
    fig_width_pt = width_pt * fraction
    inches_per_pt = 1.0 / 25.4
    fig_width_in = fig_width_pt * inches_per_pt
    fig_height_in = fig_width_in * ratio_pt * (subplots[0]/subplots[1])
    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def plot_imshow(fig, ax, quantity, title, cmap='inferno'):
    cs = ax.imshow(quantity, origin='lower', cmap=cmap)
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)

    axins1 = ax.inset_axes([0.1, 1.085, 0.8, 0.05])

    cbar = fig.colorbar(cs, cax=axins1, pad=0.05, 
                      fraction=0.025, shrink=.5, orientation="horizontal")
    tick_locator = ticker.MaxNLocator(nbins=2)
    cbar.locator = tick_locator
    cbar.update_ticks()
    cbar.set_label(title, fontsize=8.5, labelpad=-28, y=0.85)
    cbar.ax.yaxis.set_offset_position('right')  