# copied from visualization_stress_field3.py

import os
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.colors as mcolors
from matplotlib.collections import LineCollection
from matplotlib import rc,rcParams

from plot_helper_for_paper import set_size
plt.style.use(['prl_paper.mplstyle'])
import colorcet

import pickle as pkl
import sys

sys.path.append('..')

from GNN.model import energy_density_NN

import argparse


parser = argparse.ArgumentParser()


parser.add_argument('--num_nodes_per_element', type=int, default=3, help='Number of vertex on each element.') 
parser.add_argument('--space_dim', type=int, default=2, help='space dimension') 

parser.add_argument('--batch_size', type=int, default=1, help='Number of samples per batch.')
parser.add_argument('--input_dim', type=int, default=2, help='Two invariants.')
parser.add_argument('--hidden_dim', type=int, default=128, help='Hidden dimension.')
parser.add_argument('--activation', type=str, default='elu', help='[]')

# parser.add_argument('--material', type= str, default= "-1", help= "[AB, Fung, Gent, MR, NH, VK]")
# parser.add_argument('--F_path', type= str, default= "-1", help= "uniaxial tension (UT), uniaxial compression (UC), biaxial tension (BT), biaxial compression (BC), simple shear (SS) and pure shear (PS).")

parser.add_argument('--gamma_steps', type=int, default=200, help='For plotting')
parser.add_argument('--exterpolation_ratio', type= float, default=0.2, help= "for the exterpolation test")
args = parser.parse_args()


def cmpt_cauchy_green_deformation(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2

    C = torch.cat((C11,C12,C21,C22), dim=-1)
    return C



def cmpt_green_strain_tensor(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2
    
    E11 = C11 - 1.0
    E12 = C12
    E21 = C21
    E22 = C22 - 1.0
    return 0.5 * torch.cat((E11, E12, E21, E22), dim= -1)



def return_time_steps(dir):
    file_list = os.listdir(dir)
    used_time_steps = []
    for f_name in file_list:
        if f_name[0] != "a":
            continue
        f_name = f_name.split(".")[0]
        used_time_steps.append(int(f_name[2:]))
    return np.sort(used_time_steps)



def cmpt_groundTruth_W_and_P(F_numExample_4)  -> np.array:
    Youngs_modulus = 1e4
    nu= 0.3 # Poisson rate
    mu  = Youngs_modulus / (2.0*(1.0 + nu))
    lmbda = Youngs_modulus * nu / ((1.0 + nu)*(1.0 - 2.0*nu))
    bulk_K = Youngs_modulus / (3.0 * (1.0 - 2 * nu) ) #bulk modulus

    if material == "NH":
        # New-Hookean
        F_numExample_4.requires_grad = True
        C_numExample_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_numExample_1 = C_numExample_4[:, 0:1] + C_numExample_4[:, 3:4]
        J_numExample_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        W = (mu / 2) * (I1_numExample_1 - 2) - mu * torch.log(J_numExample_1) + (lmbda / 2) * (torch.log(J_numExample_1))**2 # 2D
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "VK":            
        F_numExample_4.requires_grad = True
        E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4)
        I1 = E_numExample_4[:, 0:1] + E_numExample_4[:, 3:4]
        I2 = E_numExample_4[:, 0:1] * E_numExample_4[:, 3:4] - E_numExample_4[:, 1:2] * E_numExample_4[:, 2:3]
        W = (lmbda /2 ) * I1 **2 + mu * (I1 **2 - 2 * I2)
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    
    elif material == "MR":
        C10 = 7. * mu / 16
        C01 = 1. * mu / 16
        
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I2_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        I1_bar = J_T_numE_1 ** (- 2. / 2.) * I1_T_numE_1
        I2_bar = J_T_numE_1 ** (- 4. / 2.) * I2_T_numE_1
        term1 = C10 * (I1_bar - 2)
        term2 = C01 * (I2_bar - 1)
        term3 = 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        W = term1 + term2 + term3
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Fung":
        b = 1.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        exponential = b * (I1_T_numE_1 * ( I3_T_numE_1 ** (-0.5) ) - 2)
        W = mu / (2 * b) * ( exponential + torch.exp(exponential) - 1.0 ) + 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Gent":
        Jm = 10
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        W_inc = - (mu / 2) * (Jm * torch.log( 1 - ( I3_T_numE_1 ** (-0.5) * I1_T_numE_1 - 2.0) / Jm )  )
        W_vol = 0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W_inc + W_vol
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "AB":
        C1 = mu
        N = 10.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_bar = I3_T_numE_1 ** (-0.5) * I1_T_numE_1
        W1 = C1 * ( 0.5 * (I1_bar - 2) + (1 / (20 * N) ) * (I1_bar**2  - 4) + (11 / (N ** 2 * 1050 ) ) * (I1_bar**3  - 8) + (19 / (N ** 3 * 7000 ) ) * (I1_bar**4  - 16) + (519 / (N ** 4 * 673750 ) ) * (I1_bar**5  - 32) )
        W2 =  0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W1 + W2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    else:
        raise RuntimeError("material error")
    
    return W.detach().numpy(), P_numExample_4.detach().numpy()


def predict_W_and_P(F_numExample_4) -> np.array:
    zero_strain_1_4 = torch.zeros((1, 4) )
    if cuda_flag:
        zero_strain_1_4 = zero_strain_1_4.cuda()
    zero_strain_1_4.requires_grad = True
    


    ## input data
    E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4) # green strain tensor
    E_numExample_4 = torch.autograd.Variable(E_numExample_4)
    E_numExample_4.requires_grad = True

    # normalize INPUT Green strain
    normalizedE_numExample_4 = E_numExample_4 / greenStrain_max

    energy_numExample_1 = energy_model(normalizedE_numExample_4)
    S_numExample_4 = torch.autograd.grad(energy_numExample_1.sum(), E_numExample_4, create_graph=True)[0]

    ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
    zeroEnergy_1_1 = energy_model(zero_strain_1_4)
    zeroS_1_4  = torch.autograd.grad(zeroEnergy_1_1.sum(), zero_strain_1_4, create_graph = True)[0]
    zeroS_1_2_2 = zeroS_1_4.reshape(1, 2, 2)
    zeroP_1_2_2 = zeroS_1_2_2 # because of the unit_F_1_1_2_2
    
    # energy
    energy_numExample_1 = energy_numExample_1 - zeroEnergy_1_1
    

    ## correct the predicted stress
    S_numE_2_2 = S_numExample_4.reshape(S_numExample_4.shape[0],  2, 2) # last two dimensions are i and j. sum over j
    F_numE_2_2 = F_numExample_4.reshape(F_numExample_4.shape[0], 2, 2)
    P_numE_2_2 = torch.matmul(F_numE_2_2, S_numE_2_2)
    P_numE_2_2 = P_numE_2_2 - zeroP_1_2_2
    P_numExample_4 = P_numE_2_2.reshape(P_numE_2_2.shape[0], 4)
    
    # unnormalize predictions
    P_numExample_4 = P_numExample_4 * momentum_max
    energy_numExample_1 = energy_numExample_1 * momentum_max
    
    # return energy_numExample_1.detach().numpy() * greenStrain_max, P_numExample_4.detach().numpy()
    return energy_numExample_1.cpu().detach().numpy(), P_numExample_4.cpu().detach().numpy()



### color map
def make_colormap(seq):
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return mcolors.LinearSegmentedColormap('CustomMap', cdict)


def get_box_edges(triang):
    border_edges = []
    x_ticks = set([x/ 100. for x in range(4, 100, 5)] )
    y_ticks = set([y/ 100. for y in range(4, 40, 5)] )
    for i in range(len(triang.edges) ):
        this_v1 = triang.edges[i, 0]
        this_v2 = triang.edges[i, 1]
        if vertex_coordinates_N_2[this_v1, 1] == 0 and vertex_coordinates_N_2[this_v2, 1] == 0:
            if vertex_coordinates_N_2[this_v1, 0] <= 0.5 and vertex_coordinates_N_2[this_v2, 0] <= 0.5:
                if round(vertex_coordinates_N_2[this_v1, 0], 2) in x_ticks or round(vertex_coordinates_N_2[this_v2, 0], 2) in x_ticks:
                    continue
                border_edges.append([this_v1, this_v2])
        elif vertex_coordinates_N_2[this_v1, 1] == 0.2 and vertex_coordinates_N_2[this_v2, 1] == 0.2:
            if vertex_coordinates_N_2[this_v1, 0] <= 0.5 and vertex_coordinates_N_2[this_v2, 0] <= 0.5:
                if round(vertex_coordinates_N_2[this_v1, 0], 2) in x_ticks or round(vertex_coordinates_N_2[this_v2, 0], 2) in x_ticks:
                    continue
                border_edges.append([this_v1, this_v2])
        
        elif vertex_coordinates_N_2[this_v1, 0] == 0 and vertex_coordinates_N_2[this_v2, 0] == 0:
            if vertex_coordinates_N_2[this_v1, 1] <= 0.2 and vertex_coordinates_N_2[this_v2, 1] <= 0.2:
                if round(vertex_coordinates_N_2[this_v1, 1], 2) in y_ticks or round(vertex_coordinates_N_2[this_v2, 1], 2) in y_ticks:
                    continue
                border_edges.append([this_v1, this_v2])
        
        elif vertex_coordinates_N_2[this_v1, 0] == 0.5 and vertex_coordinates_N_2[this_v2, 0] == 0.5:
            if vertex_coordinates_N_2[this_v1, 1] <= 0.2 and vertex_coordinates_N_2[this_v2, 1] <= 0.2:
                if round(vertex_coordinates_N_2[this_v1, 1], 2) in y_ticks or round(vertex_coordinates_N_2[this_v2, 1], 2) in y_ticks:
                    continue
                border_edges.append([this_v1, this_v2])        
        
    return border_edges




def get_box_edges_20percents(triang):
    border_edges = []
    large_x = 0.4
    large_y = 0.39
    small_x = 0.01
    small_y = 0.25
    for i in range(len(triang.edges) ):
        this_v1 = triang.edges[i, 0]
        this_v2 = triang.edges[i, 1]
        if vertex_coordinates_N_2[this_v1, 1] == small_y and vertex_coordinates_N_2[this_v2, 1] == small_y:
            if vertex_coordinates_N_2[this_v1, 0] <= large_x and vertex_coordinates_N_2[this_v2, 0] <= large_x:
                if vertex_coordinates_N_2[this_v1, 0] >= small_x and vertex_coordinates_N_2[this_v2, 0] >= small_x:
                    border_edges.append([this_v1, this_v2])
        elif vertex_coordinates_N_2[this_v1, 1] == large_y and vertex_coordinates_N_2[this_v2, 1] == large_y:
            if vertex_coordinates_N_2[this_v1, 0] <= large_x and vertex_coordinates_N_2[this_v2, 0] <= large_x:
                if vertex_coordinates_N_2[this_v1, 0] >= small_x and vertex_coordinates_N_2[this_v2, 0] >= small_x:
                    border_edges.append([this_v1, this_v2])
        
        elif vertex_coordinates_N_2[this_v1, 0] == small_x and vertex_coordinates_N_2[this_v2, 0] == small_x:
            if vertex_coordinates_N_2[this_v1, 1] <= large_y and vertex_coordinates_N_2[this_v2, 1] <= large_y:
                if vertex_coordinates_N_2[this_v1, 1] >= small_y and vertex_coordinates_N_2[this_v2, 1] >= small_y:
                    border_edges.append([this_v1, this_v2])
        
        elif vertex_coordinates_N_2[this_v1, 0] == large_x and vertex_coordinates_N_2[this_v2, 0] == large_x:
            if vertex_coordinates_N_2[this_v1, 1] <= large_y and vertex_coordinates_N_2[this_v2, 1] <= large_y:
                if vertex_coordinates_N_2[this_v1, 1] >= small_y and vertex_coordinates_N_2[this_v2, 1] >= small_y:
                    border_edges.append([this_v1, this_v2])        
        
    return border_edges




def get_border_edges_rectangle(triang):
    border_edges = []
    for i in range(len(triang.edges) ):
        this_v1 = triang.edges[i, 0]
        this_v2 = triang.edges[i, 1]
        if vertex_coordinates_N_2[this_v1, 1] == 0 and vertex_coordinates_N_2[this_v2, 1] == 0:            
            border_edges.append([this_v1, this_v2])
        elif vertex_coordinates_N_2[this_v1, 1] == 0.4 and vertex_coordinates_N_2[this_v2, 1] == 0.4:
            border_edges.append([this_v1, this_v2])
        
        elif vertex_coordinates_N_2[this_v1, 0] == 0 and vertex_coordinates_N_2[this_v2, 0] == 0:
            border_edges.append([this_v1, this_v2])
        
        elif vertex_coordinates_N_2[this_v1, 0] == 1.0 and vertex_coordinates_N_2[this_v2, 0] == 1.0:
            border_edges.append([this_v1, this_v2])        
        
    return border_edges



def plot_colorbar(fig, ax, cs, pos):
    # ax.axes.get_xaxis().set_visible(False)
    # ax.axes.get_yaxis().set_visible(False)

    # axins1 = ax.inset_axes([0.1, 1.085, 0.8, 0.05])
    # axins1 = ax.inset_axes(pos)
    # fig.canvas.draw()
    # ax_pos = ax.get_position()    
    # left, bottom, width, height  = ax_pos.x0, ax_pos.y1, ax_pos.width, ax_pos.height
    # axins1 = fig.add_axes([left, bottom * 1.01, width, height * 0.05])
    axins1 = fig.add_axes(pos)

    cbar = fig.colorbar(cs, cax=axins1, pad=0.05, fraction=0.025, shrink=.2, orientation="horizontal")
    cbar.ax.xaxis.set_tick_params(pad=1)
    tick_locator = matplotlib.ticker.MaxNLocator(nbins=2)
    cbar.locator = tick_locator
    cbar.ax.xaxis.set_major_locator(tick_locator)
    cbar.update_ticks()
    # cbar.ax.yaxis.set_offset_position('left')  

    formatter = matplotlib.ticker.ScalarFormatter(useMathText= True, useOffset=False)
    # formatter.set_powerlimits((-1, 1))
    formatter.set_powerlimits((0, 0))
    cbar.ax.xaxis.set_major_formatter(formatter)

    plt.gcf().canvas.draw()
    offset_text = cbar.ax.xaxis.get_offset_text().get_text()
    cbar.ax.get_xaxis().get_offset_text().set_visible(False)
    cbar.ax.text(1.01, -0., offset_text, transform=cbar.ax.transAxes, ha='left', va='baseline')




    
def plot_W_P_field():
    num_time_steps = len(used_time_steps)
    assert num_time_steps == 2
    
    
    ## colormaps    
    energy_cmap = colorcet.cm.coolwarm    
    stress_cmap = colorcet.cm.cwr
    c = mcolors.ColorConverter().to_rgb
    min_rgb_value = np.array(c('white')) # yellow, gold,  goldenrod, 
    max_rgb_value = np.array(c('xkcd:dark grey'))# maroon, brown, tomato, darkorange, orangered, sienna, chocolate, saddlebrown, khaki, fuchsia
    # max_rgb_value = np.array([173., 46., 182.]) / 255.
    difference_cmap = make_colormap([tuple(min_rgb_value), tuple(min_rgb_value/2.0 + max_rgb_value/2.0), 0.5, tuple(min_rgb_value/2.0 + max_rgb_value/2.0), tuple(max_rgb_value)])

    colorbar_height = 0.05
    colorbar_vp = 1.06
    lineCollection_linewidth = 0.1
    sub_domain_border_linewidth = 0.7
    for index, time_id in enumerate(range(num_time_steps)):
        # fig = plt.figure(figsize=set_size(width='two-column', fraction=1.0, height_ratio= "golden"), constrained_layout=True) # height_ratio='golden'

        # gs0 = matplotlib.gridspec.GridSpec(1, 2,  wspace=0.,  figure=fig)

        # gs00 = [matplotlib.gridspec.GridSpecFromSubplotSpec(2, 3, subplot_spec=gs0[0], wspace=0, hspace = 0.2), 
        #         matplotlib.gridspec.GridSpecFromSubplotSpec(2, 3, subplot_spec=gs0[1], wspace=0, hspace = 0.2)]


        # gs1 = fig.add_gridspec(2, 6, wspace=0.001, hspace=0.0001)


        this_time = used_time_steps[time_id]

        vertexPos_N_2 = vertex_coordinates_N_2 + displacement_fields[this_time]

        ## construct the tri.Triangulation
        rotate_vertexPos_N_2 = np.zeros(shape= vertexPos_N_2.shape)
        rotate_vertexPos_N_2[:, 0] = -  vertexPos_N_2[:, 1]
        rotate_vertexPos_N_2[:, 1] =  vertexPos_N_2[:, 0]
        triang = tri.Triangulation(rotate_vertexPos_N_2[:, 0], rotate_vertexPos_N_2[:, 1], ele_node_array_numE_3 )

        if sim_case != 3:
            border_edges  = get_border_edges_rectangle(triang)
        else:
            raise RuntimeError("invalid simcase for Exp5")
            border_edges = get_border_edges_ellipse(triang)

        box_edges  = get_box_edges(triang) # the box of observation
        # box_edges  = get_box_edges_20percents(triang) # the box of observation
        

        ## W
        true_W_numEle_1 = true_W_fields[this_time][:, 0]
        predict_W_numEle_1 = predict_W_fields[this_time][:, 0]
        # diff_abs = np.abs(predict_P_numEle_1 - true_P_numEle_1)
        print(np.sort(true_W_numEle_1)[-100: ])
        print(np.sort(predict_W_numEle_1)[-100: ])

        print("true_W_numEle_1 shape", true_W_numEle_1.shape)
        print("predict_W_numEle_1 shape", predict_W_numEle_1.shape)

        W_min = np.min([np.min(true_W_numEle_1), np.min(predict_W_numEle_1)])
        W_max = np.max([np.max(true_W_numEle_1), np.max(predict_W_numEle_1)])

        ## P
        true_P_numEle_1 = true_P_fields[this_time][:, 0]
        predict_P_numEle_1 = predict_P_fields[this_time][:, 0]
        print("true_P_numEle_1 shape", true_P_numEle_1.shape)
        print("predict_P_numEle_1 shape", predict_P_numEle_1.shape)
        P_min = np.min([np.min(true_P_numEle_1), np.min(predict_P_numEle_1)])
        P_max = np.max([np.max(true_P_numEle_1), np.max(predict_P_numEle_1)])

        

        ##################################
        # plot W
        ##################################

        ## plot true W
        fig = plt.figure(figsize=set_size(width='one-column', fraction=0.9, height_ratio= 0.5), constrained_layout=False) # height_ratio='golden'
        # gs1 = gs00[index]
        gs1 = fig.add_gridspec(1, 3, wspace=0., hspace=0.)

        # ax = fig.add_subplot(gs1[0, time_id * 3 + 0])
        ax0 = fig.add_subplot(gs1[0, 0])
        ax0.set_aspect('auto')
        ax0.axes.get_xaxis().set_ticks([])
        ax0.axes.get_yaxis().set_ticks([])
        pos_original = ax0.get_position() # get the original position 
        pos_new = [pos_original.x0, pos_original.y0,  pos_original.width, pos_original.height] 
        # ax0.set_position(pos_new) # set a new position

        tpc_true = ax0.tripcolor(triang, true_W_numEle_1, vmin= W_min, vmax= W_max, shading='flat', antialiased=True, linewidth=0.5, edgecolors='face', cmap= energy_cmap, rasterized=True)
        # cax = fig.add_axes([axs[time_id, 0].get_position().x1+0.01, axs[time_id, 0].get_position().y0, 0.02, axs[time_id, 0].get_position().height])
        # tpc_true = axs[time_id, 0].tripcolor(triang, true_W_numEle_1, shading='flat', antialiased=True, linewidth=0.001, edgecolors='face', cmap= energy_cmap)
        lc = LineCollection(rotate_vertexPos_N_2[border_edges], linewidths= lineCollection_linewidth, colors= "black")
        ax0.add_collection(lc)
        lc = LineCollection(rotate_vertexPos_N_2[box_edges], linewidths= sub_domain_border_linewidth, colors= "black")
        ax0.add_collection(lc)
        
        # plot_colorbar(fig, ax0, tpc_true, [0.4, colorbar_vp, 1.8, colorbar_height])        
        fig.canvas.draw()
        ax_pos = ax0.get_position()    
        left, bottom, width, height  = ax_pos.x0, ax_pos.y1, ax_pos.width, ax_pos.height
        # plot_colorbar(fig, ax0, tpc_true, [left * 1.3, bottom * 1.01, width * 2, height * 0.05])        
        # text
        # plt.gcf().canvas.draw()
        # ax0.text(-.001, 0.8, "$W$", transform=ax0.transAxes, ha='right', va='bottom')
        # ax0.text(-0.1, 1., "t=" + " {}".format(this_time), transform=ax0.transAxes, ha='right', va='bottom', fontweight="semibold")
        
        ax0.spines['top'].set_visible(False)
        ax0.spines['right'].set_visible(False)
        ax0.spines['bottom'].set_visible(False)
        ax0.spines['left'].set_visible(False)
        # ax0.set_xlabel( r'$W$', labelpad= 0.2, fontsize=8)

        ax0.set_title(r'${W}$', y=0.92, fontsize=8)
        ax0.set_ylabel( r'$y$', labelpad= 0.5, fontsize=8)
        ax0.set_xlabel(r'$x$', labelpad=-3, fontsize=8)
        
       
        ## plot predict W
        # ax1 = fig.add_subplot(gs1[0, time_id * 3 + 1])
        ax1 = fig.add_subplot(gs1[0, 1])
        ax1.set_aspect('auto')
        # ax1.set(xlim=(x_pos_min - 0.02 , x_pos_max + 0.02))        
        ax1.axes.get_xaxis().set_ticks([])
        ax1.axes.get_yaxis().set_ticks([])
        pos_original = ax1.get_position() # get the original position 
        pos_new = [pos_original.x0  - 0.04, pos_original.y0,  pos_original.width, pos_original.height] 
        # ax1.set_position(pos_new) # set a new position

        tpc_predict = ax1.tripcolor(triang, predict_W_numEle_1,  vmin= W_min, vmax= W_max, shading='flat', antialiased=True, linewidth=0.5, edgecolors='face', cmap= energy_cmap, rasterized=True)
        # tpc_predict = ax1.tripcolor(triang, predict_W_numEle_1, shading='flat', antialiased=True, linewidth=0.001, edgecolors='face', cmap= energy_cmap)
        # cax = fig.add_axes([ax1.get_position().x1+0.01, ax1.get_position().y0, 0.02, ax1.get_position().height])
        lc = LineCollection(rotate_vertexPos_N_2[border_edges], linewidths= lineCollection_linewidth, colors= "black")
        ax1.add_collection(lc)
        lc = LineCollection(rotate_vertexPos_N_2[box_edges], linewidths= sub_domain_border_linewidth, colors= "black")
        ax1.add_collection(lc)

        fig.canvas.draw()
        ax_pos = ax1.get_position()    
        right, bottom, width, height  = ax_pos.x1, ax_pos.y1, ax_pos.width, ax_pos.height
        plot_colorbar(fig, ax1, tpc_predict, [(left + right) / 2 - width * 0.8, bottom * 1.16, width * 1.5, height * 0.05])        

        # text
        # plt.gcf().canvas.draw()
        # ax1.text(-.001, 0.8, "$\widehat{W}$", transform=ax1.transAxes, ha='right', va='bottom')

        ax1.spines['top'].set_visible(False)
        ax1.spines['right'].set_visible(False)
        ax1.spines['bottom'].set_visible(False)
        ax1.spines['left'].set_visible(False)
        # ax1.set_xlabel( r'$\widehat{W}$', labelpad=0.2, fontsize=8)

        ax1.set_title(r'$\widehat{W}$', y=0.92, fontsize=8)
        ax1.set_xlabel( r'$x$', labelpad= -3, fontsize=8)


        ## plot W difference
        # ax2 = fig.add_subplot(gs1[0, time_id * 3 + 2])
        ax2 = fig.add_subplot(gs1[0, 2])
        ax2.axes.get_xaxis().set_ticks([])
        ax2.axes.get_yaxis().set_ticks([])
        ax2.set_aspect('auto')
        # ax2.set(xlim=(x_pos_min - 0.02 , x_pos_max + 0.02))        
        
        pos_original = ax2.get_position() # get the original position 
        pos_new = [pos_original.x0 - 0.06, pos_original.y0,  pos_original.width, pos_original.height] 
        # ax2.set_position(pos_new) # set a new position
        W_diff = np.abs(predict_W_numEle_1 - true_W_numEle_1) / np.abs(true_W_numEle_1)
        tpc_diff = ax2.tripcolor(triang, W_diff, shading='flat', antialiased=True, linewidth=0.5, edgecolors='face', cmap= difference_cmap, rasterized=True)
        lc = LineCollection(rotate_vertexPos_N_2[border_edges], linewidths= lineCollection_linewidth, colors= "black")
        ax2.add_collection(lc)
        lc = LineCollection(rotate_vertexPos_N_2[box_edges], linewidths= sub_domain_border_linewidth, colors= "black")
        ax2.add_collection(lc)

        fig.canvas.draw()
        ax_pos = ax2.get_position()    
        left, bottom, width, height  = ax_pos.x0, ax_pos.y1, ax_pos.width, ax_pos.height
        plot_colorbar(fig, ax2, tpc_diff, [left* 1.05, bottom * 1.16, width * 0.7, height * 0.05])
        # text
        # plt.gcf().canvas.draw()
        # ax2.text(-.001, 0.8, "$|W - \widehat{W}|$", transform=ax2.transAxes, ha='right', va='bottom')


        ax2.spines['top'].set_visible(False)
        ax2.spines['right'].set_visible(False)
        ax2.spines['bottom'].set_visible(False)
        ax2.spines['left'].set_visible(False)
        # ax2.set_xlabel( r'${|\widehat{W} - W|}~ / ~ {|W| }$', labelpad= 0.1, fontsize=8)
        
        ax2.set_title(r'$|W - \widehat{W}|~/~|W|$', y=0.92, fontsize=8)
        ax2.set_xlabel( r'$x$', labelpad= -3, fontsize=8)

        plt.savefig(save_fig_folder + '{}_W_t{}.pdf'.format(material, this_time), bbox_inches='tight', pad_inches = 0.03, dpi=300, format='pdf')
        plt.close()









if __name__ == "__main__":

    cuda_flag = False
    # material_list = ["NH", "VK", "MR", "Fung", "Gent", "AB"]
    material = "NH"

    sim_case = 1

    # time_step_id_list = [0, 1, 15]
    # time_step_id_list = [0, 16, 200]
    time_step_id_list = [0, 200]

    exp_id = 1 # or compute the mean from multiple experiments

    true_W_fields = {} # true_W_fields[time_step] is np.array() with shape (num_ele, 1)
    predict_W_fields = {}# predict_W_fields[time_step] is np.array() with shape (num_ele, 1)
    true_P_fields = {} # true_P_fields[time_step] is np.array() with shape (num_ele, 4)
    predict_P_fields = {}# true_P_fields[time_step] is np.array() with shape (num_ele, 4)

    displacement_fields = {}# displacement_fields[time_step] is np.array() with shape (num_node, 2), for constructing triangle position
    used_time_steps = []

    # node coordinate in the reference frame
    simulation_folder = "../../Experiment1_different_material_backup/stress_field_visualization/case{}/NH_data_0.01/".format(sim_case)
    GNN_input_folder = "../../Experiment1_different_material_backup/stress_field_visualization/case{}/NH_data_0.01_F/".format(sim_case)

    save_fig_folder = "case{}/".format(sim_case)
    os.makedirs(save_fig_folder, exist_ok=True)

    vertex_coordinates_N_2 = np.load(simulation_folder + "coordinates.npy")


    # connectivity, for constructing tri.Triangulation
    with open(simulation_folder + "mesh_info.pkl", "rb") as f:
        mesh_info = pkl.load(f)
        assert (vertex_coordinates_N_2 == mesh_info["coordinates"] ).all()
        ele_node_array_numE_3 = mesh_info["ele_node_array_numE_3"].astype(np.int64)
    
    # deformation gradient 
    with open(GNN_input_folder + "deformation_gradient.pkl", "rb") as f:
        F_T_numEle_4 = pkl.load(f) 
    assert len(F_T_numEle_4.shape) == 3
    

    
    # all available time steps
    available_time_steps = return_time_steps(simulation_folder)
    print("available_time_steps: ", available_time_steps)
    assert F_T_numEle_4.shape[0] == len(available_time_steps)



    ## the trained model
    trained_model_folder = "../GNN/trained_NH_ele_0.001_float64/interpolate_0.002/unmasked_percent_0.25/"
    energy_model = energy_density_NN(args.input_dim, args.hidden_dim, args.activation)    
    best_trained_model_file = trained_model_folder + "exp{}/".format(exp_id) + "best_trained_model.pt"
    energy_model.load_state_dict(torch.load(best_trained_model_file))

    with open(trained_model_folder + "exp{}/".format(exp_id) + "greenStrain_max.pkl", "rb") as f:
        greenStrain_max = pkl.load(f)
    with open(trained_model_folder + "exp{}/".format(exp_id) + "momentum_max.pkl", "rb") as f:
        momentum_max_dict = pkl.load(f)
        momentum_max = momentum_max_dict["momentum_max"]

    

    ### check every time step
    for time_id in time_step_id_list:
        this_time = available_time_steps[time_id]
        used_time_steps.append(this_time)
        print("time_id: ", time_id, " this_time: ", this_time)

        u_N_2 = np.load(simulation_folder + "u_{}.npy".format(this_time )  )
        # currentPos_N_2 = vertex_coordinates_N_2 + u_N_2
        displacement_fields[this_time] = u_N_2

        F_numEle_4 = F_T_numEle_4[time_id] # note the time_id


        # W and P, can also be mean here
        true_W_numEle_1, true_P_numEle_4 = cmpt_groundTruth_W_and_P(F_numEle_4)

        predict_W_numEle_1, predict_P_numEle_4 = predict_W_and_P(F_numEle_4)

        # predict_W_mean = np.stack(predict_W_mean)
        # predict_W_mean = np.mean(predict_W_mean, axis= 0)
        assert len(predict_W_numEle_1.shape) == 2
        assert predict_W_numEle_1.shape[-1] == 1
        # predict_P_mean = np.stack(predict_P_mean)
        # predict_P_mean = np.mean(predict_P_mean, axis= 0)
        assert len(predict_P_numEle_4.shape) == 2
        assert predict_P_numEle_4.shape[-1] == 4



        true_W_fields[this_time] = true_W_numEle_1
        predict_W_fields[this_time] = predict_W_numEle_1
        true_P_fields[this_time] = np.linalg.norm(true_P_numEle_4, axis= -1, keepdims= True) ## NOTE: norm of the stress
        predict_P_fields[this_time] = np.linalg.norm(predict_P_numEle_4, axis= -1, keepdims= True) ## NOTE: norm of the stress            
    

        

    plot_W_P_field()