## Learn constitutive relations with partial obervation
This folder contains the code for experiments in Sec.4.2.


### Instructions

1. Suppose the Neo-Hookean FEM simulation (at h_gen = 0.001) has been generated in in  `../Experiment_various_materials/generate_data/`  
   Otherwise, following the instructions in the README in `../Experiment_various_materials/generate_data/` to generate the simulation
   


2. Process the simulation and generate input for training the neural network, with partial observation
   
   ```bash
   # in data_process/
   bash construct_node_mask.bash
   ```

3. Train uLED
   
   ```bash
   #in uLED/
   CUDA_VISIBLE_DEVICES=0 python training_sparse.py # train uLED, with partial observation
   ```



4. Reproduce Fig.9
      ```bash
      # in learned_field_visualization/
      mkdir case1
      python visualization_energy_density_new.py
    
      # after this, figures should be generated in learned_field_visualization/case1/
      ```
