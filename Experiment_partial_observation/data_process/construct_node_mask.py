import pickle as pkl
import numpy as np
import pathlib
from fractions import Fraction


# based on the GNN_input of NH material in ../../Experiment_convergence/data_process/GNN_input_interpolation_0.001/0.002/
input_folder = "../../Experiment_convergence/data_process/GNN_input_interpolation_0.001/0.002/"

# save folder
save_folder = "./GNN_input_node_mask_NH_0.001/0.002/"
pathlib.Path(save_folder).mkdir(parents=True, exist_ok=True)


with open(input_folder + "vertex_coordinates_N_2.pkl", "rb") as f:
    vertex_coordinates_N_2 = pkl.load(f)
with open(input_folder + "vertex_boundary_mask.pkl", "rb") as f:
    vertex_boundary_mask = pkl.load(f)

print("vertex_coordinates_N_2 shape: ", vertex_coordinates_N_2.shape)
print("vertex_boundary_mask len: ", len(vertex_boundary_mask), ", vertex_boundary_mask type: ", type(vertex_boundary_mask), ", vertex_boundary_mask dtype: ", vertex_boundary_mask.dtype)
print("boundary vertex percentage: ", Fraction(vertex_boundary_mask.sum() /  np.size(vertex_boundary_mask) ).limit_denominator())
# print("internal vertex percentage: ", Fraction(vertex_internal_mask.sum() /  np.size(vertex_internal_mask) ).limit_denominator())

min_pos = np.min(vertex_coordinates_N_2, axis= 0)
max_pos = np.max(vertex_coordinates_N_2, axis= 0)

print("min_pos: ", min_pos)
print("max_pos: ", max_pos)





def unmased_percent_25_100():
    large_x = max_pos[0] * 0.5
    # large_y = max_pos[1] * 0.5
    small_y = max_pos[1] * 0.5

    num_nodes = len(vertex_coordinates_N_2)
    for i in range(num_nodes):
        boundary_flag = False
        this_x = vertex_coordinates_N_2[i, 0]
        this_y = vertex_coordinates_N_2[i, 1]
        
        if this_x > large_x or this_y < small_y:
            boundary_flag = True
        
        if vertex_boundary_mask[i] == True:
            vertex_boundary_mask[i] == True
        else:
            vertex_boundary_mask[i] = boundary_flag






unmased_percent_25_100()      





print("vertex_boundary_mask len: ", len(vertex_boundary_mask), ", vertex_boundary_mask type: ", type(vertex_boundary_mask), ", vertex_boundary_mask dtype: ", vertex_boundary_mask.dtype)
print("boundary vertex percentage: ", Fraction(vertex_boundary_mask.sum() /  np.size(vertex_boundary_mask) ).limit_denominator() )
print("boundary_percentage: ", vertex_boundary_mask.sum() /  np.size(vertex_boundary_mask) )


with open(save_folder + "unmasked_percent_0.2/vertex_boundary_mask.pkl", "wb") as f:
    pkl.dump(vertex_boundary_mask, f)
    print("saved vertex_boundary_mask.pkl to: ", save_folder + "vertex_boundary_mask.pkl")