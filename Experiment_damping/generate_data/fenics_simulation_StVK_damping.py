# Modified from FEM_data/beam/2D_beam_dynamic_newmark.py
# https://fenicsproject.discourse.group/t/save-the-result-of-mpirun-in-a-numpy-array/6669/3
# https://rabernat.github.io/research_computing/parallel-programming-with-mpi-for-python.html
# https://mpi4py.readthedocs.io/en/stable/tutorial.html
# https://education.molssi.org/parallel-programming/03-distributed-examples-mpi4py.html

# import fenics as fe
from fenics import *
import numpy as np
from mpi4py import MPI
import pathlib
from tqdm import tqdm


print('started')

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
print('My rank is ',rank)
# exit()

# Adjust log level
set_log_level(40)

# Turn on optimization
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

# num_quadrature_degree = 3 should be better for the mass matrix
parameters["form_compiler"]["quadrature_degree"] = 3

L = 1
H = 0.4
elem_size =  0.001
num_points_x = int(L/elem_size)
num_points_y = int(H/elem_size)


mesh = RectangleMesh(Point(0, 0), Point(L, H), num_points_x, num_points_y)
print(mesh.num_vertices())
print(mesh.num_cells())
# https://fenicsproject.discourse.group/t/number-of-elements-in-a-mesh/3152/2

sample_every_K_timeStep = 1

# Parameters for time-stepping
dt = 0.002
T = 3.0
beta = 0.25
gamma = 0.5


# Material parameters
rho = 1.0
E = 1e4
nu= 0.3

mu  = E / (2.0*(1.0 + nu))
lmbda = E * nu / ((1.0 + nu)*(1.0 - 2.0*nu))


# Rayleigh damping coefficients
damping_coefficient = 5E-4
eta_m = Constant(0.0)
eta_k = Constant(damping_coefficient)

tol = 1E-14



#------------------------------
# Define function space (P1-P1)
V = VectorFunctionSpace(mesh, "Lagrange", 1)

# Define left boundary
class LeftBoundary(SubDomain):
    def inside(self, x, on_boundary):
        # return x[0] <  DOLFIN_EPS
        return near(x[0], 0, tol)

# Define right boundary
class RightBoundary(SubDomain):
    def inside(self, x, on_boundary):
        # return x[0] > L - DOLFIN_EPS
        return near(x[0], L, tol)

# Mark boundaries
left_boundary = LeftBoundary()
right_boundary = RightBoundary()

boundaries = MeshFunction("size_t", mesh, mesh.topology().dim() - 1) 
boundaries.set_all(0)
left_boundary.mark(boundaries, 1)
right_boundary.mark(boundaries, 2)



# Redefine boundary measure
ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

# Define boundary conditions
clamp_bc = DirichletBC(V, (0, 0), boundaries, 1)
bcs = [clamp_bc]

# Define forces
B = Constant((0, 0)) # no body force
# G = Expression(("t<1.0 ? 20 : 0", "t<1.0 ? -30 : 0"), degree = 1, t=0) # for linear elasticity
G = Expression(("t<0.9 ? 20 : 0", "t<0.9 ? -30 : 0"), degree = 1, t=0) # for linear elasticity


# Define functions and test functions
v = TestFunction(V)
du = TrialFunction(V)

u0 = Function(V) # deformation at t0
u1 = Function(V) # deformation at t1, the solution in solve()

v0 = Function(V) # velocity at t0
a0 = Function(V) # acceleration at t0


u = u1 # the solution


# Define material constants
rho   = Constant(rho)
mu    = Constant(mu)
lmbda = Constant(lmbda)
h     = Constant(dt)
min_dt =  mesh.rmin() / (np.sqrt((2*float(mu)+float(lmbda)) / float(rho) ) )
print("used dt: ", dt, "min dt: ", min_dt)


# parameters for Newmark integration
beta = Constant(beta)
gamma = Constant(gamma)


def get_stress(u_):
    # Define strain measures
    I = Identity(2) # the identity matrix, 2D
    F = I + grad(u_) # the deformation gradient
    F = variable(F)
    C = variable(F.T*F)       # the right Cauchy-Green tensor
    E = variable( 0.5*(C - I) ) # the Green-Lagrange strain tensor

    # St. Venant–Kirchhoff
    W = (lmbda / 2) * variable(tr(E) ** 2) + mu * variable(tr(E * E ) ) # E is symmetric
    P = diff(W, F)
    return P

# Update accelleration
def update_a(u_new, u_old, v_old, a_old):
    dt_=float(dt)
    beta_=float(beta)
    return ((u_new - u_old - dt_*v_old)/beta/dt_**2 - (1-2*beta_)/2/beta_*a_old)

def update_v(a_new, u_old, v_old, a_old):
    dt_=float(dt)
    gamma_=float(gamma)
    return v_old + dt_*((1-gamma_)*a_old + gamma_* a_new)

def update_fields(u_new, u_old, v_old, a_old):
    '''Update all fields at the end of a timestep.'''
    u_new_vec, u0_vec = u_new.vector(), u_old.vector()
    v0_vec, a0_vec = v_old.vector(), a_old.vector()
    
    #call update functions
    a_vec = update_a(u_new_vec, u0_vec, v0_vec, a0_vec)
    v_vec = update_v(a_vec, u0_vec, v0_vec, a0_vec)
    
    #assign u_new->u_old
    v_old.vector()[:], a_old.vector()[:] = v_vec, a_vec
    u_old.vector()[:] = u_new.vector()

acc1 = 1.0 / (beta * h * h) * u1 - ( 1.0 / (beta * h * h) * u0 + 1.0 / (beta * h) * v0 + (1 - 2 * beta) / (2 * beta) * a0 )
v1 = update_v(acc1, u0, v0, a0)


# Stress tensor
def sigma(r):
    return 2.0*mu*sym(grad(r)) + lmbda*tr(sym(grad(r))) * Identity(len(r))

# Mass form
def m(u, u_):
    return rho*inner(u, u_)*dx

# Elastic stiffness form
def k(u, u_):
    # return inner(sigma(u), sym(grad(u_)))*dx
    return inner(sigma(u), grad(u_) ) * dx

def k_full(u, u_):
    return inner(get_stress(u), sym(grad(u_)))*dx

# Rayleigh damping form
def c(u, u_):
    return eta_m * m(u, u_) + eta_k * k(u, u_)



# Define nonlinear problem for one time-step

# damping_term = eta_m * rho * inner(v1, v) * dx   +   eta_k * inner(get_stress(v1), grad(v) ) * dx
damping_term = c(v1, v)
P = get_stress(u) # u = u1


R = rho*dot(acc1, v)*dx + inner(P, grad(v))*dx - dot(B, v)*dx - inner(G, v)*ds(2) + damping_term
dR = derivative(R, u1, du)







# coordinate
mesh_coordinates = mesh.coordinates()

# boundary indicator
vertex_boundary_indicator = []
num_vertex = len(mesh_coordinates)
for i in range(num_vertex):
    x = mesh_coordinates[i]
    x0_boundary_mark = 0
    x1_boundary_mark = 0
    if near(x[0], 0, tol):
        x0_boundary_mark = 1 # left
    if near(x[0], L, tol):
        x0_boundary_mark = 2 # right
    vertex_boundary_indicator.append([x0_boundary_mark, x1_boundary_mark])


    




# Time-stepping
t = dt
counter = 1
info(parameters, True)

varproblem = NonlinearVariationalProblem(R, u1, bcs, J=dR)
solver_u = NonlinearVariationalSolver(varproblem)

solver_options={'linear_solver' : 'gmres', 
                'preconditioner': 'ilu',
                'relative_tolerance' : 1e-8,
                'absolute_tolerance' : 1e-8,
                'maximum_iterations' : 200}
 

solver_u.parameters['newton_solver']['relative_tolerance'] = solver_options['relative_tolerance']
solver_u.parameters['newton_solver']['absolute_tolerance'] = solver_options['absolute_tolerance']
solver_u.parameters['newton_solver']['maximum_iterations'] = solver_options['maximum_iterations']
solver_u.parameters['newton_solver']['linear_solver'] = solver_options['linear_solver']

save_folder = "StVK_data_{}_{}/".format(elem_size, damping_coefficient)
pathlib.Path(save_folder).mkdir(parents=True, exist_ok=True)

# dumper = XDMFFile(comm, save_folder + "output_new.xdmf")
# dumper.parameters['flush_output'] = True
# dumper.parameters['functions_share_mesh'] = True
# dumper.parameters['rewrite_function_mesh'] = False


print("u0 name: ", u0.name())
print("v0 name: ", v0.name())
print("a0 name: ", a0.name())

# dumper.write(mesh)
# dumper.write_checkpoint(u0, "u", 0.0,  XDMFFile.Encoding.HDF5, True)
# dumper.write_checkpoint(v0, "v", 0.0,  XDMFFile.Encoding.HDF5, True)
# dumper.write_checkpoint(a0, "a", 0.0,  XDMFFile.Encoding.HDF5, True)
# with open(save_folder + "u_{}.npy".format(0), "wb" ) as f:
#     u_0_list = u0.sub(0).compute_vertex_values()
#     u_1_list = u0.sub(1).compute_vertex_values()
#     u_at_vertex = np.stack((u_0_list, u_1_list), axis = -1)
#     np.save(f, u_at_vertex)

## save vertex coordinates
print("comm: ", rank, "mesh_coordinates shape: ", mesh_coordinates.shape) # mesh_coordinates shape:  (697, 2)
gathered_coordinates = comm.gather(mesh_coordinates, root=0)
assert mesh_coordinates.shape[1] == 2
global_indices = comm.gather(mesh.topology().global_indices(0) )
if comm.rank == 0:
    num_vertices = mesh.num_entities_global(0)
    all_coordinates = np.zeros((num_vertices, 2) )
    for coord, indices in zip(gathered_coordinates, global_indices):
        all_coordinates[indices] = coord
    with open(save_folder + "coordinates.npy".format(counter), "wb" ) as f:
        np.save(f, all_coordinates)
    
    del all_coordinates

del mesh_coordinates
del gathered_coordinates



# # for paraview
# displacement_file = File(save_folder + "newmark_results/displacement.pvd")
# displacement_file << u0

energies = []
E_damp = 0


for t in np.arange(0, T+dt, dt):
    if counter % 100 == 0:
        print("counter: ", counter)
    # print(t)
    G.t = t

    solver_u.solve()
    
    # Move to next interval
    update_fields(u1, u0, v0, a0)

    # displacement_file << u0 # for paraview
    # energy
    E_elas = assemble(0.5*k_full(u0, u0))
    E_kin = assemble(0.5*m(v0, v0))
    E_damp += dt * assemble(c(v0, v0))
    E_tot = E_elas + E_kin + E_damp 
    energies.append([E_elas, E_kin, E_damp, E_tot])

    # Save solution to file
    if counter % sample_every_K_timeStep == 0:
        # dumper.write(u0, t)
        # dumper.write(v0, t)
        # dumper.write(a0, t)
        # dumper.write_checkpoint(u0, "u", t,  XDMFFile.Encoding.HDF5, True)
        # dumper.write_checkpoint(v0, "v", t,  XDMFFile.Encoding.HDF5, True)
        # dumper.write_checkpoint(a0, "a", t,  XDMFFile.Encoding.HDF5, True)

        # ## No MPI
        # with open(save_folder + "verify_u_{}.npy".format(counter), "wb" ) as f:
        #     u_0_list = u0.sub(0).compute_vertex_values()
        #     u_1_list = u0.sub(1).compute_vertex_values()
        #     u_at_vertex = np.stack((u_0_list, u_1_list), axis = -1)
        #     np.save(f, u_at_vertex)

        
        ## MPI
        u_x = u0.sub(0).compute_vertex_values()
        u_y = u0.sub(1).compute_vertex_values()
        v_x = v0.sub(0).compute_vertex_values()
        v_y = v0.sub(1).compute_vertex_values()
        a_x = a0.sub(0).compute_vertex_values()
        a_y = a0.sub(1).compute_vertex_values()
        gathered_u_x = comm.gather(u_x, root=0)
        gathered_u_y = comm.gather(u_y, root=0)
        gathered_v_x = comm.gather(v_x, root=0)
        gathered_v_y = comm.gather(v_y, root=0)
        gathered_a_x = comm.gather(a_x, root= 0)
        gathered_a_y = comm.gather(a_y, root= 0)
        global_indices = comm.gather(mesh.topology().global_indices(0))
        if comm.rank == 0:
            num_vertices = mesh.num_entities_global(0)
            all_values_u = np.zeros((num_vertices, 2 ) )
            all_values_v = np.zeros((num_vertices, 2 ) )
            all_values_a = np.zeros((num_vertices, 2))
            for u_value_x, u_value_y, v_value_x, v_value_y, a_value_x, a_value_y, indices in zip(gathered_u_x, gathered_u_y, gathered_v_x, gathered_v_y, gathered_a_x, gathered_a_y, global_indices):
                all_values_u[indices] = np.transpose([u_value_x, u_value_y])  # dedicate each vertex value to corresponding global index
                all_values_v[indices] = np.transpose([v_value_x, v_value_y])
                all_values_a[indices] = np.transpose([a_value_x, a_value_y])
            with open(save_folder + "u_{}.npy".format(counter), "wb" ) as f:
                np.save(f, all_values_u)
            with open(save_folder + "v_{}.npy".format(counter), "wb" ) as f:
                np.save(f, all_values_v)
            with open(save_folder + "a_{}.npy".format(counter), "wb" ) as f:
                np.save(f, all_values_a)

            del all_values_u
            del all_values_v
            del all_values_a

        del u_x
        del u_y
        del v_x
        del v_y
        del a_x
        del a_y
        del gathered_u_x
        del gathered_u_y
        del gathered_v_x
        del gathered_v_y
        del gathered_a_x
        del gathered_a_y
        del global_indices
        
        

    counter += 1



energies = np.stack(energies)

if comm.rank == 0:
    with open(save_folder + 'energies.npy', 'wb') as f:
        np.save(f, energies)
