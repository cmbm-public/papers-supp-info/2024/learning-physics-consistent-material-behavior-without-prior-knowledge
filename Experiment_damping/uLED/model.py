import torch
# import torch.nn as nn
# import torch.nn.functional as F
import math
import numpy as np

torch.set_default_dtype(torch.float64)

def get_activation(activation):
    if activation == "silu":
        return torch.nn.SiLU()
    elif activation == "celu":
        return torch.nn.CELU()
    elif activation == 'elu':
        return torch.nn.ELU()
    elif activation == "softplus":
        return torch.nn.Softplus()
    else:
        raise NotImplementedError('activation [%s] is not found' % activation)


class ConvexLinear(torch.nn.Linear):
    def __init__(self, *kargs, **kwargs):

        super(ConvexLinear, self).__init__(*kargs, **kwargs)

       #self.weight.data.copy_(torch.abs(self.weight.data))

        if not hasattr(self.weight, 'be_positive'):
            self.weight.be_positive = 1.0

    def forward(self, input):
        out = torch.nn.functional.linear(input, self.weight, bias = None)
        return out
    

        
class energy_density_NN(torch.nn.Module):
    # predict the energy density function
    def __init__(self, input_dim, hidden_dim, act):
        super(energy_density_NN, self).__init__()

        assert act == "elu"
        

        self.fc1_normal = torch.nn.Linear(input_dim, hidden_dim, bias = True)
        self.activ_1 = get_activation(act)


        self.fc2_normal = torch.nn.Linear(input_dim, hidden_dim, bias = True)
        self.fc2_convex = ConvexLinear(hidden_dim, hidden_dim, bias = False)
        self.activ_2 = get_activation(act)
        
        
        self.fc3_normal = torch.nn.Linear(input_dim, hidden_dim, bias = True)
        self.fc3_convex = ConvexLinear(hidden_dim, hidden_dim, bias = False)
        self.activ_3 = get_activation(act)

        self.fc4_normal = torch.nn.Linear(input_dim, hidden_dim, bias = True)
        self.fc4_convex = ConvexLinear(hidden_dim, hidden_dim, bias = False)
        self.activ_4 = get_activation(act)

        self.last_normal = torch.nn.Linear(input_dim, 1, bias = True)
        self.last_convex = ConvexLinear(hidden_dim, 1, bias = False)

        self.apply(self.init_weights)


    def forward(self, GreenStrain_numT_numE_dim):
        assert GreenStrain_numT_numE_dim.shape[-1] == 4

        ## input transformation
        # input = GreenStrain_numT_numE_dim
        ## use strain invariants
        if len(GreenStrain_numT_numE_dim.shape) == 3:
            C_numT_numE_dim = 2 * GreenStrain_numT_numE_dim
            C_numT_numE_dim[:, :, 0:1] += 1.0
            C_numT_numE_dim[:, :, 3:4] += 1.0
            I1_T_numE_1 = C_numT_numE_dim[:, :, 0:1] + C_numT_numE_dim[:, :, 3:4]
            J_T_numE_1 = C_numT_numE_dim[:, :, 0:1] * C_numT_numE_dim[:, :, 3:4] - C_numT_numE_dim[:, :, 1:2] * C_numT_numE_dim[:, :, 2:3]
            
            # I1_T_numE_1 = GreenStrain_numT_numE_dim[:, :, 0:1] + GreenStrain_numT_numE_dim[:, :, 3:4]
            # J_T_numE_1 = GreenStrain_numT_numE_dim[:, :, 0:1] * GreenStrain_numT_numE_dim[:, :, 3:4] - GreenStrain_numT_numE_dim[:, :, 1:2] * GreenStrain_numT_numE_dim[:, :, 2:3]
        elif len(GreenStrain_numT_numE_dim.shape) == 2:                
            C_numT_numE_dim = 2 * GreenStrain_numT_numE_dim
            C_numT_numE_dim[:, 0:1] += 1.0
            C_numT_numE_dim[:, 3:4] += 1.0
            I1_T_numE_1 = C_numT_numE_dim[:, 0:1] + C_numT_numE_dim[:, 3:4]
            J_T_numE_1 = C_numT_numE_dim[:, 0:1] * C_numT_numE_dim[:, 3:4] - C_numT_numE_dim[:, 1:2] * C_numT_numE_dim[:, 2:3]

            # I1_T_numE_1 = GreenStrain_numT_numE_dim[:, 0:1] + GreenStrain_numT_numE_dim[:, 3:4]
            # J_T_numE_1 = GreenStrain_numT_numE_dim[:, 0:1] * GreenStrain_numT_numE_dim[:, 3:4] - GreenStrain_numT_numE_dim[:, 1:2] * GreenStrain_numT_numE_dim[:, 2:3]
        else:
            raise RuntimeError("GreenStrain_numT_numE_dim shape error ")
        
        input = torch.cat([I1_T_numE_1, J_T_numE_1], dim = -1)            
            

        x = self.activ_1(self.fc1_normal(input) )

        x = self.activ_2(self.fc2_convex(x) + self.fc2_normal(input) )

        x = self.activ_3(self.fc3_convex(x) + self.fc3_normal(input) )

        x = self.activ_4(self.fc4_convex(x) + self.fc4_normal(input) )
        
        x = self.last_convex(x) + self.last_normal(input) 
        
        output_numT_numE_dim = x
        return output_numT_numE_dim


    def init_weights(self, m):
        # if isinstance(m, torch.nn.Linear):
        classname = m.__class__.__name__
        print(classname)
        if classname == 'Linear':
            print("init Linear !!!!")
            torch.nn.init.xavier_uniform_(m.weight.data)
            m.bias.data.fill_(0.1)
        
        if classname == "ConvexLinear":
            print("init ConvexLinear !!!")
            torch.nn.init.xavier_uniform_(m.weight.data)
            m.weight.data = torch.abs(m.weight.data)
        



# predict stress directly from the strain rate
# for damping term            
class stress_NN(torch.nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim=4):
        super(stress_NN, self).__init__()

        # self.stress_func return the 1-st Piola Kirchhoff stress
        self.stress_func = torch.nn.Sequential(
            torch.nn.Linear(input_dim, hidden_dim), # strain rate
            torch.nn.SiLU(),

            # torch.nn.Linear(hidden_dim, hidden_dim),
            # torch.nn.SiLU(),

            # torch.nn.Linear(hidden_dim, hidden_dim),
            # torch.nn.SiLU(),

            #(Can turn on or off this layer:)
            torch.nn.Linear(hidden_dim, hidden_dim), 
            torch.nn.SiLU(),
            
            torch.nn.Linear(hidden_dim, output_dim) # stress
        )
        self.stress_func.apply(self.init_weights)


    def init_weights(self, m):
        classname = m.__class__.__name__
        print(classname)
        if isinstance(m, torch.nn.Linear):
            print("init Linear !!!!")
            torch.nn.init.xavier_uniform_(m.weight.data)
            m.bias.data.fill_(0.1)
        elif isinstance(m, torch.nn.BatchNorm1d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
        else:
            pass
    

    def forward(self, input_numT_numE_dim):
        output_numT_numE_dim = self.stress_func(input_numT_numE_dim) 
        return output_numT_numE_dim