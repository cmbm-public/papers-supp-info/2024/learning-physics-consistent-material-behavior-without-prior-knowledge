"""
## normalize momentum, in util.py
## normalize strain, in train.py
## force zero stress with zero strain
"""
import os
import numpy as np
from tqdm import tqdm
import pickle as pkl
import torch
from fractions import Fraction
import matplotlib.pyplot as plt
from matplotlib import rc,rcParams
from matplotlib.ticker import FormatStrFormatter
# plt.rcParams["font.weight"] = "bold"
# plt.rcParams['xtick.major.pad']='1'
# plt.rcParams['ytick.major.pad']='0.5'
# plt.rc('xtick', labelsize=5)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=5)
# plt.rc('legend', fontsize=5)
plt.style.use(['prl_paper.mplstyle'])
# from plot_helper_for_paper import *
# import importlib
# plot_helper = importlib.import_module( "plot_helper.plot_helper_for_paper" )
from plot_helper_for_paper import set_size


torch.set_default_dtype(torch.float64)


from utils import cmpt_green_strain_tensor
from model import energy_density_NN, stress_NN

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--num_nodes_per_element', type=int, default=3, help='Number of vertex on each element.') 
parser.add_argument('--space_dim', type=int, default=2, help='space dimension') 

parser.add_argument('--batch_size', type=int, default=1, help='Number of samples per batch.')
parser.add_argument('--input_dim', type=int, default=2, help='Two invariants.')
parser.add_argument('--hidden_dim', type=int, default=128, help='Hidden dimension.')
parser.add_argument('--activation', type=str, default='elu', help='[]')

parser.add_argument('--init_lr', type=float, default=5e-4, help='learning rate.')
parser.add_argument('--training_epochs', type=int, default=1000, help='training epochs.')



parser.add_argument('--input_data_dir', type=str, default='../data_process/StVK_GNN_input_0.01_0.0005/', help='Dir for input data')
parser.add_argument('--original_simulation_dir', type=str, default='../generate_data/StVK_data_0.01_0.0005/', help='Dir for fenics simulation data')
parser.add_argument('--system_energy_dir', type=str, default='./system_energy/input_size_0.01/', help='Dir for saving system energies')
parser.add_argument('--trained_model_folder', type=str, default='./trained_StVK_ele0.001_0.0005_float64/interpolate_0.002/exp2/', help='Dir to save trained model')

args = parser.parse_args()



green_strain_normalization_flag = True
cuda_flag = False
# cuda_flag = True
if cuda_flag:
    torch.cuda.set_device(3)


Youngs_modulus = 1e4
nu= 0.3 # Poisson rate
mu  = Youngs_modulus / (2.0*(1.0 + nu))
lmbda = Youngs_modulus * nu / ((1.0 + nu)*(1.0 - 2.0*nu))
bulk_K = Youngs_modulus / (3.0 * (1.0 - 2 * nu) ) #bulk modulus
dt = 0.002



def load_data(input_data_dir):
    ## input (deformation gradient)
    with open(input_data_dir + "deformation_gradient.pkl", "rb") as f:
        F_T_numE_4 = pkl.load(f)   

    ## input (strain rate)
    with open(input_data_dir + "strain_rate.pkl", "rb") as f:
        strain_rate_T_numE_4 = pkl.load(f)
    
    ## element weights
    with open(input_data_dir + "qpWeights.pkl", "rb") as f:
        qpWeights_numE = pkl.load(f)
    print("numE: ", len(qpWeights_numE))

    # node-element-adjacency
    with open(input_data_dir + "connectivity.pkl", "rb") as f:            
        connectivity = pkl.load(f)
    with open(input_data_dir + "gradNa.pkl", "rb") as f:
        gradNa = pkl.load(f)
   
    # internal node ids
    with open(input_data_dir + "vertex_boundary_mask.pkl", "rb") as f:
        vertex_boundary_mask = pkl.load(f) # list


    return F_T_numE_4, strain_rate_T_numE_4, qpWeights_numE, connectivity, gradNa, vertex_boundary_mask




def cmpt_true_elastic_energy():
    # the ground-truth energy density
    # St. Venant–Kirchhoff
    batch_size = 10
    total_time_steps = len(F_T_numE_4)
    energy_T = []
    for counter in range(0, total_time_steps, batch_size):
        batch_F_T_numE_4 = F_T_numE_4[counter: counter + batch_size]
        E_T_numE_4  = cmpt_green_strain_tensor(batch_F_T_numE_4)
        I1 = E_T_numE_4[:, :, 0:1] + E_T_numE_4[:, :, 3:4]
        I2 = E_T_numE_4[:, :, 0:1] * E_T_numE_4[:, :, 3:4] - E_T_numE_4[:, :, 1:2] * E_T_numE_4[:, :, 2:3]
        W_density_T_numE_1 = (lmbda /2 ) * I1 **2 + mu * (I1 **2 - 2 * I2)
        energy_T_numE_1 = W_density_T_numE_1 * EleWeights_numE[None, :, None]
        assert len(energy_T_numE_1.shape) == 3
        energy_T.append(torch.sum(energy_T_numE_1, dim= (1, 2), keepdim= False) ) 
    energy_T = torch.cat(energy_T)
    assert len(energy_T) == len(F_T_numE_4)
    return energy_T



def cmpt_true_damping_energy():
    # E_damp += dt * assemble(c(v0, v0))
    # c(u, u_): eta_k * k(u, u_)
    # k(u, u_):  inner(sigma(u), grad(u_) ) * dx

    num_nodes_per_element = 3
    space_dim = 2

    eta_k = 5E-4
    dt = 0.002

    

    total_time_steps = len(strain_rate_T_numE_4) 

    import sys
    sys.path.append('../data_process/')
    from generate_GNN_input import return_time_steps

    available_time_steps = return_time_steps(args.original_simulation_dir)
    assert len(available_time_steps) == total_time_steps

    energy_sum = 0.0
    energy_T = []

    

    for loop in range(total_time_steps):
        t = available_time_steps[loop]
        vel_nodes_numNode_dim = np.load(args.original_simulation_dir + "v_{}.npy".format(t) )
        vel_nodes_numNode_dim = torch.tensor(vel_nodes_numNode_dim)
        
        batch_strain_rate_numE_4 = strain_rate_T_numE_4[loop]
        # print("batch_strain_rate_numE_4 shape: ", batch_strain_rate_numE_4.shape) # batch_strain_rate_numE_4 shape:  torch.Size([320, 4])

        num_elements = len(batch_strain_rate_numE_4)

        I = torch.zeros((num_elements, 4), device = batch_strain_rate_numE_4.device)
        I[:, 0] += 1.0
        I[:, 3] += 1.0

        dampingP_numE_4 = 2.0 * mu * batch_strain_rate_numE_4 + lmbda * (batch_strain_rate_numE_4[:, 0:1] + batch_strain_rate_numE_4[ :, 3:4]) * I
        # print("dampingP_numE_4 shape: ", dampingP_numE_4.shape) # dampingP_numE_4 shape:  torch.Size([320, 4])

        damping_energy_numE_2 = torch.zeros((num_elements, 2), dtype = dampingP_numE_4.dtype, device = dampingP_numE_4.device)

        dampingP_numE_2_2 = dampingP_numE_4.reshape(num_elements, 2, 2)
        for a in range(num_nodes_per_element):
            vel_a = vel_nodes_numNode_dim[connectivity[a] ]
            assert vel_a.shape == (num_elements, 2)
            assert gradNa[a].shape == (num_elements, 2)
            for i in range(space_dim):        
                dampingP_numE_2 = dampingP_numE_2_2[ :, i, :] * gradNa[a][:, :] * EleWeights_numE[:, None] # P_T_numE_2, 2 is j
                dampingP_numE = dampingP_numE_2.sum(-1)
                damping_energy_numE_2[:, i].index_add_(0, connectivity[a], dampingP_numE * vel_a[:, i] ) # pay attention dim is i
        
        damping_energy = damping_energy_numE_2.sum().item()
        energy_sum += dt * eta_k * damping_energy
        energy_T.append(energy_sum)
    
    energy_T = torch.tensor(energy_T)
    print(energy_T)
    return energy_T




def cmpt_true_kinetic_energy():
    # E_kin = assemble(0.5*m(v0, v0))
    import sys
    sys.path.append('../data_process/')
    from generate_GNN_input import return_time_steps
    

    rho = 1.0
    num_nodes_per_element = 3

    kinetic_energy_T = []
    

    available_time_steps = return_time_steps(args.original_simulation_dir)


    for t in available_time_steps:
        vel_nodes_numNode_dim = np.load(args.original_simulation_dir + "v_{}.npy".format(t) )
        vel_nodes_numNode_dim = torch.tensor(vel_nodes_numNode_dim)
        
        numNodes = vel_nodes_numNode_dim.shape[0]
        # print("numNodes: ", numNodes)

        node_kinetic_numNode_1 = torch.zeros((numNodes, 1), dtype= torch.float64) 
        for a in range(num_nodes_per_element):
            acc_a = vel_nodes_numNode_dim[connectivity[a] ]
            for b in range(num_nodes_per_element):
                acc_b = vel_nodes_numNode_dim[connectivity[b] ]
                mass = EleWeights_numE[:, None ] # rho is 1.0
                # print("acc_b shape: ", acc_b.shape) # acc_b shape:  torch.Size([200, 2])
                # print("mass shape: ", mass.shape) # mass shape:  torch.Size([200, 1])
                # exit()
                if b == a:
                    this_m_vSquare = 1. / 6. * mass * (acc_a * acc_b).sum(-1, keepdim= True)
                else:
                    this_m_vSquare = 1. / 12. * mass * (acc_a * acc_b).sum(-1, keepdim= True)
                assert len(this_m_vSquare.shape) == 2
                assert this_m_vSquare.shape[1] == 1
                assert this_m_vSquare.shape[0] == F_T_numE_4.shape[1]
                node_kinetic_numNode_1.index_add_(0, connectivity[a], this_m_vSquare)
        node_kinetic_numNode = node_kinetic_numNode_1.squeeze()
        node_kinetic_numNode = 0.5 * node_kinetic_numNode
        kinetic_energy_T.append(node_kinetic_numNode.sum(0, keepdim= True) )
    kinetic_energy_T = torch.cat(kinetic_energy_T)
    return kinetic_energy_T



def cmpt_predict_elastic_energy():
    energy_model.eval()

    batch_size = 10
    total_time_steps = len(F_T_numE_4)
    energy_T = []

    zero_strain_1_1_4 = torch.zeros((1, 1, 4) )
    with torch.no_grad():
        zeroEnergy_1_1_1 = energy_model(zero_strain_1_1_4)

    for counter in range(0, total_time_steps, batch_size):
        batch_F_T_numE_4 = F_T_numE_4[counter: counter + batch_size]        
             
        E_T_numE_4  = cmpt_green_strain_tensor(batch_F_T_numE_4) # green strain tensor

        # normalize INPUT Green strain
        normalizedE_T_numE_4 = E_T_numE_4 / greenStrain_max

        ## learn energy density
        with torch.no_grad():
            W_density_T_numE_1 = energy_model(normalizedE_T_numE_4)        
            W_density_T_numE_1 = W_density_T_numE_1 - zeroEnergy_1_1_1
        W_density_T_numE_1 = W_density_T_numE_1 * momentum_max # unnormalize output
        
        energy_T_numE_1 = W_density_T_numE_1 * EleWeights_numE[None, :, None]

        assert len(energy_T_numE_1.shape) == 3
        energy_T.append(torch.sum(energy_T_numE_1, dim= (1, 2), keepdim= False) ) 
                
    energy_T = torch.cat(energy_T)        
    
    return energy_T






def cmpt_predict_damping_energy():

    num_nodes_per_element = 3
    space_dim = 2

    dt = 0.002

    

    total_time_steps = len(strain_rate_T_numE_4) 

    import sys
    sys.path.append('../data_process/')
    from generate_GNN_input import return_time_steps

    available_time_steps = return_time_steps(args.original_simulation_dir)
    assert len(available_time_steps) == total_time_steps

    energy_sum = 0.0
    energy_T = []

    damping_zero_rate_4 = torch.zeros(4, dtype= strain_rate_T_numE_4.dtype, device= strain_rate_T_numE_4.device)
    damping_zero_rate_4 = damping_zero_rate_4 / strain_rate_max
    with torch.no_grad():
        dampingP_zero_rate_4 = stress_model(damping_zero_rate_4)


    for loop in range(total_time_steps):
        t = available_time_steps[loop]
        vel_nodes_numNode_dim = np.load(args.original_simulation_dir + "v_{}.npy".format(t) )
        vel_nodes_numNode_dim = torch.tensor(vel_nodes_numNode_dim)
        
        batch_strain_rate_numE_4 = strain_rate_T_numE_4[loop]
        # print("batch_strain_rate_numE_4 shape: ", batch_strain_rate_numE_4.shape) # batch_strain_rate_numE_4 shape:  torch.Size([320, 4])

        num_elements = len(batch_strain_rate_numE_4)

        I = torch.zeros((num_elements, 4), device = batch_strain_rate_numE_4.device)
        I[:, 0] += 1.0
        I[:, 3] += 1.0

        # dampingP_numE_4 = 2.0 * mu * batch_strain_rate_numE_4 + lmbda * (batch_strain_rate_numE_4[:, 0:1] + batch_strain_rate_numE_4[ :, 3:4]) * I

        batch_strain_rate_numE_4 = batch_strain_rate_numE_4 / strain_rate_max # normalize input
        with torch.no_grad():
            dampingP_numE_4 = stress_model(batch_strain_rate_numE_4) # learn the damping term
            dampingP_numE_4 = dampingP_numE_4 - dampingP_zero_rate_4[None, :]
        dampingP_numE_4 = dampingP_numE_4 * momentum_max

        damping_energy_numE_2 = torch.zeros((num_elements, 2), dtype = dampingP_numE_4.dtype, device = dampingP_numE_4.device)

        dampingP_numE_2_2 = dampingP_numE_4.reshape(num_elements, 2, 2)
        for a in range(num_nodes_per_element):
            vel_a = vel_nodes_numNode_dim[connectivity[a] ]
            assert vel_a.shape == (num_elements, 2)
            assert gradNa[a].shape == (num_elements, 2)
            for i in range(space_dim):        
                dampingP_numE_2 = dampingP_numE_2_2[ :, i, :] * gradNa[a][:, :] * EleWeights_numE[:, None] # P_T_numE_2, 2 is j
                dampingP_numE = dampingP_numE_2.sum(-1)
                damping_energy_numE_2[:, i].index_add_(0, connectivity[a], dampingP_numE * vel_a[:, i] ) # pay attention dim is i
        
        damping_energy = damping_energy_numE_2.sum().item()
        energy_sum += dt * damping_energy
        energy_T.append(energy_sum)
    
    energy_T = torch.tensor(energy_T)
    print(energy_T)
    return energy_T






def plot_system_energy_seperate(true_energies, mode):
    time_steps = [i + 1 for i in range(len(true_energies) ) ]
    plt.figure(figsize=(15, 5))
    plt.plot(time_steps, true_energies)
    plt.legend(("elastic", "kinetic", "damping", "total"))
    # plt.legend(("elastic", "kinetic", "damping" ) )
    plt.xlabel("Time", fontweight="bold", fontsize=8, labelpad=1)
    plt.ylabel("Energies", fontweight="bold", fontsize=8, labelpad= 1)
    plt.savefig("figs/StVK_energies_{}.pdf".format(mode), bbox_inches='tight', pad_inches = 0.03)




def plot_system_energy_together_backup():
    time_steps = [i + 1 for i in range(len(true_energies) ) ]
    # plt.figure(figsize=(15, 5))
    plt.figure(figsize=(5.4, 1.8))

    color_pairs = [
    ('blue', 'tab:blue'),  # First column: elastic
    ('tab:olive', ), # Second column: kinetic
    ('lime', 'tab:green'),  # Third column: damping
    ('orange', 'tab:brown')  # Forth column: total
    ]
    # elastic
    plt.plot(time_steps, predict_energies[:, 0], color= color_pairs[0][1], linewidth = 0.75,  label='Predict elastic')
    plt.plot(time_steps, true_energies[:, 0], color= color_pairs[0][0], linewidth = 0.75, linestyle='dotted',  label='True elastic')
    
    # damping
    plt.plot(time_steps, predict_energies[:, 2], color= color_pairs[2][1], linewidth = 0.75,  label='Predict damping')
    plt.plot(time_steps, true_energies[:, 2], color= color_pairs[2][0], linewidth = 0.75, linestyle='dotted', label='True damping')

    # total
    plt.plot(time_steps, predict_energies[:, 3], color= color_pairs[3][1], linewidth = 0.75,  label='Predict total')
    plt.plot(time_steps, true_energies[:, 3], color= color_pairs[3][0], linewidth = 0.75, linestyle='dotted', label='True total')

    # kinetic. No preidict.
    plt.plot(time_steps, true_energies[:, 1], color= color_pairs[1][0], linewidth = 0.75, label='Kinetic')

    
    leg = plt.legend(loc='upper left', ncol=4, handlelength=1, borderaxespad=0., bbox_to_anchor=(0.28, 0.98), frameon= True)
    leg.get_frame().set_edgecolor('black')
    leg.get_frame().set_linewidth(0.2)

    plt.xlabel("Time", fontweight="bold", fontsize=7, labelpad=1)
    plt.ylabel("System energies", fontweight="bold", fontsize=7, labelpad= 1)
    plt.savefig("figs/StVK_system_energies.pdf", bbox_inches='tight', pad_inches = 0.03)



def plot_system_energy_together():
    time_steps = [i + 1 for i in range(len(true_energies) ) ]
    fig = plt.figure(figsize= set_size( width='two-column', height_ratio=0.38), constrained_layout=True)

    num_time_steps = 1000
    # color_pairs = [
    # ('#a6cee3', '#1f78b4'),  # First column: elastic
    # ('tab:olive', ), # Second column: kinetic
    # ('#b2df8a', '#33a02c'),  # Third column: damping
    # ('#fb9a99', '#e31a1c')  # Forth column: total
    # ]
    color_pairs = [
    ('#377eb8', '#8dd3c7'),  # First column: elastic
    ('tab:olive', ), # Second column: kinetic
    ('#4daf4a', '#a6d854'),  # Third column: damping
    ('#ff7f00', '#feb24c')  # Forth column: total
    ]
    # color_pairs = [
    # ('#1f78b4', '#a6cee3'),  # First column: elastic
    # ('tab:olive', ), # Second column: kinetic
    # ('#33a02c', '#b2df8a'),  # Third column: damping
    # ('#ff6e54', '#ffa600')  # Forth column: total
    # ]

    marker_every = 7

    # elastic
    plt.plot(time_steps[:num_time_steps], predict_energies[:num_time_steps, 0], color= color_pairs[0][1], linewidth = 2,  label='Predict elastic')
    # plt.plot(time_steps[:num_time_steps], true_energies[:num_time_steps, 0], color= color_pairs[0][0],  linestyle='dotted',  label='True elastic')
    plt.plot(time_steps[:num_time_steps:marker_every], predict_energies[:num_time_steps:marker_every, 0], markeredgecolor = color_pairs[0][0], markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4, label = 'True elastic')            
    
    # damping
    plt.plot(time_steps[:num_time_steps], predict_energies[:num_time_steps, 2], color= color_pairs[2][1],  linewidth = 2, label='Predict damping')
    # plt.plot(time_steps[:num_time_steps], true_energies[:num_time_steps, 2], color= color_pairs[2][0],  linestyle='dotted', label='True damping')
    plt.plot(time_steps[:num_time_steps:20], true_energies[:num_time_steps:20, 2], markeredgecolor = color_pairs[2][0], markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4, label = 'True damping')            

    # total
    print("true_energies")
    print(true_energies[:, 3])
    cutpos = -1
    for i in range(len(true_energies[:, 3])):
        if round(true_energies[i, 3], 4) == 1.1637:
            print(i, true_energies[i, 3])
            cutpos = i
            break

    plt.plot(time_steps[:num_time_steps], predict_energies[:num_time_steps, 3], color= color_pairs[3][1], linewidth = 2, label='Predict total')
    # plt.plot(time_steps[:num_time_steps], true_energies[:num_time_steps, 3], color= color_pairs[3][0],  linestyle='dotted', label='True total')
    plt.plot(time_steps[:cutpos:marker_every], true_energies[:cutpos:marker_every, 3], markeredgecolor = color_pairs[3][0], markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4, label = 'True total')            
    plt.plot(time_steps[cutpos:num_time_steps:20], true_energies[cutpos:num_time_steps:20, 3], markeredgecolor = color_pairs[3][0], markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4)            

    

    # kinetic. No preidict.
    # plt.plot(time_steps, true_energies[:, 1], color= color_pairs[1][0], label='Kinetic')

    leg = plt.legend(loc='upper left', ncol=3, handlelength=1.5, borderaxespad=0., bbox_to_anchor=(0.555, 0.97), columnspacing=1.5, labelspacing=0.2, handletextpad=0.2, numpoints=1, frameon= True)
    for legobj in leg.legend_handles:
            legobj.set_markersize(5)
    # leg.get_frame().set_edgecolor('black')
    # leg.get_frame().set_linewidth(0.2)
    # # (bbox_to_anchor=(0.01, 0.99), loc="upper left", borderaxespad=0.1, ncol=3, fontsize=5, columnspacing=0.6, labelspacing=0.2, markerscale = 0.7, borderpad= 0.3)
    # for legobj in leg.legend_handles:
    #     legobj.set_linewidth(1.5)
    

    # plt.xlabel("Time", fontweight="bold", fontsize=7, labelpad=1)
    # plt.ylabel("System energies", fontweight="bold", fontsize=7, labelpad= 1)
    plt.xlim(-20, 1020)
    plt.xlabel("time steps")
    plt.ylabel("energy")
    plt.savefig("StVK_system_energies.pdf", bbox_inches='tight', pad_inches = 0.03)



### main ###
if __name__ == "__main__":
    
    trained_model_folder = args.trained_model_folder
    
    os.makedirs(args.system_energy_dir, exist_ok=True)
    
    
    #-------------------------------------
    #------------- load data -------------
    #-------------------------------------
    F_T_numE_4, strain_rate_T_numE_4, EleWeights_numE, connectivity, gradNa, vertex_boundary_mask = load_data(args.input_data_dir)
    vertex_internal_mask = ~vertex_boundary_mask

    
    #----------------------------------------------
    #------------- true system energy -------------
    #----------------------------------------------
    true_energy_file = args.system_energy_dir + "true_energies.npy"
    
    if not os.path.isfile(true_energy_file):
        print(true_energy_file, " not found, generating true_energies")
        true_elastic_energy_T = cmpt_true_elastic_energy()
        true_kinetic_energy_T = cmpt_true_kinetic_energy()
        true_damping_energy_T = cmpt_true_damping_energy()
        true_total_energy_T = true_elastic_energy_T + true_kinetic_energy_T + true_damping_energy_T

        true_energies = torch.stack([true_elastic_energy_T, true_kinetic_energy_T, true_damping_energy_T, true_total_energy_T], dim= -1)
        true_energies = true_energies.cpu().detach().numpy()
        with open(true_energy_file, 'wb') as f:
            np.save(f, true_energies )
    else:
        with open(true_energy_file, "rb") as f:
            true_energies = np.load(f)

    # plot_system_energy_seperate(true_energies, "true")



    #----------------------------------------------
    #---------- predict system energy -------------
    #----------------------------------------------
    predict_energy_file = args.system_energy_dir + "predict_energies.npy"
    if not os.path.isfile(predict_energy_file):
        print(predict_energy_file, " not found, generating predict_energies")
        if cuda_flag:
            strain_rate_T_numE_4 = strain_rate_T_numE_4.cuda()
            F_T_numE_4 = F_T_numE_4.cuda()
            EleWeights_numE = EleWeights_numE.cuda()
            for a in range(args.num_nodes_per_element):
                connectivity[a] = connectivity[a].cuda()
                gradNa[a] = gradNa[a].cuda()

        # for normalizing INPUT:  strain
        assert os.path.isfile(trained_model_folder + "greenStrain_max.pkl")
        with open(trained_model_folder + "greenStrain_max.pkl", "rb") as f:
            greenStrain_max = pkl.load(f)
        print("greenStrain_max: ", greenStrain_max)
        
        # for normalizing INPUT:  strain rate
        assert os.path.isfile(trained_model_folder + "strain_rate_max.pkl")
        with open(trained_model_folder + "strain_rate_max.pkl", "rb") as f:
            strain_rate_max = pkl.load(f)
            print("strain_rate_max: ", strain_rate_max)
        
        # for normalize OUTPUT
        assert os.path.isfile(trained_model_folder + "momentum_max.pkl")
        with open(trained_model_folder + "momentum_max.pkl", "rb") as f:
            res = pkl.load(f) 
            momentum_max = res["momentum_max"]
            print("momentum_max: ", momentum_max)
        
        
        """
        ------------- Load the trained the NN model -------------
        """
        energy_model = energy_density_NN(args.input_dim, args.hidden_dim, args.activation)    
        if cuda_flag:
            energy_model.cuda()
        best_trained_energy_model_file = os.path.join(trained_model_folder, 'best_trained_energy_model.pt')
        energy_model.load_state_dict(torch.load(best_trained_energy_model_file))
        


        # for damping
        stress_model = stress_NN(4, args.hidden_dim, 4)
        if cuda_flag:
            stress_model.cuda()
        best_trained_stress_model_file = os.path.join(trained_model_folder, 'best_trained_stress_model.pt')
        stress_model.load_state_dict(torch.load(best_trained_stress_model_file) )
        

        predict_elastic_energy_T = cmpt_predict_elastic_energy()
        predict_kinetic_energy_T = cmpt_true_kinetic_energy()
        predict_damping_energy_T = cmpt_predict_damping_energy()
        predict_total_energy_T = predict_elastic_energy_T + predict_kinetic_energy_T + predict_damping_energy_T

        predict_energies = torch.stack([predict_elastic_energy_T, predict_kinetic_energy_T, predict_damping_energy_T, predict_total_energy_T], dim= -1)
        predict_energies = predict_energies.cpu().detach().numpy()

        with open(predict_energy_file, 'wb') as f:
            np.save(f, predict_energies )
    
    else:
        with open(predict_energy_file, "rb") as f:
            predict_energies = np.load(f)


    #----------------------------------------------
    #------------- plot system energy -------------
    #----------------------------------------------
    # plot_system_energy_seperate(predict_energies, "predict")
    
    plot_system_energy_together()
