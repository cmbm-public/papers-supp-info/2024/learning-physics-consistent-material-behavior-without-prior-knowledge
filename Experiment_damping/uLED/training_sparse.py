"""
## normalize momentum, in util.py
## normalize strain, in train.py
## force zero stress with zero strain
"""
import os
import numpy as np
from tqdm import tqdm
import pickle as pkl
import torch
from fractions import Fraction

import shutil

torch.set_default_dtype(torch.float64)


from utils import load_data, cmpt_green_strain_tensor
from model import energy_density_NN, stress_NN

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--num_nodes_per_element', type=int, default=3, help='Number of vertex on each element.') 
parser.add_argument('--space_dim', type=int, default=2, help='space dimension') 

parser.add_argument('--batch_size', type=int, default=1, help='Number of samples per batch.')
parser.add_argument('--input_dim', type=int, default=2, help='Two invariants.')
parser.add_argument('--hidden_dim', type=int, default=64, help='Hidden dimension.')
parser.add_argument('--activation', type=str, default='elu', help='[]')

parser.add_argument('--init_lr', type=float, default=5e-4, help='learning rate.')
parser.add_argument('--training_epochs', type=int, default=1000, help='training epochs.')

parser.add_argument('--input_data_dir', type=str, default='../data_process/GNN_input_0.0005/', help='Dir for input data')
parser.add_argument('--save_folder', type=str, default='./trained_ele_0.0005_float64/interpolate_0/', help='Dir to save result')

args = parser.parse_args()



green_strain_normalization_flag = True
# cuda_flag = False
cuda_flag = True
# if cuda_flag:
#     torch.cuda.set_device(args.gpu_id)


def projection():
    # make the weights in ConvexLinear() positive
    for p in positive_params:
        projected_weight = torch.relu(p.data)
        p.data.copy_(projected_weight)

# def cmpt_projection_loss():
#     count_loss = 0.0
#     for p in positive_params:
#         projected_weight = torch.relu(p.data)
#         count_loss += torch.sum(projected_weight - p.data).item()
#     return count_loss

# def count_zero_number():
#     num_zeros = 0.
#     num_total = 0.
#     for p in positive_params:
#         num_total += p.numel()
#         num_zeros += p.numel() - p.nonzero().size(0)
#     return num_zeros / num_total






def cmpt_loss(data_loader):
    # https://stackoverflow.com/questions/60018578/what-does-model-eval-do-in-pytorch
    energy_model.eval()
    stress_model.eval()

    total_momentum_MAE = 0.0
    count_node = 0.0

    # for zero stress with zero strain
    # Green strain
    zero_strain_1_1_4 = torch.zeros((1, 1, 4) )
    if cuda_flag:
        zero_strain_1_1_4 = zero_strain_1_1_4.cuda()
    zero_strain_1_1_4.requires_grad = True
    # deformation gradient
    unit_F_1_1_2_2 = torch.zeros((1, 1, 2, 2)) # 2 * 2 tensor for (batch_size, numE)
    unit_F_1_1_2_2[0, 0, 0, 0] += 1.0
    unit_F_1_1_2_2[0, 0, 1, 1] += 1.0
    if cuda_flag:
        unit_F_1_1_2_2 = unit_F_1_1_2_2.cuda()
    

    # strain rate zero, for computing damping stress
    zero_strain_rate_1_1_4 = torch.zeros((1, 1, 4))
    if cuda_flag:
        zero_strain_rate_1_1_4 = zero_strain_rate_1_1_4.cuda()



    for batch_idx, (node_momentum_T_numNode_2, F_T_numE_4, strain_rate_T_numE_4) in enumerate(data_loader):
        this_batch_size = len(F_T_numE_4)
        num_total_node = node_momentum_T_numNode_2.shape[1]
        num_elements = F_T_numE_4.shape[1]
        if cuda_flag:
            node_momentum_T_numNode_2 = node_momentum_T_numNode_2.cuda()
            F_T_numE_4 = F_T_numE_4.cuda()
            strain_rate_T_numE_4 = strain_rate_T_numE_4.cuda()
             
        E_T_numE_4  = cmpt_green_strain_tensor(F_T_numE_4) # green strain tensor
        E_T_numE_4.requires_grad = True


        # print("normalizedE_T_numE_4 shape: ", normalizedE_T_numE_4.shape)

        ## learn the 2-nd Piola Kirchhoff stress S
        energy_T_numE_1 = energy_model(E_T_numE_4)
        S_T_numE_4 = torch.autograd.grad(energy_T_numE_1.sum(), E_T_numE_4, create_graph=True)[0]
        
        
        ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
        zeroEnergy_1_1_1 = energy_model(zero_strain_1_1_4)
        zeroS_1_1_4  = torch.autograd.grad(zeroEnergy_1_1_1.sum(), zero_strain_1_1_4, create_graph = True)[0]
        zeroS_1_1_2_2 = zeroS_1_1_4.reshape(1, 1, 2, 2)
        # zeroP_1_1_2_2 = torch.matmul(unit_F_1_1_2_2, zeroS_1_1_2_2)
        zeroP_1_1_2_2 = zeroS_1_1_2_2
        
        ## correct the predicted stress
        S_T_numE_2_2 = S_T_numE_4.reshape(S_T_numE_4.shape[0], S_T_numE_4.shape[1], 2, 2) # last two dimensions are i and j. sum over j
        F_T_numE_2_2 = F_T_numE_4.reshape(F_T_numE_4.shape[0], F_T_numE_4.shape[1], 2, 2)
        P_T_numE_2_2 = torch.matmul(F_T_numE_2_2, S_T_numE_2_2)
        P_T_numE_2_2 = P_T_numE_2_2 - torch.matmul(F_T_numE_2_2, zeroP_1_1_2_2) # change the regularizer!!!
        # torch.set_printoptions(precision=10)
        # print(P_T_numE_2_2[0,0])
        # print(F_T_numE_2_2[0,0])
        # print(S_T_numE_2_2[0,0])
        # exit()


        # do not need grad here
        nodeAggregatedP_T_numN_2 = torch.zeros((this_batch_size, num_total_node, 2) )
        if cuda_flag:
            nodeAggregatedP_T_numN_2 = nodeAggregatedP_T_numN_2.cuda()


        for a in range(args.num_nodes_per_element):
            assert gradNa[a].shape == (num_elements, 2)
            for i in range(args.space_dim):        
                P_T_numE_2 = P_T_numE_2_2[:, :, i, :] * gradNa[a][None, :, :] * EleWeights_numE[None, :, None] # P_T_numE_2, 2 is j
                P_T_numE = P_T_numE_2.sum(-1) # sum over j
                nodeAggregatedP_T_numN_2[:, :, i].index_add_(1, connectivity[a], P_T_numE) # pay attention dim is 1

        

        
        ## damping term, do not need grad here
        # strain_rate_T_numE_4 = strain_rate_T_numE_4 / strain_rate_max # normalize input
        with torch.no_grad():
            dampingP_T_numE_4 = stress_model(strain_rate_T_numE_4) # learn the damping term
            dampingP_zero_rate_1_1_4 = stress_model(zero_strain_rate_1_1_4) #the damping_P at zero strain rate
            dampingP_T_numE_4 = dampingP_T_numE_4 - dampingP_zero_rate_1_1_4

        damping_stiffness_T_numN_2 = torch.zeros((this_batch_size, num_total_node, 2) ) 
        if cuda_flag:
            damping_stiffness_T_numN_2 = damping_stiffness_T_numN_2.cuda()
        dampingP_T_numE_2_2 = dampingP_T_numE_4.reshape(dampingP_T_numE_4.shape[0], dampingP_T_numE_4.shape[1], 2, 2)
        for a in range(args.num_nodes_per_element):
            for i in range(args.space_dim):        
                dampingP_T_numE_2 = dampingP_T_numE_2_2[:, :, i, :] * gradNa[a][None, :, :] * EleWeights_numE[None, :, None] # P_T_numE_2, 2 is j
                dampingP_T_numE = dampingP_T_numE_2.sum(-1)
                damping_stiffness_T_numN_2[:, :, i].index_add_(1, connectivity[a], dampingP_T_numE) # pay attention dim is 1




        ## choose the internal vertex
        node_momentum_T_numNode_2 = node_momentum_T_numNode_2[:, vertex_internal_mask, :]
        nodeAggregatedP_T_numN_2 = nodeAggregatedP_T_numN_2[:, vertex_internal_mask, :]
        damping_stiffness_T_numN_2 = damping_stiffness_T_numN_2[:, vertex_internal_mask, :]
        # print("node_momentum_T_numNode_2 shape: ", node_momentum_T_numNode_2.shape)
        # print("nodeAggregatedP_T_numN_2 shape: ", nodeAggregatedP_T_numN_2.shape)

        MAE_momentum = torch.sum(torch.abs(node_momentum_T_numNode_2 + nodeAggregatedP_T_numN_2 + damping_stiffness_T_numN_2 ) )

        total_momentum_MAE += MAE_momentum.item()
        count_node += node_momentum_T_numNode_2.shape[0] * node_momentum_T_numNode_2.shape[1]
    
    
    return total_momentum_MAE / count_node





def train(epoch):
    
    energy_model.train()
    stress_model.train()

    total_momentum_MAE = 0.0
    count_node = 0.0

    ## for the zero stress with zero_strain
    # Green strain
    zero_strain_1_1_4 = torch.zeros((1, 1, 4) )
    if cuda_flag:
        zero_strain_1_1_4 = zero_strain_1_1_4.cuda()
    zero_strain_1_1_4.requires_grad = True
    # deformation gradient
    unit_F_1_1_2_2 = torch.zeros((1, 1, 2, 2)) # 2 * 2 tensor for (batch_size, numE)
    unit_F_1_1_2_2[0, 0, 0, 0] += 1.0
    unit_F_1_1_2_2[0, 0, 1, 1] += 1.0
    if cuda_flag:
        unit_F_1_1_2_2 = unit_F_1_1_2_2.cuda()
    
    # strain rate zero, for computing damping stress
    zero_strain_rate_1_1_4 = torch.zeros((1, 1, 4))
    if cuda_flag:
        zero_strain_rate_1_1_4 = zero_strain_rate_1_1_4.cuda()
    
        
    energy_model.train()
    for batch_idx, (node_momentum_T_numNode_2, F_T_numE_4, strain_rate_T_numE_4) in enumerate(train_loader):
        this_batch_size = len(F_T_numE_4)
        num_total_node = node_momentum_T_numNode_2.shape[1]
        num_elements = F_T_numE_4.shape[1]

        opt.zero_grad() # zero gradients

        if cuda_flag:
            node_momentum_T_numNode_2 = node_momentum_T_numNode_2.cuda()
            F_T_numE_4 = F_T_numE_4.cuda()
            strain_rate_T_numE_4 = strain_rate_T_numE_4.cuda()
            
        
        E_T_numE_4  = cmpt_green_strain_tensor(F_T_numE_4) # green strain tensor
        E_T_numE_4.requires_grad = True # for computing the energy, stress


        ## learn the 2-nd Piola Kirchhoff stress S
        energy_T_numE_1 = energy_model(E_T_numE_4)
        S_T_numE_4 = torch.autograd.grad(energy_T_numE_1.sum(), E_T_numE_4, create_graph=True)[0]

        ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
        zeroEnergy_1_1_1 = energy_model(zero_strain_1_1_4)
        zeroS_1_1_4  = torch.autograd.grad(zeroEnergy_1_1_1.sum(), zero_strain_1_1_4, create_graph = True)[0]
        zeroS_1_1_2_2 = zeroS_1_1_4.reshape(1, 1, 2, 2)
        # zeroP_1_1_2_2 = torch.matmul(unit_F_1_1_2_2, zeroS_1_1_2_2)
        zeroP_1_1_2_2 = zeroS_1_1_2_2 # because of the unit_F_1_1_2_2
                    
        ## correct the predicted stress
        S_T_numE_2_2 = S_T_numE_4.reshape(S_T_numE_4.shape[0], S_T_numE_4.shape[1], 2, 2) # last two dimensions are i and j. sum over j
        F_T_numE_2_2 = F_T_numE_4.reshape(F_T_numE_4.shape[0], F_T_numE_4.shape[1], 2, 2)
        P_T_numE_2_2 = torch.matmul(F_T_numE_2_2, S_T_numE_2_2)
        P_T_numE_2_2 = P_T_numE_2_2 - torch.matmul(F_T_numE_2_2, zeroP_1_1_2_2) # Change the regularizer!!!
        # torch.set_printoptions(precision=10)
        # print(P_T_numE_2_2[0,0])
        # print(F_T_numE_2_2[0,0])
        # print(S_T_numE_2_2[0,0])
        # exit()

        ## stress term, need grad
        nodeAggregatedP_T_numN_2 = torch.autograd.Variable(torch.zeros((this_batch_size, num_total_node, 2) ) )
        if cuda_flag:
            nodeAggregatedP_T_numN_2 = nodeAggregatedP_T_numN_2.cuda()

        for a in range(args.num_nodes_per_element):
            assert gradNa[a].shape == (num_elements, 2)
            for i in range(args.space_dim):        
                P_T_numE_2 = P_T_numE_2_2[:, :, i, :] * gradNa[a][None, :, :] * EleWeights_numE[None, :, None] # P_T_numE_2, 2 is j
                P_T_numE = P_T_numE_2.sum(-1) # sum over j
                nodeAggregatedP_T_numN_2[:, :, i].index_add_(1, connectivity[a], P_T_numE) # pay attention dim is 1

        # print("nodeAggregatedP_T_numN_2 shape: ", nodeAggregatedP_T_numN_2.shape)
                

        ## damping term, need grad
        dampingP_T_numE_4 = stress_model(strain_rate_T_numE_4)
        dampingP_zero_rate_1_1_4 = stress_model(zero_strain_rate_1_1_4) #the damping_P at zero strain rate
        dampingP_T_numE_4 = dampingP_T_numE_4 - dampingP_zero_rate_1_1_4
        

        damping_stiffness_T_numN_2 = torch.autograd.Variable(torch.zeros((this_batch_size, num_total_node, 2) ) )
        if cuda_flag:
            damping_stiffness_T_numN_2 = damping_stiffness_T_numN_2.cuda()
        dampingP_T_numE_2_2 = dampingP_T_numE_4.reshape(dampingP_T_numE_4.shape[0], dampingP_T_numE_4.shape[1], 2, 2)
        for a in range(args.num_nodes_per_element):
            for i in range(args.space_dim):        
                dampingP_T_numE_2 = dampingP_T_numE_2_2[:, :, i, :] * gradNa[a][None, :, :] * EleWeights_numE[None, :, None] # P_T_numE_2, 2 is j
                dampingP_T_numE = dampingP_T_numE_2.sum(-1)
                damping_stiffness_T_numN_2[:, :, i].index_add_(1, connectivity[a], dampingP_T_numE) # pay attention dim is 1


        ## choose the internal vertex
        node_momentum_T_numNode_2 = node_momentum_T_numNode_2[:, vertex_internal_mask, :]
        nodeAggregatedP_T_numN_2 = nodeAggregatedP_T_numN_2[:, vertex_internal_mask, :]
        damping_stiffness_T_numN_2 = damping_stiffness_T_numN_2[:, vertex_internal_mask, :]
        # print("node_momentum_T_numNode_2 shape: ", node_momentum_T_numNode_2.shape)
        # print("nodeAggregatedP_T_numN_2 shape: ", nodeAggregatedP_T_numN_2.shape)

        # minimize the MAE loss
        batch_loss = torch.sum(torch.abs(node_momentum_T_numNode_2 + nodeAggregatedP_T_numN_2 + damping_stiffness_T_numN_2 )  )

        batch_loss.backward()
        opt.step()

        # projection non-negative weights
        projection()

        total_momentum_MAE += torch.sum(torch.abs(node_momentum_T_numNode_2 + nodeAggregatedP_T_numN_2 + damping_stiffness_T_numN_2 ) ).item()
        count_node += node_momentum_T_numNode_2.shape[0] * node_momentum_T_numNode_2.shape[1]
    
    train_momentum_MAE_mean = total_momentum_MAE / count_node
    # ## free gpu memory
    # # https://stackoverflow.com/questions/55788093/how-to-free-gpu-memory-by-deleting-tensors
    



    ###################### test ######################
    ## valid loss
    valid_momentum_MAE_mean = cmpt_loss(valid_loader)
    
    # projection_loss = cmpt_projection_loss()
    # zero_num = count_zero_number()



    print("Epoch: {}, average train_momentum_MAE: {}".format(epoch, train_momentum_MAE_mean))
    print("Epoch: {}, average valid_momentum_MAE: {}".format(epoch, valid_momentum_MAE_mean) )
    # print("Projection loss: {}".format( projection_loss ) )
    # print("Percentage of zeros: ", zero_num)

    # logging to file

    with open(log_file, "a") as f:
        print("Epoch: {:04d}".format(epoch),
              "train_momentum_MAE_mean: {:.8f}".format(train_momentum_MAE_mean),
              "valid_momentum_MAE_mean: {:.8f}".format(valid_momentum_MAE_mean),
            #   "Percentage of zeros: {}".format(zero_num),
            #   "Projection loss: {}".format(projection_loss),
              file = f                      
              )
    
    return valid_momentum_MAE_mean


### main ###
if __name__ == "__main__":
    
    save_folder = args.save_folder
    os.makedirs(save_folder, exist_ok=True)
    experiments_done = len(os.listdir(save_folder))
    save_folder = save_folder + 'exp{}/'.format(experiments_done + 1)

    print("input_data_dir: ", args.input_data_dir)
    print("save_folder: ", save_folder)
    os.makedirs(save_folder, exist_ok=True)
    shutil.copy("./model.py", save_folder + "copiedFile-model.py")
    shutil.copy("./training_sparse.py", save_folder + "copiedFile-training_sparse.py")
    # logging
    log_file = save_folder + "logging.txt"
    with open(log_file, "w") as f:
        print("args: {}".format(args), file = f)
    
    #-------------------------------------
    #------------- load data -------------
    #-------------------------------------
    train_loader, valid_loader, EleWeights_numE, connectivity, gradNa, vertex_boundary_mask = load_data(
        args.input_data_dir, args.batch_size, save_min_max_dir = save_folder)
    vertex_internal_mask = ~vertex_boundary_mask
        
    print("vertex_boundary_mask len: ", len(vertex_boundary_mask), ", vertex_boundary_mask type: ", type(vertex_boundary_mask))
    print("boundary vertex percentage: ", Fraction(vertex_boundary_mask.sum() /  np.size(vertex_boundary_mask) ).limit_denominator())
    print("internal vertex percentage: ", Fraction(vertex_internal_mask.sum() /  np.size(vertex_internal_mask) ).limit_denominator())    
    
    if cuda_flag:
        EleWeights_numE = EleWeights_numE.cuda()
        for a in range(args.num_nodes_per_element):
            connectivity[a] = connectivity[a].cuda()
            gradNa[a] = gradNa[a].cuda()

    # # for normalizing INPUT:  strain
    # assert os.path.isfile(save_folder + "greenStrain_max.pkl")
    # with open(save_folder + "greenStrain_max.pkl", "rb") as f:
    #     greenStrain_max = pkl.load(f)
    # print("greenStrain_max: ", greenStrain_max)
    
    # # for normalizing INPUT:  strain rate
    # with open(save_folder + "strain_rate_max.pkl", "rb") as f:
    #     strain_rate_max = pkl.load(f)
    #     print("strain_rate_max: ", strain_rate_max)
    
    
    """
    ------------- Define the NN model -------------
    """
    energy_model = energy_density_NN(args.input_dim, args.hidden_dim, args.activation)    
    if cuda_flag:
        energy_model.cuda()
    
    # for damping
    stress_model = stress_NN(4, args.hidden_dim, 4)
    if cuda_flag:
        stress_model.cuda()

    # positive parameters
    positive_params = []
    for p in list(energy_model.parameters()):
        if hasattr(p, 'be_positive'):
            positive_params.append(p)
    #         print(p.shape)
    # print(len(positive_params))
    # exit()
    
    """
    ------------- optimizer -------------
    """
    learnable_params = list(energy_model.parameters()) + list(stress_model.parameters())
    opt = torch.optim.Adam(learnable_params, lr = args.init_lr, weight_decay=1e-8)


    """
    ------------- Training -------------
    """
    
    # with open(log_file, "a") as f:
    #     print("config.activation: {}".format(args.activation), file = f)
        # print("(target) config.momentum_normalization_flag: {}".format(config.momentum_normalization_flag), file = f)
        # print("(input) config.green_strain_normalization_flag: {}".format(config.green_strain_normalization_flag), file = f)
        # print("config.zero_strain_stress_flag: {}".format(config.zero_strain_stress_flag), file = f)
        # print("config.use_invariants_flag: ", config.use_invariants_flag, file = f)
        # print("config.training_objective: ", config.training_objective, file = f)
    
    best_val_loss = np.inf
    best_epoch = -1
    best_trained_energy_model_file = os.path.join(save_folder, 'best_trained_energy_model.pt')
    best_trained_stress_model_file = os.path.join(save_folder, 'best_trained_stress_model.pt')
    for epoch in tqdm(range(args.training_epochs)):
        val_loss = train(epoch)
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            best_epoch = epoch
            with open(log_file, "a") as f:
                print("loss drops+++")
                print("loss drops+++", file = f)
            torch.save(energy_model.state_dict(), best_trained_energy_model_file)
            torch.save(stress_model.state_dict(), best_trained_stress_model_file)

