## Learn constitutive relations in a dissipative setting
This folder contains the code for experiments in Sec.4.4.


### Instructions

1. Generate FEM simulation
     
   ```bash
   # in generate_data/
   python fenics_simulation_StVK_damping.py
   python fenics_simulation_mesh.py
   ``` 


2. Process the simulation and generate input for training the neural network, at the coarse data resolution
   
   ```bash
   # in data_process/
   python interpolation_generate_GNN_input.py
   ```

3. Train uLED
   
   ```bash
   #in uLED/
   bash auto_train.bash
   ```



4. Reproduce Fig.11
      ```bash
      # uLED/
      python plot_system_energy.py
    
      # after this, StVK_system_energies.pdf should be generated in learned_field_visualization/
      ```
