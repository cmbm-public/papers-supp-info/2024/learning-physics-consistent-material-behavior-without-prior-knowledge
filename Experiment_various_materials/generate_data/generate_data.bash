
# generate simulation, with default elem_size=0.001.
python fenics_simulation.py --material "NH"
python fenics_simulation.py --material "VK"
python fenics_simulation.py --material "AB"
python fenics_simulation.py --material "Fung"
python fenics_simulation.py --material "Gent"
python fenics_simulation.py --material "MR"


# save the mesh information
python fenics_simulation_mesh.py
