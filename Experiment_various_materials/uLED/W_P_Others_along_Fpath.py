### ["UT", "BT", "SS"] * ["StVK", "MR", "Gent", "AB", "Fung"]


import os
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc,rcParams
from matplotlib.ticker import FormatStrFormatter
# plt.rcParams["font.weight"] = "bold"
# plt.rcParams['xtick.major.pad']='1'
# plt.rcParams['ytick.major.pad']='0.5'
# plt.rc('xtick', labelsize=6)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=6)
# plt.rc('legend', fontsize=8)
plt.style.use(['./figs/prl_paper.mplstyle'])
from figs.plot_helper_for_paper import set_size

# plt.rcParams["axes.titlepad"] = 8
import pickle as pkl

from model import energy_density_NN

import argparse


parser = argparse.ArgumentParser()


parser.add_argument('--num_nodes_per_element', type=int, default=3, help='Number of vertex on each element.') 
parser.add_argument('--space_dim', type=int, default=2, help='space dimension') 

parser.add_argument('--batch_size', type=int, default=1, help='Number of samples per batch.')
parser.add_argument('--input_dim', type=int, default=2, help='Two invariants.')
parser.add_argument('--hidden_dim', type=int, default=128, help='Hidden dimension.')
parser.add_argument('--activation', type=str, default='elu', help='[]')

# parser.add_argument('--material', type= str, default= "-1", help= "[AB, Fung, Gent, MR, NH, VK]")
# parser.add_argument('--F_path', type= str, default= "-1", help= "uniaxial tension (UT), uniaxial compression (UC), biaxial tension (BT), biaxial compression (BC), simple shear (SS) and pure shear (PS).")

parser.add_argument('--gamma_steps', type=int, default=200, help='For plotting')
parser.add_argument('--exterpolation_ratio', type= float, default=0.2, help= "for the exterpolation test")
args = parser.parse_args()


def cmpt_cauchy_green_deformation(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2

    C = torch.cat((C11,C12,C21,C22), dim=-1)
    return C



def cmpt_green_strain_tensor(F):
    
    F11 = F[:, 0:1]
    F12 = F[:, 1:2]
    F21 = F[:, 2:3]
    F22 = F[:, 3:4]

    C11 = F11**2 + F21**2
    C12 = F11*F12 + F21*F22
    C21 = F11*F12 + F21*F22
    C22 = F12**2 + F22**2
    
    E11 = C11 - 1.0
    E12 = C12
    E21 = C21
    E22 = C22 - 1.0
    return 0.5 * torch.cat((E11, E12, E21, E22), dim= -1)



def load_F():
    def get_max_min(x):
        x = np.sort(x)
        min_id = int(len(x) * 0.005)
        max_id = int(len(x) * 0.005)
        return x[min_id], x[-max_id]
    # check the max / min values in F
    with open(input_data_dir + "deformation_gradient.pkl", "rb") as f:
        F_numExample_4 = pkl.load(f) 
    F_numExample_4 = F_numExample_4.numpy()
    num_time_steps = int(F_numExample_4.shape[0] * 0.8)
    F_numExample_4 = F_numExample_4[0:num_time_steps, :, :]
    print("F_numExample_4 shape: ", F_numExample_4.shape)

    F11_min, F11_max = get_max_min(F_numExample_4[:, :, 0].flatten())
    F12_min, F12_max = get_max_min(F_numExample_4[:, :, 1].flatten())
    F21_min, F21_max = get_max_min(F_numExample_4[:, :, 2].flatten())
    F22_min, F22_max = get_max_min(F_numExample_4[:, :, 3].flatten())
    
    # F_max = np.max(F_numExample_4, axis=(0, 1) )
    F_max = (F11_max, F12_max, F21_max, F22_max)
    # print("F_max")
    # print(F_max)
    # F_min = np.min(F_numExample_4, axis=(0, 1) )
    F_min = (F11_min, F12_min, F21_min, F22_min)
    # print("F_min")
    # print(F_min)
    return F_max, F_min


def construct_testing_F(F_max, F_min, F_path):
    F11_max, F12_max, F21_max, F22_max = F_max
    print(F11_max, F12_max, F21_max, F22_max)
    F11_min, F12_min, F21_min, F22_min = F_min
    print(F11_min, F12_min, F21_min, F22_min)

    F_numExample_4 = torch.zeros((args.gamma_steps, 4) ) # to return
    
    if F_path == "UT":
        F11_list = np.linspace(F11_min, F11_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_list[i]
            F_numExample_4[i, 3] = 1.0
        return F11_list - 1.0, F_numExample_4
    
    elif F_path == "BT":
        F11_22_min = np.max((F11_min, F22_min) )
        F11_22_max = np.min((F11_max, F22_max) )
        F11_22_list = np.linspace(F11_22_min, F11_22_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_22_list[i]
            F_numExample_4[i, 3] = F11_22_list[i]
        return F11_22_list - 1.0, F_numExample_4
    
    elif F_path == "SS":
        F12_list = np.linspace(F12_min, F12_max, args.gamma_steps)
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = 1.0
            F_numExample_4[i, 1] = F12_list[i]
            F_numExample_4[i, 3] = 1.0
        return F12_list, F_numExample_4
        
    else:
        raise RuntimeError("F_path invalid")
    
def construct_exterpolation_F(F_max, F_min, F_path):
    F11_max, F12_max, F21_max, F22_max = F_max
    print(F11_max, F12_max, F21_max, F22_max)
    F11_min, F12_min, F21_min, F22_min = F_min
    print(F11_min, F12_min, F21_min, F22_min)

    F_numExample_4 = torch.zeros((args.gamma_steps, 4) ) # to return
    
    if F_path == "UT":
        F11_min_extended = 1.0 -  (1+ args.exterpolation_ratio) * abs(F11_min - 1.0)
        F11_max_extended = 1.0 +  (1+ args.exterpolation_ratio) * abs(F11_max - 1.0)
        assert args.gamma_steps % 2 == 0
        left_F11_list = np.linspace(F11_min_extended, F11_min, args.gamma_steps // 2)
        right_F11_list = np.linspace(F11_max, F11_max_extended, args.gamma_steps // 2)

        F11_list = np.concatenate((left_F11_list, right_F11_list))

        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_list[i]
            F_numExample_4[i, 3] = 1.0
        return F11_list - 1.0, F_numExample_4, F11_min - 1.0, F11_max - 1.0
    

    elif F_path == "BT":
        F11_22_min = np.max((F11_min, F22_min) )
        F11_22_max = np.min((F11_max, F22_max) )
        F11_22_min_extended = 1.0 - (1 + args.exterpolation_ratio) * abs(F11_22_min - 1.0)
        F11_22_max_extended = 1.0 + (1 + args.exterpolation_ratio) * abs(F11_22_max - 1.0)
        assert args.gamma_steps % 2 == 0
        left_F11_22_list = np.linspace(F11_22_min_extended, F11_22_min, args.gamma_steps // 2)
        right_F11_22_list = np.linspace(F11_22_max, F11_22_max_extended, args.gamma_steps // 2)

        F11_22_list = np.concatenate((left_F11_22_list, right_F11_22_list ) )

        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = F11_22_list[i]
            F_numExample_4[i, 3] = F11_22_list[i]
        return F11_22_list - 1.0, F_numExample_4, F11_22_min - 1.0, F11_22_max - 1.0
    

    elif F_path == "SS":
        F12_min_extended = (1 + args.exterpolation_ratio) * F12_min
        F12_max_extended = (1 + args.exterpolation_ratio) * F12_max
        assert args.gamma_steps % 2 == 0
        F12_list_left = np.linspace(F12_min_extended, F12_min, args.gamma_steps // 2)
        F12_list_right = np.linspace(F12_max, F12_max_extended, args.gamma_steps // 2)

        F12_list = np.concatenate((F12_list_left, F12_list_right ) )
        for i in range(args.gamma_steps):
            F_numExample_4[i, 0] = 1.0
            F_numExample_4[i, 1] = F12_list[i]
            F_numExample_4[i, 3] = 1.0
        return F12_list, F_numExample_4, F12_min, F12_max
        
    else:
        raise RuntimeError("F_path invalid")



def cmpt_groundTruth_W_and_P(F_numExample_4)  -> np.array:
    Youngs_modulus = 1e4
    nu= 0.3 # Poisson rate
    mu  = Youngs_modulus / (2.0*(1.0 + nu))
    lmbda = Youngs_modulus * nu / ((1.0 + nu)*(1.0 - 2.0*nu))
    bulk_K = Youngs_modulus / (3.0 * (1.0 - 2 * nu) ) #bulk modulus

    if material == "NH":
        # New-Hookean
        F_numExample_4.requires_grad = True
        C_numExample_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_numExample_1 = C_numExample_4[:, 0:1] + C_numExample_4[:, 3:4]
        J_numExample_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        W = (mu / 2) * (I1_numExample_1 - 2) - mu * torch.log(J_numExample_1) + (lmbda / 2) * (torch.log(J_numExample_1))**2 # 2D
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "StVK":            
        F_numExample_4.requires_grad = True
        E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4)
        I1 = E_numExample_4[:, 0:1] + E_numExample_4[:, 3:4]
        I2 = E_numExample_4[:, 0:1] * E_numExample_4[:, 3:4] - E_numExample_4[:, 1:2] * E_numExample_4[:, 2:3]
        W = (lmbda /2 ) * I1 **2 + mu * (I1 **2 - 2 * I2)
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    
    elif material == "MR":
        C10 = 7. * mu / 16
        C01 = 1. * mu / 16
        
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I2_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        I1_bar = J_T_numE_1 ** (- 2. / 2.) * I1_T_numE_1
        I2_bar = J_T_numE_1 ** (- 4. / 2.) * I2_T_numE_1
        term1 = C10 * (I1_bar - 2)
        term2 = C01 * (I2_bar - 1)
        term3 = 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        W = term1 + term2 + term3
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Fung":
        b = 1.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        exponential = b * (I1_T_numE_1 * ( I3_T_numE_1 ** (-0.5) ) - 2)
        W = mu / (2 * b) * ( exponential + torch.exp(exponential) - 1.0 ) + 0.5 * bulk_K * (J_T_numE_1 - 1) ** 2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "Gent":
        Jm = 10
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]

        W_inc = - (mu / 2) * (Jm * torch.log( 1 - ( I3_T_numE_1 ** (-0.5) * I1_T_numE_1 - 2.0) / Jm )  )
        W_vol = 0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W_inc + W_vol
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    elif material == "AB":
        C1 = mu
        N = 10.0
        F_numExample_4.requires_grad = True
        C_T_numE_4 = cmpt_cauchy_green_deformation(F_numExample_4)
        I1_T_numE_1 = C_T_numE_4[:, 0:1] + C_T_numE_4[:, 3:4]
        I3_T_numE_1 = C_T_numE_4[:, 0:1] * C_T_numE_4[:, 3:4] - C_T_numE_4[:, 1:2] * C_T_numE_4[:, 2:3]            
        J_T_numE_1 = F_numExample_4[:, 0:1] * F_numExample_4[:, 3:4] - F_numExample_4[:, 1:2] * F_numExample_4[:, 2:3]
        I1_bar = I3_T_numE_1 ** (-0.5) * I1_T_numE_1
        W1 = C1 * ( 0.5 * (I1_bar - 2) + (1 / (20 * N) ) * (I1_bar**2  - 4) + (11 / (N ** 2 * 1050 ) ) * (I1_bar**3  - 8) + (19 / (N ** 3 * 7000 ) ) * (I1_bar**4  - 16) + (519 / (N ** 4 * 673750 ) ) * (I1_bar**5  - 32) )
        W2 =  0.5 * bulk_K * (J_T_numE_1 - 1.0) ** 2
        W = W1 + W2
        P_numExample_4 = torch.autograd.grad(W.sum(), F_numExample_4, create_graph=True)[0]
    else:
        raise RuntimeError("material error")
    
    return W.detach().numpy(), P_numExample_4.detach().numpy()


def predict_W_and_P(F_numExample_4) -> np.array:
    zero_strain_1_4 = torch.zeros((1, 4) )
    if cuda_flag:
        zero_strain_1_4 = zero_strain_1_4.cuda()
    zero_strain_1_4.requires_grad = True
    


    ## input data
    E_numExample_4  = cmpt_green_strain_tensor(F_numExample_4) # green strain tensor
    E_numExample_4 = torch.autograd.Variable(E_numExample_4)
    E_numExample_4.requires_grad = True

    # normalize INPUT Green strain
    normalizedE_numExample_4 = E_numExample_4 / greenStrain_max

    energy_numExample_1 = energy_model(normalizedE_numExample_4)
    S_numExample_4 = torch.autograd.grad(energy_numExample_1.sum(), E_numExample_4, create_graph=True)[0]

    ## the 2-nd and 1-st Piola Kirchhoff stress S with zero strain
    zeroEnergy_1_1 = energy_model(zero_strain_1_4)
    zeroS_1_4  = torch.autograd.grad(zeroEnergy_1_1.sum(), zero_strain_1_4, create_graph = True)[0]
    zeroS_1_2_2 = zeroS_1_4.reshape(1, 2, 2)
    zeroP_1_2_2 = zeroS_1_2_2 # because of the unit_F_1_1_2_2
    
    # energy
    energy_numExample_1 = energy_numExample_1 - zeroEnergy_1_1
    

    ## correct the predicted stress
    S_numE_2_2 = S_numExample_4.reshape(S_numExample_4.shape[0],  2, 2) # last two dimensions are i and j. sum over j
    F_numE_2_2 = F_numExample_4.reshape(F_numExample_4.shape[0], 2, 2)
    P_numE_2_2 = torch.matmul(F_numE_2_2, S_numE_2_2)
    P_numE_2_2 = P_numE_2_2 - zeroP_1_2_2
    P_numExample_4 = P_numE_2_2.reshape(P_numE_2_2.shape[0], 4)
    
    # unnormalize predictions
    P_numExample_4 = P_numExample_4 * momentum_max
    energy_numExample_1 = energy_numExample_1 * momentum_max
    
    # return energy_numExample_1.detach().numpy() * greenStrain_max, P_numExample_4.detach().numpy()
    return energy_numExample_1.cpu().detach().numpy(), P_numExample_4.cpu().detach().numpy()


def plot_W_fig():
    num_materials = len(material_list)
    num_F_path = len(F_path_list)
    fig, axs = plt.subplots(num_F_path, num_materials, figsize=set_size(width='two-column', fraction=1.0, height_ratio=0.56)) # gridspec_kw={'height_ratios': [1, 2]}
    
    for F_path_id in range(num_F_path):
        y_max = 0
        for material_id in range(num_materials):
            material = material_list[material_id]
            F_path = F_path_list[F_path_id]
            true_W_numExample_1 = exterpolation_true_W_curves[material][F_path]
            predict_mean_W_numExample_1 = exterpolation_predict_W_curves[material][F_path]["mean"]
            if y_max < np.max(true_W_numExample_1):
                y_max = np.max(true_W_numExample_1)
            if y_max < np.max(predict_mean_W_numExample_1):
                y_max = np.max(predict_mean_W_numExample_1)

        for material_id in range(num_materials):
        
            material = material_list[material_id]
            F_path = F_path_list[F_path_id]
            this_ax = axs[F_path_id, material_id]

            true_W_numExample_1 = true_W_curves[material][F_path]
            predict_mean_W_numExample_1 = predict_W_curves[material][F_path]["mean"]
            predict_std_W_numExample_1 = predict_W_curves[material][F_path]["std"]
            gamma_list = F_path_gamma_list[material][F_path]
            min_gamma, max_gamma = min(gamma_list), max(gamma_list)
            print("interpolation gamma min, max: ", min_gamma, max_gamma)

            this_ax.axvspan(min_gamma, max_gamma, facecolor=interpolation_color, alpha=0.8, label = "interpolation region")
            l1, = this_ax.plot(gamma_list, predict_mean_W_numExample_1, color = predict_curve_color, linewidth = 1.5, label = "predicted W") # xkcd:peacock blue
            l2, = this_ax.plot(gamma_list[::10], true_W_numExample_1[::10], markeredgecolor = true_curve_color, markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4, label = 'true W')
            # this_ax.fill_between(gamma_list, (predict_mean_W_numExample_1 - predict_std_W_numExample_1).squeeze(), (predict_mean_W_numExample_1 + predict_std_W_numExample_1).squeeze(), facecolor=predict_curve_color, alpha=1)
            
            
            # exterpolation            
            gamma_list = exterpolation_gamma_curves[material][F_path]
            true_W_numExample_1 = exterpolation_true_W_curves[material][F_path]
            predict_mean_W_numExample_1 = exterpolation_predict_W_curves[material][F_path]["mean"]
            

            # left
            min_gamma, max_gamma = min(gamma_list[:args.gamma_steps // 2]), max(gamma_list[:args.gamma_steps // 2]) 
            this_ax.axvspan(min_gamma, max_gamma, facecolor=extrapolation_color, alpha=0.8,  label = "extrapolation region")
            this_ax.plot(gamma_list[:args.gamma_steps // 2], predict_mean_W_numExample_1[:args.gamma_steps // 2], color = predict_curve_color,  linewidth = 1.5) # linestyle= (0, (3, 1, 1, 1)),
            this_ax.plot(gamma_list[:args.gamma_steps // 2:30], true_W_numExample_1[:args.gamma_steps // 2:30], markeredgecolor = true_curve_color, markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4)
            
            print("left exterpolation gamma min, max: ", min_gamma, max_gamma)
            # right
            min_gamma, max_gamma = min(gamma_list[args.gamma_steps // 2:]), max(gamma_list[args.gamma_steps // 2:])
            this_ax.axvspan(min_gamma, max_gamma, facecolor=extrapolation_color, alpha=0.8)
            l4, = this_ax.plot(gamma_list[args.gamma_steps // 2:], predict_mean_W_numExample_1[args.gamma_steps // 2:], color = predict_curve_color,  linewidth = 1.5) # , label = "predicted W extrapolation"
            l3, = this_ax.plot(gamma_list[args.gamma_steps // 2::30], true_W_numExample_1[args.gamma_steps // 2::30], markeredgecolor = true_curve_color, markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4)  # , label = "true W extrapolation"
            print("right exterpolation gamma min, max: ", min_gamma, max_gamma)

            
            # this_ax.legend()
            this_ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
            this_ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))

            this_ax.tick_params(axis='y', which='major', pad=0.5)
            this_ax.tick_params(axis='x', which='major', pad=0.8)
            if material_id != 0:
                this_ax.set_yticklabels([])

            this_ax.set_ylim(-y_max * 0.05, y_max * 1.05)

        if material == "VK":
            y_material = "StVK"
        else:
            y_material = material

    #     axs[material_id, 0].set_ylabel("{} ".format(y_material) + r'$W$', labelpad= 4)


    
    axs[0, 0].set_ylabel("UD",  fontsize = 9)#, color = colors["UT"])
    axs[1, 0].set_ylabel("BD",  fontsize = 9)#, color = colors["BT"])
    axs[2, 0].set_ylabel("SD", labelpad = 0.6, fontsize = 9)#, color = colors["SS"])

    for i in range(num_materials):
        axs[-1, i].set_xlabel('$\gamma$',  labelpad= 2, fontsize=10)
        material = material_list[i]
        if material == "VK":
            y_material = "StVK"
        else:
            y_material = material
        axs[0, i].set_title("{} ".format(y_material),  fontsize=8) #  + r'$W$'
    

    
   

    handles, labels = axs[-1, -1].get_legend_handles_labels()
    order_list = []
    ordered_label = ["predicted W", "true W", "interpolation region", "extrapolation region"]
    for label in ordered_label:
        for i in range(len(labels)):
            if label in labels[i]:
                order_list.append(i)
                break
    assert len(order_list) == len(ordered_label)
    handles = [handles[i] for i in order_list]
    labels = [labels[i] for i in order_list]
    leg = fig.legend(handles, labels, loc='upper left', ncol=4, handlelength=2, borderaxespad=0., bbox_to_anchor=(0.22, 0.99), numpoints=1, markerscale=1, columnspacing=1.0, handletextpad= 0.7, borderpad=0.5)
    for legobj in leg.legend_handles:
        legobj.set_linewidth(2)

    texts = leg.get_texts()
    texts[1].set_position((-8, 0))

    plt.subplots_adjust(wspace=0.05) 
    plt.savefig("./figs/" + 'W.pdf', bbox_inches='tight', pad_inches = 0.03)
    plt.close()




def plot_P_fig():
    num_materials = len(material_list)
    num_F_path = len(F_path_list)
    fig, axs = plt.subplots(num_F_path, num_materials, figsize=set_size(width='two-column', fraction=1.0, height_ratio=0.56)) # gridspec_kw={'height_ratios': [1, 2]}
    
    for F_path_id in range(num_F_path):
        y_max = 0
        for material_id in range(num_materials):
            material = material_list[material_id]
            F_path = F_path_list[F_path_id]
            true_P_numExample_1 = exterpolation_true_P_curves[material][F_path]
            predict_mean_P_numExample_1 = exterpolation_predict_P_curves[material][F_path]["mean"]
            if y_max < np.max(true_P_numExample_1):
                y_max = np.max(true_P_numExample_1)
            if y_max < np.max(predict_mean_P_numExample_1):
                y_max = np.max(predict_mean_P_numExample_1)

        for material_id in range(num_materials):
        
            material = material_list[material_id]
            F_path = F_path_list[F_path_id]
            this_ax = axs[F_path_id, material_id]

            true_P_numExample_1 = true_P_curves[material][F_path]
            predict_mean_P_numExample_1 = predict_P_curves[material][F_path]["mean"]
            predict_std_P_numExample_1 = predict_P_curves[material][F_path]["std"]
            gamma_list = F_path_gamma_list[material][F_path]
            min_gamma, max_gamma = min(gamma_list), max(gamma_list)
            print("interpolation gamma min, max: ", min_gamma, max_gamma)

            this_ax.axvspan(min_gamma, max_gamma, facecolor=interpolation_color, alpha=0.8, label = "interpolation region")
            l1, = this_ax.plot(gamma_list, predict_mean_P_numExample_1, color = predict_curve_color, linewidth = 1.5, label = "predicted P") # xkcd:peacock blue
            l2, = this_ax.plot(gamma_list[::10], true_P_numExample_1[::10], markeredgecolor = true_curve_color, markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4, label = 'true P')
            # this_ax.fill_between(gamma_list, (predict_mean_P_numExample_1 - predict_std_P_numExample_1).squeeze(), (predict_mean_P_numExample_1 + predict_std_P_numExample_1).squeeze(), facecolor=predict_curve_color, alpha=1)
            
            
            # exterpolation            
            gamma_list = exterpolation_gamma_curves[material][F_path]
            true_P_numExample_1 = exterpolation_true_P_curves[material][F_path]
            predict_mean_P_numExample_1 = exterpolation_predict_P_curves[material][F_path]["mean"]
            

            # left
            min_gamma, max_gamma = min(gamma_list[:args.gamma_steps // 2]), max(gamma_list[:args.gamma_steps // 2]) 
            this_ax.axvspan(min_gamma, max_gamma, facecolor=extrapolation_color, alpha=0.8,  label = "extrapolation region")
            this_ax.plot(gamma_list[:args.gamma_steps // 2], predict_mean_P_numExample_1[:args.gamma_steps // 2], color = predict_curve_color,  linewidth = 1.5) # linestyle= (0, (3, 1, 1, 1)),
            this_ax.plot(gamma_list[:args.gamma_steps // 2:30], true_P_numExample_1[:args.gamma_steps // 2:30], markeredgecolor = true_curve_color, markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4)
            
            print("left exterpolation gamma min, max: ", min_gamma, max_gamma)
            # right
            min_gamma, max_gamma = min(gamma_list[args.gamma_steps // 2:]), max(gamma_list[args.gamma_steps // 2:])
            this_ax.axvspan(min_gamma, max_gamma, facecolor=extrapolation_color, alpha=0.8)
            l4, = this_ax.plot(gamma_list[args.gamma_steps // 2:], predict_mean_P_numExample_1[args.gamma_steps // 2:], color = predict_curve_color,  linewidth = 1.5) # , label = "predicted P extrapolation"
            l3, = this_ax.plot(gamma_list[args.gamma_steps // 2::30], true_P_numExample_1[args.gamma_steps // 2::30], markeredgecolor = true_curve_color, markerfacecolor="None", linestyle="None", marker='o', markeredgewidth = 1, markersize=4)  # , label = "true P extrapolation"
            print("right exterpolation gamma min, max: ", min_gamma, max_gamma)

            
            # this_ax.legend()
            this_ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
            this_ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))

            this_ax.tick_params(axis='y', which='major', pad=0.5)
            this_ax.tick_params(axis='x', which='major', pad=0.8)
            if material_id != 0:
                this_ax.set_yticklabels([])

            this_ax.set_ylim(-y_max * 0.05, y_max * 1.05)

        if material == "VK":
            y_material = "StVK"
        else:
            y_material = material

    #     axs[material_id, 0].set_ylabel("{} ".format(y_material) + r'$P$', labelpad= 4)


    
    axs[0, 0].set_ylabel("UD",  fontsize = 9)#, color = colors["UT"])
    axs[1, 0].set_ylabel("BD",  fontsize = 9)#, color = colors["BT"])
    axs[2, 0].set_ylabel("SD", labelpad = 0.6, fontsize = 9)#, color = colors["SS"])

    for i in range(num_materials):
        axs[-1, i].set_xlabel('$\gamma$',  labelpad= 2, fontsize=10)
        material = material_list[i]
        if material == "VK":
            y_material = "StVK"
        else:
            y_material = material
        axs[0, i].set_title("{} ".format(y_material),  fontsize=8) #  + r'$P$'
    

    
   

    handles, labels = axs[-1, -1].get_legend_handles_labels()
    order_list = []
    ordered_label = ["predicted P", "true P", "interpolation region", "extrapolation region"]
    for label in ordered_label:
        for i in range(len(labels)):
            if label in labels[i]:
                order_list.append(i)
                break
    assert len(order_list) == len(ordered_label)
    handles = [handles[i] for i in order_list]
    labels = [labels[i] for i in order_list]
    leg = fig.legend(handles, labels, loc='upper left', ncol=4, handlelength=2, borderaxespad=0., bbox_to_anchor=(0.22, 0.99), numpoints=1, markerscale=1, columnspacing=1.0, handletextpad= 0.7, borderpad=0.5)
    for legobj in leg.legend_handles:
        legobj.set_linewidth(2)

    texts = leg.get_texts()
    texts[1].set_position((-8, 0))

    plt.subplots_adjust(wspace=0.05) 
    plt.savefig("./figs/" + 'P.pdf', bbox_inches='tight', pad_inches = 0.03)
    plt.close()




if __name__ == "__main__":
    cuda_flag = False
    material_list = ["StVK", "MR", "Gent", "AB", "Fung"]

    F_path_list = ["UT", "BT", "SS"]
    colors = {"UT": "#E49E21", "BT": "#A61B29", "SS": "#A48CE6"}

    interpolation_color = "#6eaf9a"
    extrapolation_color =  "#f9c2c1"

    predict_curve_color = "black"
    true_curve_color = "black"

    predict_W_curves = {}
    true_W_curves = {}
    predict_P_curves = {}
    true_P_curves = {}

    F_path_gamma_list = {}
    
    exterpolation_predict_W_curves = {} # for exterpolation
    exterpolation_true_W_curves = {} # for exterpolation
    exterpolation_predict_P_curves = {} # for exterpolation
    exterpolation_true_P_curves = {} # for exterpolation

    exterpolation_gamma_curves = {} # for exterpolation


    for material in material_list:
        print("material: ", material)
        if material == "StVK":
            material_name = "VK"
        else:
            material_name = material
        input_data_dir = "../data_process/{}_GNN_input_0.001/".format(material_name)
        trained_model_folder = "./trained_{}_ele_0.001_float64/interpolate_0.002/".format(material)  

        predict_W_curves[material] = {}
        true_W_curves[material] = {}
        predict_P_curves[material] = {} # the magnitude of P
        true_P_curves[material] = {} # the magnitude of P

        F_path_gamma_list[material] = {}

        exterpolation_predict_W_curves[material] = {} # for exterpolation
        exterpolation_true_W_curves[material] = {} # for exterpolation
        exterpolation_predict_P_curves[material] = {} # exterpolation, the magnitude of P
        exterpolation_true_P_curves[material] = {} # exterpolation, the magnitude of P

        exterpolation_gamma_curves[material] = {} # for exterpolation
              
        
        F_max, F_min = load_F()        



        for F_path in F_path_list:
            print("F_path: ", F_path)
            predict_W_curves[material][F_path] = {}
            exterpolation_predict_W_curves[material][F_path] = {}
            predict_P_curves[material][F_path] = {}
            exterpolation_predict_P_curves[material][F_path] = {}
            

            ## construct input
            gamma_list, F_numExample_4 = construct_testing_F(F_max, F_min, F_path)
            F_path_gamma_list[material][F_path] = gamma_list

            # exterpolation input
            exterpolation_gamma, exterpolation_F_numExample_4, left_tick, right_tick = construct_exterpolation_F(F_max, F_min, F_path)
            exterpolation_gamma_curves[material][F_path] = exterpolation_gamma

            ## true W and P
            true_W_numExample_1, true_P_numExample_4 = cmpt_groundTruth_W_and_P(F_numExample_4)
            print("true_W_numExample_1 shape: ", true_W_numExample_1.shape)
            true_W_curves[material][F_path] = true_W_numExample_1
            true_P_curves[material][F_path] = np.linalg.norm(true_P_numExample_4, axis= -1, keepdims= True)

            # true exterpolation W and P
            exterpolation_true_W_numExample_1, exterpolation_true_P_numExample_4 = cmpt_groundTruth_W_and_P(exterpolation_F_numExample_4)
            exterpolation_true_W_curves[material][F_path] = exterpolation_true_W_numExample_1
            exterpolation_true_P_curves[material][F_path] = np.linalg.norm(exterpolation_true_P_numExample_4, axis= -1, keepdims= True)


            ## predicted W and P in multiple experiments
            predict_W = []
            predict_P = []
            exterpolation_predict_W = []
            exterpolation_predict_P = []

            num_experiments = len(os.listdir(trained_model_folder ) )
            exp_list = [i + 1 for i in range(num_experiments)]
            for exp in exp_list:                
                # load trained model
                energy_model = energy_density_NN(args.input_dim, args.hidden_dim, args.activation)    
                best_trained_model_file = trained_model_folder + "exp{}/".format(exp) + "best_trained_model.pt"
                
                with open(trained_model_folder + "exp{}/".format(exp) + "greenStrain_max.pkl", "rb") as f:
                    greenStrain_max = pkl.load(f)
                with open(trained_model_folder + "exp{}/".format(exp) + "momentum_max.pkl", "rb") as f:
                    momentum_max_dict = pkl.load(f)
                    momentum_max = momentum_max_dict["momentum_max"]

                energy_model.load_state_dict(torch.load(best_trained_model_file))
                
                # predicted W and P
                predict_W_numExample_1, predict_P_numExample_4 = predict_W_and_P(F_numExample_4)
                predict_W.append(predict_W_numExample_1)
                predict_P.append(predict_P_numExample_4)

                # exterpolation predicted W and P
                exterpolation_predict_W_numExample_1, exterpolation_predict_P_numExample_4 = predict_W_and_P(exterpolation_F_numExample_4)
                exterpolation_predict_W.append(exterpolation_predict_W_numExample_1)
                exterpolation_predict_P.append(exterpolation_predict_P_numExample_4)


            # W
            predictW_exp_numExamples_1 = np.stack(predict_W, axis= 0)
            print("predictW_exp_numExamples_1 shape: ", predictW_exp_numExamples_1.shape)
            exterpolation_predictW_exp_numExamples_1 = np.stack(exterpolation_predict_W, axis= 0) # exterpolation


            predict_W_curves[material][F_path]["mean"] = np.mean(predictW_exp_numExamples_1, axis= 0)
            predict_W_curves[material][F_path]["std"] = np.std(predictW_exp_numExamples_1, axis= 0)
            print("predict_W_curves[material][F_path][mean] shape: ", predict_W_curves[material][F_path]["mean"].shape, material, F_path)
            print("predict_W_curves[material][F_path][std] shape: ", predict_W_curves[material][F_path]["std"].shape, material, F_path)

            exterpolation_predict_W_curves[material][F_path]["mean"] = np.mean(exterpolation_predictW_exp_numExamples_1, axis= 0) # exterpolation
            exterpolation_predict_W_curves[material][F_path]["std"] = np.std(exterpolation_predictW_exp_numExamples_1, axis= 0) # exterpolation


            # P
            predictP_exp_numExamples_4 = np.stack(predict_P, axis= 0)
            exterpolation_predictP_exp_numExamples_4 = np.stack(exterpolation_predict_P, axis= 0) # exterpolation
            assert predictP_exp_numExamples_4.shape[-1] == 4
            assert exterpolation_predictP_exp_numExamples_4.shape[-1] == 4
            predict_Pmagnitude_exp_numExamples_1 = np.linalg.norm(predictP_exp_numExamples_4, axis= -1, keepdims= True)
            exterpolation_predict_Pmagnitude_exp_numExamples_1 = np.linalg.norm(exterpolation_predictP_exp_numExamples_4, axis= -1, keepdims= True)

            predict_P_curves[material][F_path]["mean"] = np.mean(predict_Pmagnitude_exp_numExamples_1, axis= 0)
            predict_P_curves[material][F_path]["std"] = np.std(predict_Pmagnitude_exp_numExamples_1, axis= 0)

            exterpolation_predict_P_curves[material][F_path]["mean"] = np.mean(exterpolation_predict_Pmagnitude_exp_numExamples_1, axis= 0) # exterpolation
            exterpolation_predict_P_curves[material][F_path]["std"] = np.std(exterpolation_predict_Pmagnitude_exp_numExamples_1, axis= 0) # exterpolation

    plot_W_fig()
    
    
    
    plot_P_fig()

