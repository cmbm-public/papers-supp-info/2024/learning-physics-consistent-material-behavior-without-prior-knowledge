# | StVK | MR  | Fung |Gent | AB |


import os
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc,rcParams
# plt.rcParams["font.weight"] = "bold"
# plt.rcParams['xtick.major.pad']='1'
# plt.rcParams['ytick.major.pad']='0.5'
# plt.rc('xtick', labelsize=6)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=6)
# plt.rc('legend', fontsize=7)
plt.style.use(['./figs/prl_paper.mplstyle'])
from figs.plot_helper_for_paper import set_size

import pickle as pkl

from model import energy_density_NN

import argparse




def read_logs(log_file):
    with open(log_file, "r") as f:
        lines = [line.strip() for line in f]
    
    train_momentum_MAE_list = []
    valid_momentum_MAE_list = []
    epoch_list = []
    
    for line in lines:
        line = line.split()
        if line[0] == "Epoch:":
            epoch_list.append(int(line[1]))

            if line[2] == "train_momentum_MAE_mean:":
                train_momentum_MAE_list.append(float(line[3] ) )
            else:
                raise RuntimeError("line[2] error")
            
            if line[4] == "valid_momentum_MAE_mean:":
                valid_momentum_MAE_list.append(float(line[5] ) )
            else:
                raise RuntimeError("line[6] error")
    
    print("train_loss_list len: ", len(train_momentum_MAE_list))
    print("valid_loss_list len: ", len(valid_momentum_MAE_list))
    # print("last epoch: ", epoch_list[-1])
    return train_momentum_MAE_list, valid_momentum_MAE_list


def plot_training_curves_fig():
    train_curve_label = "training curve"
    valid_curve_label = "validation curve"
    num_materials = len(material_list)
    assert num_materials == 5

    num_time_steps = -1
    
    fig, axs = plt.subplots(1, 5, figsize=set_size(width='two-column', fraction=1.0, height_ratio=0.18)) # gridspec_kw={'height_ratios': [1, 2]}
    # fig = plt.figure(figsize=set_size(width='two-column', fraction=1.0, height_ratio=0.4), constrained_layout=True, dpi=300)

    max_value =  -1
    for material_id in range(num_materials):
        material = material_list[material_id]
        if material == "Fung":
            num_time_steps = 1800
        else:
            num_time_steps = 900

        max_value = max(max_value, np.max(train_curves[material]["exp{}".format(1)][:num_time_steps]))
        max_value = max(max_value, np.max(valid_curves[material]["exp{}".format(1)][:num_time_steps]))



    # fig.tight_layout()
    for material_id in range(num_materials):
        material = material_list[material_id]
        this_ax = axs[material_id % 5]
        print("pos: ({}, {})".format(material_id // 5, material_id % 5) )
        

        if material == "Fung":
            num_time_steps = 1800
        else:
            num_time_steps = 1000

        gamma_list = np.arange(0, 2000, 1)

        
        this_ax.plot(gamma_list[:num_time_steps], valid_curves[material]["exp{}".format(1)][:num_time_steps], color = "#00a0a4", linewidth = 0.8, label = valid_curve_label)
        # this_ax.plot(gamma_list, valid_curves[material]["exp{}".format(2)], color = "xkcd:purple pink", linewidth = 1)
        # this_ax.plot(gamma_list, valid_curves[material]["exp{}".format(3)], color = "xkcd:purple pink", linewidth = 1)
        # this_ax.fill_between(gamma_list, (predict_mean_W_numExample_1 - predict_std_W_numExample_1).squeeze(), (predict_mean_W_numExample_1 + predict_std_W_numExample_1).squeeze(), facecolor="lightskyblue", alpha=1)
        
        this_ax.plot(gamma_list[:num_time_steps], train_curves[material]["exp{}".format(1)][:num_time_steps], color = "#003f5c", linewidth = 0.8, label = train_curve_label)
        # this_ax.plot(gamma_list, train_curves[material]["exp{}".format(2)], color = "xkcd:peacock blue", linewidth = 1)
        # this_ax.plot(gamma_list, train_curves[material]["exp{}".format(3)], color = "xkcd:peacock blue", linewidth = 1)

        
        if material == "VK":
            y_material = "StVK"
        else:
            y_material = material
        # this_ax.set_ylabel("{} ".format(y_material) + 'loss', fontweight="bold", fontsize=8, labelpad= 2)
        # this_ax.set_ylabel("{} ".format(y_material) + 'loss', labelpad= 1)
        if material_id % 5 == 0:
            this_ax.set_ylabel('loss', labelpad= 1,fontsize=10)
        
        if y_material == "StVK":
            print("material: ", material)
            this_ax.set_title(title_names[y_material],  x=0.52, y=0.58, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5', linewidth=0.6), fontsize = 8) # lightblue
        else:
            this_ax.set_title(title_names[y_material],  x=0.52, y=.7, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5', linewidth=0.6), fontsize = 8) # lightblue


        # this_ax.set_xlabel('training epochs', labelpad= 0.5)
        # if material_id %2 == 0:
        #     this_ax.set_xlabel('training epochs', labelpad= 0.5)
        
        # leg = this_ax.legend()
        # for legobj in leg.legend_handles:
        #     legobj.set_linewidth(1.5)
        this_ax.grid(False)
        this_ax.set_ylim(0, max_value)

        if material_id != 0:
            this_ax.set_yticklabels([])
        
        if material != "Fung":
            # this_ax.set_xticks([0, 300, 600, 900])
            this_ax.set_xticks([0, 500, 1000])
        else:
            # this_ax.set_xticks([0, 600, 1200, 1800])
            this_ax.set_xticks([0, 900, 1800])
        this_ax.xaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter('%d'))
        for label in this_ax.get_xticklabels():
            
            if label.get_text() == "0":
                label.set_ha('center')
            else:
                label.set_ha('center')

    
    axs[2].set_xlabel('training epochs', labelpad= 3)

    handles, labels = axs[-1].get_legend_handles_labels()
    
    order_list = []
    ordered_label = [train_curve_label, valid_curve_label]
    for label in ordered_label:
        for i in range(len(labels)):
            if label in labels[i]:
                order_list.append(i)
                break
    assert len(order_list) == len(ordered_label)
    handles = [handles[i] for i in order_list]
    labels = [labels[i] for i in order_list]

    
    leg = fig.legend(handles, labels, loc='center', ncol=4, handlelength=2.4, borderaxespad=0., bbox_to_anchor=(0.5, 1.0), fontsize = 8)
    for legobj in leg.legend_handles:
        legobj.set_linewidth(2)
    
    fig.subplots_adjust(wspace=0.08)

    plt.savefig("./figs/" + 'loss_curve.pdf', bbox_inches='tight', pad_inches = 0.03, dpi=300)
    plt.close()




if __name__ == "__main__":
    cuda_flag = False
    # material_list = ["NH", "StVK", "MR", "Fung", "Gent", "AB"]
    # material_list = ["StVK", "MR",  "Fung", "Gent", "AB"] 
    material_list = ["StVK", "MR", "Gent", "AB", "Fung"]

    title_names = {"NH": "Neo-Hookean", "StVK": "St. Venant–\n Kirchhoff", "MR": "Mooney-Rivlin", "Fung": "Fung", "Gent": "Gent", "AB": "Arruda-Boyce"}

    train_curves = {}
    valid_curves = {}
    # training_epoch_curves = {}
    
   


    for material in material_list:
        print("material: ", material)
        trained_model_folder = "./trained_{}_ele_0.001_float64/interpolate_0.002/".format(material)  

        train_curves[material] = {}
        valid_curves[material] = {}
        # training_epoch_curves[material] = {} # the magnitude of P


        
        # for exp in range(1, 4):                
        for exp in [1]:
            # read the training logging
            log_file = trained_model_folder + "exp{}/".format(exp) + "logging.txt"
            train_momentum_MAE_list, valid_momentum_MAE_list = read_logs(log_file)
            print("train_momentum_MAE_list length: ", len(train_momentum_MAE_list))
            print("valid_momentum_MAE_list length: ", len(valid_momentum_MAE_list))

            train_curves[material]["exp{}".format(exp)] = train_momentum_MAE_list
            valid_curves[material]["exp{}".format(exp)] = valid_momentum_MAE_list



    plot_training_curves_fig()
    
    