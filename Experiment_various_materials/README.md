## Learn various constitutive relations
This folder contains the code for experiments in Sec.3.


### Instructions

1. Generate simulation and save mesh info:  
   
   ```bash
   # in generate_data/
   bash generate_data.bash
   ```

2. Process the simulation and generate input for training the neural network
   
   ```bash
   # in data_process/
   bash generate_uLED_input.bash
   ```

3. Train uLED
   
   ```bash
   #in uLED/
   bash auto_train_StVK.bash # learn St. Venant–Kirchhoff
   bash auto_train_NH.bash # learn Neo-Hookean
   bash auto_train_MR.bash # learn Mooney-Rivlin
   bash auto_train_Gent.bash # learn Gent
   bash auto_train_AB.bash # learn Arruda-Boyce
   bash auto_train_Fung.bash # learn Fung
   ```

4. Reproduce Fig.4 (visualize the learned W and P fields for NH material)
      ```bash
      # in learned_field_visualization/
      mkdir case1/
      python visualization_stress_field_case1_new.py
    
      # after this, the figures should be generated in learned_field_visualization/case1/
      ```

5. Reproduce Fig.5(a)
      ```bash
      # in uLED/
      python W_P_NH_Fpath.py
    
      # after this, NH_W_Fpath.pdf should be in uLED/figs/
      ```

6. Reproduce Fig.5(b)
      ```bash
      # in uLED/
      python visualization_NH_training_curve.py
    
      # after this, NH_loss_curve.pdf should be in uLED/figs/
      ```

7. Reproduce Fig.6
      ```bash
      # in learned_field_visualization/
      mkdir case3/
      python visualization_stress_field_case3_new.py
    
      # after this, the figures should be generated in learned_field_visualization/case3/
      ```

8. Reproduce Fig.7
      ```bash
      # in uLED/
      python W_P_error_Fpath_discontinuousY.py
    
      # after this, W_error_discontinuous.pdf should be in uLED/figs/
      ```


